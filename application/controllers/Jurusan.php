<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller {

	// ************************************************//
	// Contoller jurusan/parameter                     //
	// fungsi :                                        //
	// 1. Menambah parameter baru                      //
	// 2. Mengubah parameter yang ada                  //
	// 3. Menghapus parameter                          //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();

		$this->load->model('model_admin','madmin');	
		$this->load->model('model_jurusan','mjurusan');
		

		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'jurusan');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
			
	   	$user=$this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
            'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
			'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
			'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
			'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'jurusan'),
			'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')
		
		
         );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_jurusan');
		$this->load->view('absen/footer_jurusan');
	}

	

	function fetch_jurusan(){

			
		$fetch_data= $this->mjurusan->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->PARAMETER;
			$sub_array[] = $row ->CODE;
			$sub_array[] = $row ->DESCRIPTION;
			switch ($row ->FLAG_ACTIVE) {
			       case "Y":
					$sub_array[] = '<span class="badge badge-success">Aktif</span>';
					break;
					case "N":
					$sub_array[] =  '<span class="badge badge-danger">Tidak Aktif</span>';
					break;
			}
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->NO_SR.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit</button> <button type="button" name="delete" id="'. $row ->NO_SR.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mjurusan->get_all_data(),
			"recordsFiltered"=>$this->mjurusan->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action(){
		if($_POST["action"]=="Add")
		{
				$insert_data = array(
				'PARAMETER'=>$this->input->post('paramater'),
				'CODE'=>$this->input->post('code'),
				'DESCRIPTION	'=>$this->input->post('description'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			
			$hasilCek =$this->madmin->cekExist('CODE',$this->input->post('code'),'m_gcm');
			if($hasilCek <> true){
				$this->mjurusan->insert_crud($insert_data);
				echo 'masuk';
			}else{
				echo 'exist';
			}
			
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'PARAMETER'=>$this->input->post('paramater'),
				'CODE'=>$this->input->post('code'),
				'DESCRIPTION	'=>$this->input->post('description'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			$this->mjurusan->update_crud($this->input->post('unik'),$update_data);
			echo 'ubah';
		}
	}
	
	

	function fetch_single_user(){
		$output = array();
		$this->load->model('mjurusan');	
		$data =$this->mjurusan->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['PARAMETER'] = $row->PARAMETER;
			$output['CODE'] = $row->CODE;
			$output['DESCRIPTION'] = $row->DESCRIPTION;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
			$output['NO_SR'] = $row->NO_SR;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->mjurusan->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

		
}

// ウェンディバユ作成 //
