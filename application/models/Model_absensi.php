<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_absensi extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "t_absensi_detail";
	var $select_column = array ("t_absensi_id","tgl_absensi","cd_user","waktu_masuk","waktu_keluar");
	var $order_column = array(null,"tgl_absensi","cd_user","waktu_masuk","waktu_keluar");

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if ($userGroupIn <>'SUPERADMIN'){
			$this->db->where('cd_user',$userLoginIn);	
		}
		
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("tgl_absensi", $_POST["search"]["value"]);
		$this->db->like("cd_user", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("tgl_absensi");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert('t_absensi_detail',$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("t_absensi_id",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("t_absensi_id",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("t_absensi_id",$id);
		$this->db->delete($this->table);
		
	}

	function cek_absen(){
		$userLogCek=$this->session->userdata('user');
		$this->db->where('cd_user',$userLogCek);
		$this->db->where('tgl_absensi',date("Y-m-d"));
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cek_absen_pulang(){
		$userLogCek=$this->session->userdata('user');
		$this->db->where('cd_user',$userLogCek);
		$this->db->where('tgl_absensi',date("Y-m-d"));
		$this->db->where('waktu_keluar IS NOT NULL');
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function getIdAbsensi(){
		$userLogCekAbsen=$this->session->userdata('user');
		$this->db->select("waktu_masuk,t_absensi_id");
		$this->db->where('tgl_absensi',date("Y-m-d"));
		$this->db->where('cd_user',$userLogCekAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}



	}

	function upload_jamKeluar($user,$tanggal,$jamKeluar,$Image){
		$sql ="call update_absen(?,?,?,?)";
		$param = array(
			'user'=>$user,
			'tanggal'=>$tanggal,
			'jamKeluar'=>$jamKeluar,
			'filename'=>$Image
			
		);
		$this->db->query($sql,$param);
	}

	function getTglCabang($cabang){
		$sql="select date_format(TGL_TODAY,'%Y-%m-%d') from m_cabang where CABANG_ID = ? and FLAG_ACTIVE ='Y' ";

		$this->db->query($sql,$cabang);
	}
}
