<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ;?></title>
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/dist/img/favicon-32x32.png');?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css"> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/all.min.css');?>"> -->
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- flag-icon-css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jqvmap/jqvmap.min.css');?>">

 <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.css');?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/summernote/summernote-bs4.css');?>">

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.css');?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css');?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
   <link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet">

   <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed ">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <ul class="navbar-nav " style="border-right: 1px solid ">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto" >
      
            
      <!-- Language Dropdown Menu -->
      <!-- <li class="nav-item dropdown" >

      <?php foreach ($def_bahasa as $default) { ?>
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="<?php echo $default['CHARVALUE']; ?>"></i>
        </a>
    
        <div class="dropdown-menu dropdown-menu-right p-0">
          <?php foreach ($bahasa as $bahasa) { ?>
         
          <?php if(get_cookie('lang_is') == $bahasa['CODE']) { ?>
            <a href="<?php echo site_url().'lang_setter/set_to/'.$bahasa['CODE'] ;?>" class="dropdown-item active">
            <i class="<?php echo $bahasa['CHARVALUE']; ?>"></i> <?php echo $bahasa['DESCRIPTION']; ?>
          </a>
          <?php } else { ?> 
          <a href="<?php echo site_url().'lang_setter/set_to/'.$bahasa['CODE'] ;?>" class="dropdown-item">
            <i class="<?php echo $bahasa['CHARVALUE']; ?>"></i> <?php echo $bahasa['DESCRIPTION']; ?>
          </a>
          <?php } }?>
          
          
        </div>
        <?php }?>
      </li> -->
      <li class="nav-item dropdown">
				<a href="#" class="nav-link" data-toggle="dropdown" data-hover="dropdown">
                    
          <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" style="width: 10%;"  class="img-circle">
          <em><strong>Hi</strong>, <span class="namaUser"><?php foreach ($get_current_user as $get_current_user) { echo $get_current_user['NAME'] ; }?></span> </em> <i class="dropdown-icon fa fa-angle-down"></i>
        </a>
										<!-- <ul class="dropdown-menu pull-right icon-right arrow"> -->
          <div class="dropdown-menu dropdown-menu-right p-0">		
           <a href="#" class="dropdown-item">
                        <i class="fa fa-user"></i> Profile</a>
												<a href="#" class="dropdown-item"  data-toggle="modal" data-target="#modal-default">
                        <i class="fa fa-key"></i> Change Password</a>
                        
                        <a href="<?php echo base_url('login/logout');?>" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>
                        Sign Out
                        </a> 
                        
                        </div>
                      <!-- </ul> -->
      </li> 
                              <!-- /.card-tools -->
     </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-maroon elevation-5">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('welcome');?>" class="brand-link">
      <img src='<?php echo base_url('assets/dist/img/android-icon-36x36.png');?>'>
      <span class="brand-text font-weight-normal"> Stikom CKI</span>

    </a>

    <!-- Sidebar -->
    <div class="sidebar ">
      <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host"><div class="os-resize-observer observed" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer observed"></div></div><div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 560px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll; right: 0px; bottom: 0px;"><div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
      <?php foreach ($get_current_group as $get_current_group) { 

          echo $this->dynamic_menu->build_menu($get_current_group['GROUP_USER'],'P','Y',$pilih_bahasa); 
        }?>
        </ul>
      </nav>

        
  <!-- /.sidebar-menu -->
    </div></div></div>
    <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="height: 44.5946%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div>
    <!-- /.sidebar -->
  </aside>

  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Password</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="formPassword" action="#" method="post">
              <div class="form-group">
                        <label>Old Password</label>
                        <input type="text" class="form-control" placeholder="Old Password..." id="oldpasswd" onchange="">
              </div>
              <div class="form-group">
                        <label>New Password</label>
                        <input type="text" class="form-control" placeholder="New Password..." id="newpasswd">
              </div>
              <div class="form-group">
                        <label>Re-Type Password</label>
                        <input type="text" class="form-control" placeholder="Re-Type Password..." id="retypepasswd">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>