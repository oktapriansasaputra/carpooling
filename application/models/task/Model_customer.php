<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_customer extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "m_customer";
	var $select_column = array ("CUSTOMER_ID","CUSTOMER_NAME","CUSTOMER_ADDRESS",
		"(select DESCRIPTION from m_gcm where PARAMETER='CITY' and CODE =CITY and FLAG_ACTIVE='Y') as CITY","NO_TLP","NO_KTP","(select DESCRIPTION from m_gcm where PARAMETER='SEX' and CODE =SEX and FLAG_ACTIVE='Y')SEX","BIRTH_DATE");
	var $order_column = array("CUSTOMER_ID","CUSTOMER_NAME","CUSTOMER_ADDRESS",
		"CITY","NO_TLP","NO_KTP","SEX","BIRTH_DATE");

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');
		$userCabang=$this->session->userdata('vp_cabang');
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		
		if($userGroupIn<>'SUPERADMIN'){
			$this->db->where('CABANG_ID',$userCabang);
		}
		
		
			
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CUSTOMER_ID", $_POST["search"]["value"]);
			$this->db->like("CUSTOMER_NAME", $_POST["search"]["value"]);
			$this->db->like("CABANG_ID", $_POST["search"]["value"]);
		
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CUSTOMER_ID");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("CUSTOMER_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("CUSTOMER_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("CUSTOMER_ID",$id);
		$this->db->delete($this->table);
		
	}

	

	
}
//69ffe55a99db23b320584d31242cab8f //