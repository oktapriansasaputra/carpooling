<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Assignorder extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('model_admin', 'madmin');
		$this->load->model('order/model_assignorder', 'massignorder');


		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'order/assignorder');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title' => '',
			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboAktif' => $this->madmin->select_cbo_parameter('FLAG_AKTIF'),
			'cboEmployee' => $this->madmin->getEmployeForClaim($user, $group),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'order/assignorder'),

			'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
			'getColorTopbar' => $this->madmin->getSettingById('OP04'),
			'getColorSidebar' => $this->madmin->getSettingById('OP05'),
			'cboCar' => $this->madmin->select_cbo_parameter('CD_CAR'),

		);

		$this->load->view('absen/header_user', $data);
		$this->load->view('order/view_assignorder');
		$this->load->view('order/footer_assignorder');
	}



	function fetch_order()
	{


		$fetch_data = $this->massignorder->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();

			$sub_array[] = $row->id_request;
			$sub_array[] = $row->nik_user;
			$sub_array[] = $row->EMPLOYEE_NAME;
			$sub_array[] = $row->CD_TITLE;
			$sub_array[] = $row->CD_DESIGN;
			$sub_array[] = date_format(new DateTime($row->tanggal_order), "d M Y");
			$sub_array[] = $row->tujuan;
			$sub_array[] = $row->penjemputan;
			$sub_array[] = $row->jam_jemput;

			$sub_array[] = $row->tot_day;
			$sub_array[] = $row->tot_people;
			$sub_array[] = $row->mobil_recomend;

			$sub_array[] = '<button type="button" name="edit" id="' . $row->id_request . '" class="btn btn-warning btn-xs edit" data-toggle="tooltip" data-placement="top" title="assign"><i class="fas fa-mouse"></i> assign</button>
			<button type="button" name="delete" id="' . $row->id_request . '" class="btn btn-secondary btn-xs delete" data-toggle="tooltip" data-placement="top" title="cetak hasil scoring"><i class="fas fa-print"></i> Cetak</button>';
			$no = $no + 1;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=> $this->massignorder->get_all_data(),
			"recordsFiltered" => $this->massignorder->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action()
	{


		if ($_POST["action"] == "Edit") {
			$user = $this->session->userdata('user');
			$update_data = array(
				'nik_user' => $this->input->post('nik'),
				'tanggal_order' => $this->input->post('tgl_overtime'),
				'tujuan' => $this->input->post('tujuan'),
				'penjemputan' => $this->input->post('jemput'),
				'mobil' => $this->input->post('mobil'),
				'driver' => $this->input->post('driver'),
				'assign_by' => $user,
				'STATUS' => 'ON'

			);


			$this->massignorder->update_crud($this->input->post('unik'), $update_data);
			echo 'ubah';
		}
	}



	function fetch_single_user()
	{
		$output = array();

		$data = $this->massignorder->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['nik_user'] = $row->nik_user;
			$output['tanggal_order'] = $row->tanggal_order;
			$output['tujuan'] = $row->tujuan;
			$output['jam_jemput'] = $row->jam_jemput;
			$output['penjemputan'] = $row->penjemputan;
			$output['id_request'] = $row->id_request;
			$output['mobil'] = $row->mobil;
			$output['mobil_recomend'] = $row->mobil_recomend;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->massignorder->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	function submit_all()
	{
		$user = $this->session->userdata('user');
		$pilih = 'A';
		var_dump($user);

		$this->massignorder->submit($user, $pilih);
		echo 'masuk';
	}
	function getDriver()
	{
		$id = $this->input->post('id');
		$data = $this->massignorder->getdriver($id);
		echo json_encode($data);
	}

	function genScoring()
	{
		$this->massignorder->genScoring();
		echo 'masuk';
	}
	function initiate()
	{
		//$request_id = $this->input->post['user_id'];
		$user = $this->session->userdata('user');
		$id = $_POST['user_id'];
		$this->_cetak($id);
	}

	private function _cetak($data)
	{
		$this->load->library('CustomPDF');



		//$table = $table_ == 'stokopname' ? 'Barang Masuk' : 'Barang Keluar';

		$pdf = new FPDF();
		$pdf->AddPage('P', 'Letter');
		$pdf->SetFont('Times', 'B', 16);


		// $no = 1;
		$pdf->Image('assets/dist/img/uob.png', 10, 0, 20, 25);
		//$pdf->Cell(25, 7, '', 0, 0,  'L');

		$pdf->Ln();
		$pdf->Cell(25, 0, '', 0, 0,  'L');
		$pdf->Cell(155, 0, 'PT Bank UOB Indonesia', 0, 0, 'L');
		$pdf->Ln();
		$pdf->SetFont('Times', 'B', 10);
		$pdf->Cell(25, 7, '', 0, 0,  'L');
		$pdf->Cell(155, 7, 'HEAD OFFICE, UOB Plaza, JL. MH. Thamrin No 10. Jakarta 19230', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(25, 1, '', 0, 0,  'L');
		$pdf->Cell(155, 1, 'Tlp :(021) 2350 6000 | Fax :(021) 2993 6632', 0, 0, 'L');

		$pdf->SetLineWidth(1);
		$pdf->Line(10, 22, 210, 22);
		$pdf->SetLineWidth(0);
		$pdf->Line(10, 23, 210, 23);
		$pdf->Ln(10);
		$pdf->SetFont('Times', 'B', 14);
		$pdf->Cell(190, 7, 'HASIL SCORING SISTEM PENDUKUNG KEPUTUSAN ', 0, 0,  'C');
		$pdf->SetLineWidth(0);
		$pdf->Line(50, 33, 160, 33);
		$pdf->Ln(5);
		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(190, 7, 'Metode : Simple Addictive Weighting (SAW)', 0, 0,  'C');
		$pdf->Ln(20);

		$header = $this->massignorder->getRequest($data);
		$no = 1;



		$pdf->SetFont('Times', '', 10);
		$pdf->Cell(60, 7, 'Data yang akan di uji adalah sebagai berikut :', 0, 0, 'C');
		$pdf->Ln(7);
		$pdf->Cell(20, 7, 'No. Order', 1, 0, 'C');
		$pdf->Cell(20, 7, 'Jmlh. Order', 1, 0, 'C');
		$pdf->Cell(50, 7, 'Nama Karyawan', 1, 0, 'C');
		$pdf->Cell(40, 7, 'Jabatan', 1, 0, 'C');
		$pdf->Cell(40, 7, 'Divisi', 1, 0, 'C');

		$pdf->Cell(15, 7, 'Hari', 1, 0, 'C');
		$pdf->Cell(20, 7, 'Penumpang', 1, 0, 'C');

		$pdf->Ln();
		$pdf->Cell(20, 7, $data, 1, 0, 'C');
		$countOrder = $this->massignorder->getCountOrder($data);
		foreach ($countOrder as $co) {
			$pdf->Cell(20, 7, $co['Total'], 1, 0, 'C');
		}
		$dataEmployee = $this->massignorder->getDataEmployee($data);
		foreach ($dataEmployee as $dataEmployee) {
			$pdf->Cell(50, 7, $dataEmployee['EMPLOYEE_NAME'], 1, 0, 'C');
			$g = $this->massignorder->getDataDetail('CD_TITLE', $dataEmployee['CD_TITLE']);
			foreach ($g as $g) {
				$pdf->Cell(40, 7, $g['DESCRIPTION'], 1, 0, 'C');
			}
			$h = $this->massignorder->getDataDetail('CD_DIVISI', $dataEmployee['CD_DESIGN']);
			foreach ($h as $h) {
				$pdf->Cell(40, 7, $h['DESCRIPTION'], 1, 0, 'C');
			}
		}


		$dataOrder = $this->massignorder->getDataOrder($data);
		foreach ($dataOrder as $dataOrder) {
			$pdf->Cell(15, 7, $dataOrder['tot_day'], 1, 0, 'C');
			$pdf->Cell(20, 7, $dataOrder['tot_people'], 1, 0, 'C');
		}



		$pdf->Ln(10);

		$pdf->Cell(100, 7, 'Kriteria Penilaian adalah sebagai berikut ', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(100, 7, '1. C1 = Total Order', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(100, 7, '2. C2 = Jabatan Karyawan', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(100, 7, '3. C3 = Total Hari pemakaian mobil', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(100, 7, '4. C4 = Divisi Karyawan', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(100, 7, '5. C5 = Jumlah Penumpang', 0, 0, 'L');
		$pdf->Ln(10);

		$pdf->Cell(100, 7, 'Dari data di atas maka dapat di ubah menjadi sebagai berikut :', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(15, 7, 'Mobil', 1, 0, 'C');
		foreach ($header as $d) {
			$pdf->Cell(10, 7, $this->convertKriteria($d['NO_SR']), 1, 0,  'C');
		}
		$pdf->Ln();
		$car = $this->massignorder->getDataCar();

		foreach ($car as $c) {
			$pdf->Cell(15, 7, $c['DESCRIPTION'], 1, 0, 'L');
			$carvalue = $this->massignorder->getDataWithCar($c['CODE'], $data);
			$no = 1;
			foreach ($carvalue as $carvalue) {

				$pdf->Cell(10, 7, $carvalue['VALUE'], 1, 0,  'C');
			}
			$pdf->Ln();
		}
		$pdf->Cell(15, 7, '', 0, 0, 'C');
		$pdf->Cell(15, 7, 'Tabel 1 - Hasil Konversi', 0, 0, 'C');

		$pdf->Ln(10);
		$pdf->Cell(15, 7, 'Mobil', 1, 0, 'C');
		foreach ($header as $e) {
			$pdf->Cell(30, 7,  $this->convertKriteria($e['NO_SR']), 1, 0,  'C');
		}
		$pdf->Ln();
		$car1 = $this->massignorder->getDataCar();

		foreach ($car1 as $c1) {
			$pdf->Cell(15, 7, $c1['DESCRIPTION'], 1, 0, 'L');
			$carvalue1 = $this->massignorder->getDataWithCar($c1['CODE'], $data);
			$no = 1;
			foreach ($carvalue1 as $carvalue1) {

				$pdf->Cell(30, 7, number_format($carvalue1['MAX_VALUE'], 4), 1, 0,  'C');
			}
			$pdf->Ln();
		}

		$pdf->Cell(15, 7, '', 0, 0, 'C');
		$pdf->Cell(15, 7, 'Tabel 2 - Hasil Normalisasi', 0, 0, 'C');

		$pdf->Ln(10);
		$pdf->Cell(15, 7, 'Mobil', 1, 0, 'C');
		$car2 = $this->massignorder->getRanking($data);
		foreach ($car2 as $c2) {
			$car4 = $this->massignorder->getDataDetail('CD_CAR', $c2['CD_CAR']);
			foreach ($car4 as $c4) {
				$pdf->Cell(30, 7, $c4['DESCRIPTION'], 1, 0, 'C');
			}
		}
		$pdf->Ln();

		$pdf->Cell(15, 7, 'Nilai', 1, 0, 'C');
		$car3 = $this->massignorder->getRanking($data);

		foreach ($car3 as $c3) {
			$pdf->Cell(30, 7, number_format($c3['RANK_VALUE'], 4), 1, 0,  'C');
		}
		$pdf->Ln();
		$pdf->Cell(15, 7, 'Rank', 1, 0, 'C');

		for ($i = 1; $i <= 4; $i++) {
			$pdf->Cell(30, 7, $i, 1, 0,  'C');
		}


		$pdf->Ln();
		$pdf->Cell(15, 7, '', 0, 0, 'C');
		$pdf->Cell(15, 7, 'Tabel 3 - Hasil Ranking', 0, 0, 'C');

		$file_name = 'hasil_scoring_';
		$pdf->Output('I', $file_name);
	}

	function convertKriteria($data)
	{
		if ($data == 1) {
			$hasil = 'C1';
		} elseif ($data == 2) {
			$hasil = 'C2';
		} elseif ($data == 3) {
			$hasil = 'C3';
		} elseif ($data == 4) {
			$hasil = 'C4';
		} elseif ($data == 5) {
			$hasil = 'C5';
		}

		return $hasil;
	}
}
