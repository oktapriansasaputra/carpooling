<footer class="main-footer">

    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="<?php echo base_url('assets/plugins/jquery/jquery-3.5.1.js'); ?>"></script>

<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables/1.10.22/jquery.dataTables.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables-bs4/js/1.10.22/dataTables.bootstrap4.min.js'); ?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-rowreorder/js/1.2.7/dataTables.rowReorder.min.js'); ?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-responsive/js/2.2.6/dataTables.responsive.min.js'); ?>"></script>

<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>



<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js'); ?>"></script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        var dataTable = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "searching": false,
            "lengthChange": false,
            "language": {
                "infoFiltered": ""
            },
            "order": [],

            "ajax": {
                url: "<?php echo base_url() . 'kriteria/SubKriteria/fetch_user'; ?>",
                type: "POST",
                "data": function(data) {
                    data.kriteriaSearch = $('#kriteriaSearch').val();
                    data.carSearch = $('#carSearch').val();


                }

            },

            "columnDefs": [{
                "target": [0],
                "orderable": false
            }]

        });
        $('#btn-filter').click(function() { //button filter event click
            dataTable.ajax.reload(); //just reload table
        });

        $('#btn-reset').click(function() { //button reset event click
            $('#form-filter')[0].reset();
            dataTable.ajax.reload(); //just reload table
        });

        $(document).on('submit', '#user_form', function(event) {
            event.preventDefault();


            $.ajax({
                url: "<?php echo base_url() . 'kriteria/SubKriteria/user_action'; ?>",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 'masuk') {

                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'Data telah berhasil di tambahkan !',
                            showConfirmButton: false,
                            timer: 1500
                        }) // end sweet alert
                    } else if (response == 'ubah') {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'Data telah berhasil di ubah !',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else if (response == 'null') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Claim Transport',
                            text: 'Nama Karyawan tidak ada di absen',
                            showConfirmButton: false,
                            timer: 1500
                        }) //end if
                    } else if (response == 'exist') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Claim Transport',
                            text: 'Claim Transport sudah pernah di ajukan',
                            showConfirmButton: false,
                            timer: 1500
                        }) //end if
                    }
                    $('#user_form')[0].reset();
                    $('#modal-lg').modal('hide');
                    $('#action').val("Add");
                    $('.modal-title').text("Form Transport");
                    dataTable.ajax.reload();
                }
            });


        });

        $(document).on('click', '.edit', function(event) {
            var user_id = $(this).attr('id');
            $.ajax({
                url: "<?php echo base_url() . 'kriteria/SubKriteria/fetch_single_user'; ?>",
                method: "POST",
                data: {
                    user_id: user_id
                },
                dataType: "json",
                success: function(data) {

                    $('#modal-lg').modal('show');

                    //$('#nim').attr('disabled','disabled');
                    $('#unik').val(data.id_subkriteria);
                    $('#id_kriteria').val(data.id_kriteria);
                    $('#id_subkriteria').val(data.nama_subkriteria);
                    $('#nilai').val(data.value);

                    $('#mobil').val(data.cd_car);

                    $('.modal-title').text("Edit Sub Kriteria");
                    $('#action').val("Edit");

                }
            })
        });

        $(document).on('click', '.close', function(event) {
            var user_id = $(this).attr('id');
            $('#user_form')[0].reset();
            $('#modal-lg').modal('hide');
            $('#action').val("Add");
            $('#nim').removeAttr('disabled');
            //$('.modal-title').reset();
            $('.modal-title').text("Tambah Sub Kriteria");

        });

        $(document).on('click', '.delete', function(event) {
            event.preventDefault();

            var user_id = $(this).attr('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "Data user akan di hapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus Data!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "<?php echo base_url() . 'kriteria/SubKriteria/delete_single_user'; ?>",
                        method: "POST",
                        data: {
                            user_id: user_id
                        },
                        success: function(data) {
                            dataTable.ajax.reload();
                            Swal.fire({
                                icon: 'success',
                                title: 'Data User',
                                text: 'Data telah berhasil di hapus !',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    })


                } // end if
            }) //end sweetalert

        });


        $(document).on('submit', '#formPassword', function(event) {
            event.preventDefault();

            $.ajax({
                url: "<?php echo base_url() . 'login/NewPassword'; ?>",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 'ubah') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Password',
                            text: 'Password telah berhasil di ubah !',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#modal-default').modal('hide');
                    } else if (response == 'beda') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Password',
                            text: 'Password tidak sama !',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else if (response == 'null') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Password',
                            text: 'Password kosong !',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } //end if

                }
            });


        });

        $('#user_form').validate({
            rules: {
                karyawan: {
                    required: true
                },
                mobil: {
                    required: true

                },
                warna: {
                    required: true
                },
                platno: {
                    required: true
                },

                aktif: {
                    required: true
                },
            },
            messages: {
                karyawan: {
                    required: "Nama karyawan silahkan di isi"
                },
                mobil: {
                    required: "Pilih salah satu mobil"

                },
                warna: {
                    required: "Pilih salah satu warna mobil"
                },
                platno: {
                    required: "Plat No Harap di isi"
                },
                aktif: {
                    required: "Please choose status user"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        $('#id_kriteria').change(function() { //button filter event click
            var id = $(this).val();
            $.ajax({
                url: "<?php echo base_url() . 'kriteria/SubKriteria/getSubCuy'; ?>",
                method: "POST",
                data: {
                    id: id
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].CODE + '>' + data[i].DESCRIPTION + '</option>';
                    }
                    $('#id_subkriteria').html(html);
                }
            });
        });

    });
</script>
</body>

</html>