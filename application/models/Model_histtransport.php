<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_histtransport extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "t_transport";
	var $select_column = array ("TRANSPORT_ID","NIK","(select EMPLOYEE_NAME from m_employee 
		where m_employee.CD_EMPLOYEE =t_transport.NIK ) as EMPLOYEE_NAME",
		"TGL_REIMBURSE","STATUS","(select SALARY_VALUE from m_salary where m_salary.CD_EMPLOYEE = t_transport.NIK and CD_SALARY ='005') AS VALUE");
	var $order_column = array(null,"NIK","EMPLOYEE_NAME","TGL_REIMBURSE",NULL);

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('FLAG_SUBMIT','Y');
			
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("TGL_REIMBURSE", $_POST["search"]["value"]);
		$this->db->like("NIK", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("NIK","TGL_REIMBURSE");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("TRANSPORT_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("TRANSPORT_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("TRANSPORT_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cekAbsen($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekExistClaimTransport($cdUser,$tglAbsen){
		
		$this->db->where('NIK',$cdUser);
		$this->db->where('TGL_REIMBURSE',$tglAbsen);
		$query = $this->db->get($this->table);
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}
}
