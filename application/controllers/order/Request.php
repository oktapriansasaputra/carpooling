<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	function __construct(){
		parent::__construct();

		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}
	}

	public function index()
	{	
		$this->load->model('model_admin');	

		if(get_cookie('lang_is')==='EN'){
            // $this->db->where('NO_SR','EN');
            $id='EN';
        }elseif (get_cookie('lang_is')==='MY') {
            // $this->db->where('NO_SR','MY');
            $id='MY';
        
        }else{
            // $this->db->where('NO_SR','IN');
            $id='IN';
    	}

    	$user=$this->session->userdata('user');

		$data = array(
                'title' => 'TERAS  |  Laboratary Information System',
				'bahasa' =>$this->model_admin->ambilbahasa('LANG'),
				'def_bahasa' =>$this->model_admin->ambilbahasa('DEF_LANG'),
				'dashboard' =>$this->model_admin->ambilDeskiprsiAll('DSB_LANG',$id),
				'pilih_bahasa'=>$id,
				'get_current_user'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->model_admin->ambiluserbyid('m_user',$user),
                'cboAktif'=>$this->model_admin->select_cbo_parameter('FLAG_AKTIF'),
				'cboRequest'=>$this->model_admin->select_cbo_parameter('STATUS_REQUEST'),
				'user_req'=>$user


            );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_request',$data);
		$this->load->view('absen/footer_request');
	}

	

	function fetch_request(){

		$this->load->model('Model_request');	
		$fetch_data= $this->Model_request->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->id_request;
			$sub_array[] = $row ->nik_user;
			$sub_array[] = $row ->tanggal_order;
            $sub_array[] = $row ->tujuan;
			
			
			switch ($row ->status) {
				case "REQ":
				 $sub_array[] = '<span class="badge badge-success">Request</span>';
				 break;
				 case "PEN":
				 $sub_array[] =  '<span class="badge badge-danger">Pending</span>';
				 break;
				 case "ACC":
					$sub_array[] =  '<span class="badge badge-danger">Accept</span>';
					break;
		 }
			
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->id_request.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="'. $row ->id_request.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->Model_request->get_all_data(),
			"recordsFiltered"=>$this->Model_request->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'tanggal_order'=>$this->input->post('tanggal'),
				'nik_user'=>$this->input->post('nik'),
				'tujuan'=>$this->input->post('tujuan'),
				'penjemputan'=>$this->input->post('penjemputan'),
				'keterangan'=>$this->input->post('keterangan'),
				'status'=>'REQ'
			);
			$this->load->model('model_request');
			$this->model_request->insert_crud($insert_data);
			echo 'data inserted successfully';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'PARAMETER'=>$this->input->post('paramater'),
				'CODE'=>$this->input->post('code'),
				'DESCRIPTION	'=>$this->input->post('description'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			$this->load->model('model_request');
			$this->model_request->update_crud($this->input->post('unik'),$update_data);
			echo 'data updates successfully';
		}
	}

	function fetch_single_user(){
		$output = array();
		$this->load->model('model_request');	
		$data =$this->model_jurusan->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['PARAMETER'] = $row->PARAMETER;
			$output['CODE'] = $row->CODE;
			$output['DESCRIPTION'] = $row->DESCRIPTION;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
			$output['NO_SR'] = $row->NO_SR;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->load->model('model_request');	
		$this->model_request->delete_single_user($_POST['user_id']);
		echo "data telah dihapus";
	}

	
}
