  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">

      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
          <div class="container-fluid">
              <!-- Small boxes (Stat box) -->


              <div class="row">
                  <div class="col-md-12">
                      <div class="card">
                          <div class="card-header">
                              <h3 class="card-title"><strong><?php foreach ($cboMenu as $isiMenu) {
                                                                    echo $isiMenu['DESCRIPTION'];
                                                                } ?></strong></h3>
                              <div align='right'>

                                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info " data-toggle="tooltip" data-placement="top" title="Request Overtime"><i class="fas fa-plus-circle"></i> Baru</button>
                              </div>
                          </div>
                          <!-- /.card-header -->
                          <div class="panel panel-default">


                              <div class="card-body">
                                  <form id="form-filter" class="form-horizontal">
                                      <div class="row">
                                          <div class="col-sm-1">
                                              <label>Search :</label>
                                          </div>
                                          <div class="col-sm-4">
                                              <select name="carSearch" id="carSearch" class="form-control ">
                                                  <option value="">-Pilih Mobil-</option>
                                                  <?php foreach ($getCar as $car) { ?>
                                                      <option value="<?php echo $car['CODE']; ?>">
                                                          <?php echo $car['DESCRIPTION']; ?></option>
                                                  <?php  } ?>
                                              </select>
                                          </div>
                                          <div class="col-sm-4">
                                              <select name="kriteriaSearch" id="kriteriaSearch" class="form-control ">
                                                  <option value="">-Pilih Kriteria-</option>
                                                  <?php foreach ($getKriteria as $k) { ?>
                                                      <option value="<?php echo $k['CODE']; ?>">
                                                          <?php echo $k['DESCRIPTION']; ?></option>
                                                  <?php  } ?>
                                              </select>
                                          </div>
                                          <div class="col-sm-3">
                                              <button type="button" id="btn-filter" class="btn btn-primary"><i class="fas fa-search"></i> Filter</button>
                                              <button type="button" id="btn-reset" class="btn btn-default"><i class="fas fa-undo"></i> Reset</button>
                                          </div>
                                      </div>

                                  </form>
                                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                                      <thead>
                                          <tr>
                                              <th>No</th>
                                              <th>Mobil</th>
                                              <th>Nama Kriteria</th>
                                              <th>Bobot (%)</th>
                                              <th>Sifat</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>

                                  </table>
                              </div>
                              <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                      </div>
                  </div>
                  <!-- /.col -->
                  <!-- ./col -->
              </div>





              <!-- /.row (main row) -->
          </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
      <div class="modal-dialog">
          <form method="POST" id="user_form">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title">Tambah Kriteria Baru</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Mobil</label>
                          <select name="mobil" id="mobil" class="form-control">
                              <option value="">-- Pilih Mobil --</option>
                              <?php foreach ($getCar as $getCar) { ?>
                                  <option value="<?php echo $getCar['CODE']; ?>">
                                      <?php echo $getCar['DESCRIPTION']; ?></option>
                              <?php  } ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Jenis Kriteria</label>
                          <input type="hidden" class="form-control" name="action" id="action" value="Add">
                          <input type="hidden" class="form-control" name="unik" id="unik">
                          <select name="id_kriteria" id="id_kriteria" class="form-control">

                              <?php foreach ($getKriteria as $getKriteria) { ?>
                                  <option value="<?php echo $getKriteria['CODE']; ?>">
                                      <?php echo $getKriteria['DESCRIPTION']; ?></option>
                              <?php  } ?>
                          </select>

                      </div>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Nilai Bobot</label>
                          <input type="text" name="nilai" class="form-control" id="nilai">
                      </div>

                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Sifat Kriteria</label>
                          <select name="spk" id="spk" class="form-control">
                              <option value="">-- Pilih Sifat Kriteria --</option>
                              <?php foreach ($getSPK as $getSPK) { ?>
                                  <option value="<?php echo $getSPK['CODE']; ?>">
                                      <?php echo $getSPK['DESCRIPTION']; ?></option>
                              <?php  } ?>
                          </select>
                      </div>



                  </div>
                  <div class="modal-footer">
                      <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
                      <input type="submit" name="action" class="btn btn-success" value="Submit" />
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>


                  </div>

              </div>
          </form>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->