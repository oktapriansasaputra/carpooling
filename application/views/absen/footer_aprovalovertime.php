x<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>

  <!-- jquery-validation -->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js');?>"></script>
<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
<script type="text/javascript"> 
  var save_method; //for save method string
var table;
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'approvertime/fetch_overtime';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[-1],
          "orderable" :false
      }]
  });


  

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('is-invalid'); // clear error class

     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('#paramater').removeAttr('disabled');
     $('.modal-title').text("Form Overtime");
  });

  $(document).on('click','.delete',function(event){
    event.preventDefault();
    var user_id = $(this).attr('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "Permohonan Data Overtime Karyawan",
      icon: 'warning',
      showDenyButton: true,
      confirmButtonColor: '#3085d6',
      denyButtonColor: '#d33',
      confirmButtonText: 'Approve',
      denyButtonText:'Reject'
    }).then((result) => {
      if (result.isConfirmed) {
         $.ajax({
          url:"<?php echo base_url().'approvertime/aproval';?>",
          method:"POST",  
          data:{user_id:user_id},
          success:function(data)
          {             
            dataTable.ajax.reload();
            Swal.fire({
                  icon: 'success',
                  title: 'Permohonan Overtime',
                  text: 'Permohonan di approve!',
                  showConfirmButton: false,
                  timer: 1500
                }) 
           }  
        })
       
      }else if(result.isDenied) {
        $.ajax({
          url:"<?php echo base_url().'approvertime/reject';?>",
          method:"POST",  
          data:{user_id:user_id},
          success:function(data)
          {             
            dataTable.ajax.reload();
            Swal.fire({
                  icon: 'success',
                  title: 'Permohonan Overtime',
                  text: 'Permohonan di tolak!',
                  showConfirmButton: false,
                  timer: 1500
                }) 
           }  
        })
      }// end if
    })//end sweetalert

    
  });
  $('#user_form').validate({
    rules: {
      nik: {
        required: true,
      },
      tgl_overtime: {
        required: true        
      },
      jamMulai: {
        required: true        
      },
       
       jamSelesai: {
        required: true
      },
    },
    messages: {
      nik: {
        required: "Please choose your nama karyawan"
      },
      tgl_overtime: {
        required: "Please enter your overtime date"
      },
      
      jamMulai: {
        required: "Please enter your start time"
      },
      jamSelesai: {
        required: "Please enter your end time"
      }
    },
    errorElement: 'span',
    errorClass: "help-block",
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });




});
    
   
 
  
</script>

</body>
</html>