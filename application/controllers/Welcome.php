<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	// ************************************************//
	// Contoller dashboad                              //
	// fungsi :                                        //
	// 1. Mengelola dashboard                         //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		

		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
			//echo $this->session->userdata('status');
		}
	}

	public function index()
	{	
				
    	$user=$this->session->userdata('user');
    	$results =$this->madmin->get_chart_data();
		$data = array(
                			
				'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05'),
				'cTotEmployee' => $this->madmin->count_article('t_report_order'),
				'cTotEmployeePermanent' =>$this->madmin->countOrderByStatus('t_report_order','FINISH_ORDER',1),
				'cTotEmployeeContract' =>$this->madmin->countOrderByStatus('t_report_order','CANCEL_ORDER',1),
				'cTotEmployeeMagang' =>$this->madmin->countOrderByStatus('t_report_order','PENDING_ORDER',1),
				'year_list' => $this->madmin->fetch_year(),
				'chart_data'=>$results['chart_data'],
				'get_bulan'=>$this->madmin->fetch_bulan(),
				'get_mobil'=>$this->madmin->fetch_mobil(),
		
		  );

		
        //$data['min_year'] = $results['min_year'];
        //$data['max_year'] = $results['max_year'];

		//echo $this->session->userdata('status');
				// $this->load->view('header',$data);
				//  $this->load->view('home',$data);
				//  $this->load->view('footer');
		$member = $this->madmin->select_cbo_parameter('MEMBER');
		foreach ($member as $member) {
			if ($member['CODE']=='MCL'){
				$this->load->view('header',$data);
				 $this->load->view('home',$data);
				 $this->load->view('footer');
			}elseif ($member['CODE']=='RDN') {
				$this->load->view('header',$data);
				 $this->load->view('home',$data);
				 $this->load->view('footer');
			}
				
			
		}
			
		
	}

	public function fetch_data()
	 {
	  if($this->input->post('year'))
	  {
	   $chart_data = $this->madmin->fetch_chart_data($this->input->post('year'));
	   
	   foreach($chart_data->result_array() as $row)
	   {
	    $output[] = array(
	     'month'  => $row["bulan"] ,
	     'total'  => $row["total"] 
	    );
	   }
	   echo json_encode($output);
	  }
	 }
	
}
