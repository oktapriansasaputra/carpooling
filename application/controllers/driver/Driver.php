<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller {

	// ************************************************//
	// Contoller Driver                                  //
	// fungsi :                                        //
	// 1. Menambah Driver baru                           //
	// 2. Mengubah password,grup user yang ada         //
	// 3. Menghapus User                               //
	//												   //	 
	// Created by   : Okta Priansa Saputra                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email oktapriansasaputra@gmail.com              //
	// ************************************************//


	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('model_driver','mdriver');
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'driver/driver');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}

		
	}

	public function index()
	{	
				
    	$user=$this->session->userdata('user');
    	$group = $this->session->userdata('group');

		$data = array(
   		'get_current_user'=> $this->madmin->ambiluserbyid('m_user', $user),
		'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
		'cboGroup'=>$this->madmin->select_cbo_parameter('GRP_USR'),
		'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
		'cboKaryawan'=>$this->madmin->getEmployeDriver(),
		'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'driver/driver'),
		'cboCabang'=>$this->madmin->getCabang(),
		'cboMobil'=>$this->madmin->select_cbo_parameter('CD_CAR'), 
		'cboWarna'=>$this->madmin->select_cbo_parameter('CD_COLOUR'),
		'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
		'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
		'getColorSidebar'=> $this->madmin->getSettingById('OP05')


    	);
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('driver/view_driver',$data);
		$this->load->view('driver/footer_driver');
	}

	

	function fetch_user(){

		$fetch_data= $this->mdriver->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->CD_EMPLOYEE;
			$sub_array[] = $row ->EMPLOYEE_NAME;
			$sub_array[] = $row ->CD_CAR;
			$sub_array[] = $row ->CD_COLOUR;
			$sub_array[] = $row ->POLICE_NO;
						
			
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->ID_DRIVER.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="'. $row ->ID_DRIVER.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mdriver->get_all_data(),
			"recordsFiltered"=>$this->mdriver->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'CD_EMPLOYEE'=>$this->input->post('karyawan'),
				'CD_CAR	'=>$this->input->post('mobil'),
				'CD_COLOUR'=>$this->input->post('warna'),
				
				'POLICE_NO'=>$this->input->post('platno')
			);
			$hasilCek =$this->madmin->cekExist('CD_EMPLOYEE',$this->input->post('karyawan'),'m_driver');
			if($hasilCek <> true){
				$this->mdriver->insert_crud($insert_data);
				echo 'masuk';
			}else{
				echo 'exist';
			}

			
			
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'CD_EMPLOYEE'=>$this->input->post('karyawan'),
				'CD_CAR	'=>$this->input->post('mobil'),
				'CD_COLOUR'=>$this->input->post('warna'),
				
				'POLICE_NO'=>$this->input->post('platno')
			);
			$this->load->model('model_crud');
			$this->mdriver->update_crud($this->input->post('unix'),$update_data);
			echo 'ubah';
		}
	}

	function fetch_single_user(){
		$output = array();
			
		$data =$this->mdriver->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			
			$output['CD_EMPLOYEE'] = $row->CD_EMPLOYEE;
			$output['CD_CAR'] = $row->CD_CAR;
			$output['CD_COLOUR'] = $row->CD_COLOUR;
			$output['POLICE_NO'] = $row->POLICE_NO;
			$output['ID_DRIVER'] = $row->ID_DRIVER;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
			
		$this->mdriver->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	

	
}
// ウェンディバユ作成 //
