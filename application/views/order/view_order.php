  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->


        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong><?php foreach ($cboMenu as $isiMenu) {
                                                  echo $isiMenu['DESCRIPTION'];
                                                } ?></strong></h3>
                <div align='right'>
                  <button type="button" id="submitall" name="submitall" class="btn btn-info " data-toggle="tooltip" data-placement="top" title="Submit All"><i class="fas fa-circle-notch"></i> Proses</button>
                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info " data-toggle="tooltip" data-placement="top" title="Request Overtime"><i class="fas fa-plus-circle"></i> Baru</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">


                <div class="card-body">

                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIK</th>
                        <th>Nama Karyawan</th>
                        <th>Tgl Request</th>
                        <th>Tujuan</th>
                        <th>Penjemputan</th>
                        <th>Jam Jemput</th>
                        <th>Selesai</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col -->
          <!-- ./col -->
        </div>





        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form method="POST" id="user_form">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Form Order</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Nama Karyawan</label>
              <input type="hidden" class="form-control" name="action" id="action" value="Add">
              <input type="hidden" class="form-control" name="unik" id="unik">
              <select name="nik" id="nik" class="form-control">

                <?php foreach ($cboEmployee as $isiTitle) { ?>
                  <option value="<?php echo $isiTitle['CD_EMPLOYEE']; ?>">
                    <?php echo $isiTitle['EMPLOYEE_NAME']; ?></option>
                <?php  } ?>
              </select>

            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Tanggal Request</label>
              <input type="date" name="tgl_overtime" class="form-control" id="tgl_overtime">
            </div>
            <div class="row">
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Tempat Tujuan</label>
                <input type="text" class="form-control" id="tujuan" name="tujuan">
              </div>
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Penjemputan</label>
                <input type="text" class="form-control" id="jemput" name="jemput">
              </div>
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Jam Jemput</label>
                <input type="time" class="form-control" id="jamJemput" name="jamJemput">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-6">
                <label for="recipient-name" class="col-form-label">Total Hari</label>
                <input type="text" class="form-control" id="totHari" name="totHari">
              </div>
              <div class="form-group col-md-6">
                <label for="recipient-name" class="col-form-label">Total Orang</label>
                <input type="text" class="form-control" id="totOrang" name="totOrang">
              </div>
            </div>



          </div>
          <div class="modal-footer">
            <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
            <input type="submit" name="action" class="btn btn-success" value="Submit" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>


          </div>

        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->