<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_task extends CI_Model {

	// ************************************************//
	// Model Model_task                                //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// email        : wendy.bayu@gmail.com             //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	// var $user=$this->session->userdata('user');
	var $table = "t_task";
	var $select_column = array ("TASK_ID","TYPE_TASK","DESCRIPTION_TASK",
		"(select CUSTOMER_NAME from m_customer where m_customer.CUSTOMER_ID= t_task.CUSTOMER_ID)AS CUSTOMER_NAME","(select CUSTOMER_ADDRESS from m_customer where m_customer.CUSTOMER_ID= t_task.CUSTOMER_ID) as CUSTOMER_ADDRESS","(select (select DESCRIPTION from m_gcm where PARAMETER ='CITY' and m_gcm.CODE = m_customer.CITY and FLAG_ACTIVE='Y' ) from m_customer where m_customer.CUSTOMER_ID= t_task.CUSTOMER_ID)as CITY","(select NO_TLP from m_customer where m_customer.CUSTOMER_ID= t_task.CUSTOMER_ID)as NO_TLP",
		"STATUS");
	var $order_column = array("TASK_ID","TYPE_TASK","DESCRIPTION_TASK",
		"USER_SENDER","SENDER_ADDRESS","USER_RECEIPT","RECEIPT_ADDRESS",
		null);

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('FLAG_SUBMIT','N');
		$this->db->where('STATUS',null);
		if ($userGroupIn <>'SUPERADMIN'){
			$this->db->where('USER_CREATED',$userLoginIn);
		}
		
		
			
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("TASK_ID", $_POST["search"]["value"]);
			$this->db->like("DESCRIPTION_TASK", $_POST["search"]["value"]);
		
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("TASK_ID");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("TASK_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("TASK_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("TASK_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cekOvertime($nik,$tglOvertime){
		
		$this->db->where('NIK',$nik);
		$this->db->where('TGL_OVERTIME',$tglOvertime);
		$query = $this->db->get('t_overtime');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekDataBeforeSubmitOvertime($user){
		
		$sql = "select	TASK_ID from t_task
			where FLAG_SUBMIT='N' and USER_CREATED = ? ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}


	function cekExistAbsenBeforeClaim($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}
	}

	function submit($user,$pilih){
		$sql ="call submit_all(?,?)";
		$param = array(
			'user'=>$user,
			'pilih'=>$pilih
			
		);
		$this->db->query($sql,$param);
	}

	function getCustomer($cabang){
		$this->db->where('CABANG_ID',$cabang);
		$query = $this->db->get('m_customer');
		return $query->result_array(); 

	}
}
//69ffe55a99db23b320584d31242cab8f //