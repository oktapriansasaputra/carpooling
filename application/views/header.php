
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 2</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/fontawesome-free/css/all.min.css');?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css');?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="<?php foreach ($getColorTopbar as $getColorTopbar) { echo $getColorTopbar['SETTING_VALUE'] ; }?>">
    <!-- Left navbar links -->
   <ul class="navbar-nav">
       
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
     
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
	  <!-- Notifications Dropdown Menu -->
	  
	   
	  <li class="nav-item dropdown" align='right'>
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
            <img src="<?php echo base_url('assets/dist/img/profile.png');?>" style="width: 3%;"  class="img-circle">
		   <em><strong>Hi</strong>, <span class="namaUser"><?php foreach ($get_current_user as $get_current_user) { echo $get_current_user['NAME'] ; }?></span>
		    </em> <i class="dropdown-icon fa fa-angle-down"></i>
         </a>
         <div class="dropdown-menu dropdown-menu-right p-0"> 
          <!--<a href="<?php echo base_url('profile');?>" class="dropdown-item">
                        <i class="fa fa-user"></i> Profile</a>-->
												<a href="#" class="dropdown-item"  data-toggle="modal" data-target="#modal-default">
                        <i class="fa fa-key"></i> Change Password</a>
                        
                        <a href="<?php echo base_url('login/logout');?>" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>
                        Sign Out
                        </a> 
        </div>
      </li>
	  
	  <li class="nav-item">
      <li class="nav-item">
       <!-- <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a> -->
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
   <aside class="main-sidebar sidebar-light-indigo elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('welcome');?>" class="<?php foreach ($getColorSidebar as $getColorSidebar) { echo $getColorSidebar['SETTING_VALUE'] ; }?>">
     <img src="<?php echo base_url('assets/img/Order.svg');?>"  class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php foreach ($getTitleSidebar as $getTitleSidebar) { echo $getTitleSidebar['SETTING_VALUE'] ; }?> </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
     

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php foreach ($get_current_group as $get_current_group) { 

          echo $this->dynamic_menu->build_menu($get_current_group['GROUP_USER'],'P','Y'); 
        }?>
           
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>



  <div class="modal fade" id="modal-default" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="formPassword"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Password</h4>
              <button type="button" class="closePassword" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Password Baru</label>
                <input type="password" name="newPassword" class="form-control" id="password">
                
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Re-type password</label>
                <input type="password" class="form-control" name="retypePass" id='retypePass'>
              </div>
              
                          
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
             </div>
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>