x<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>
<script>
   
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'income/fetch_income';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[0,3,4],
          "orderable" :false
      }]
  });

  $(document).on('submit','#user_form',function(event){
      event.preventDefault();
      var paramater = $("#paramater").val();
      var nosr = $("#nosr").val();

      if(paramater !='' && nosr !='')
      {
        $.ajax({
          url:"<?php echo base_url().'income/user_action';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(data)
          {
            alert(data);
            $('#user_form')[0].reset();
            $('#modal-lg').modal('hide');
            $('#action').val("Add");
            $('.modal-title').text("Add Income Statement");
            dataTable.ajax.reload();
          }
        });

      }else{
        alert("Both field are required !");
      }
  });

  $(document).on('click','.edit',function(event){
    var user_id = $(this).attr('id');
    $.ajax({
       url:"<?php echo base_url().'income/fetch_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          dataType:"json",
          success:function(data)
          {
            
            $('#modal-lg').modal('show');
            $('#stock').val(data.STOCK_CODE);
            $('#quarter').val(data.QUARTER);
            $('#year').val(data.YEAR);
             $('#totalSales').val(data.TOTAL_SALES);
             $('#cogs').val(data.COGS);
             $('#grossProfit').val(data.GROSS_PROFIT);
             $('#opex').val(data.OPEX);
            $('#ebit').val(data.EBIT);
            $('#otherIncome').val(data.OTHER_INCOME);
            $('#earnBeforeTax').val(data.EARN_BEFORE_TAX);
            $('#netIncomeAfterTax').val(data.NET_INCOME_AFTER_TAX);
            $('#minorityInterest').val(data.MINORITY_INTEREST);
            $('#netIncome').val(data.NET_INCOME);
            $('#eps').val(data.EPS);
            $('#bv').val(data.BV);
            $('#closePrice').val(data.CLOSE_PRICE);
            $('#per').val(data.PER);
            $('#pbv').val(data.PBV);
             $('#unik').val(data.CODE_EQUITY);
            $('.modal-title').text("Edit Income Statement");
            $('#action').val("Edit");

          }  
        })
  });

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('.modal-title').text("Add Income Statement");
  });

  $(document).on('click','.delete',function(event){
    var user_id = $(this).attr('id');
    if(confirm("Are you sure you want to delete this ?"))
    {
      $.ajax({
       url:"<?php echo base_url().'user/delete_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          success:function(data)
          {
            
            alert(data);
            dataTable.ajax.reload();

          }  
        })
    }else{
      return false;
    }
    
  });

$("#modal-lg").tabs({
    activate: function (event, ui) {
        console.log(event);
    }
});

});
    
    
  
</script>
</body>
</html>