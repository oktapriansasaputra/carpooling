<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_hasilabsensi extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "t_absensi_detail";
	var $select_column = array ("tgl_absensi","(select m_user.NAME from m_user where m_user.CD_USER = t_absensi_detail.cd_user and m_user.CD_USER='17110110030')","(select m_gcm.DESCRIPTION from m_gcm where m_gcm.NO_SR = t_absensi_detail.CD_MATKUL and m_gcm.PARAMETER='MATKUL' )",
		"waktu_absen");
	var $order_column = array("t_absensi_detail","waktu_absen");

	
	
	function make_query(){
		

		$jurusan = $this->session->userdata('prodi');
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('CD_MATKUL',$jurusan);
		$this->db->where('SEMESTER','6');

		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CD_MATKUL", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CD_MATKUL","DESC");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert('t_absensi_detail',$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("KELAS_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("KELAS_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("KELAS_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cek_absen($user_id,$tgl,$matkul){
		$this->db->where('cd_user',$user_id);
		$this->db->where('tgl_absensi',$tgl);
		$this->db->where('CD_MATKUL',$matkul);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}
}
