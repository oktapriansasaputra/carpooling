<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setting extends CI_Model {

	
	var $table = "m_setting";
	var $select_column = array ("ID_SETTING","(select DESCRIPTION from m_gcm where PARAMETER ='OPTION' and FLAG_ACTIVE='Y' and CODE =ID_SETTING) as DESCRIPTION","SETTING_VALUE");
	var $order_column = array ("ID_SETTING","SETTING_VALUE");
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("ID_SETTING", $_POST["search"]["value"]);
			$this->db->or_like("SETTING_VALUE", $_POST["search"]["value"]);
			

		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("ID_SETTING");
		}	
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("ID_SETTING",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("ID_SETTING",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("ID_SETTING",$id);
		$this->db->delete($this->table);
		
	}

	function select_cbo_parameter($param,$aktif)
	{
		$this->db->where ("PARAMETER",$param);
		$this->db->where ("FLAG_ACTIVE",$aktif);
		$query = $this->db->get('m_gcm');
		return $query->result();
	}

	public function qetCabang()
	{
		$this->db->where('FLAG_ACTIVE','Y');
		$query =$this->db->from('m_cabang');
		return $query->result();
	}
}
