<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_kriteria extends CI_Model
{


    // var $user=$this->session->userdata('user');
    var $table = " m_kriteria";
    var $select_column = array(
        "id_kriteria", "nama_kriteria", "value", "cabang_id", "cd_car", "cd_spk"
    );
    var $order_column = array("id_kriteria", null, null, "tgl_order", "mulai", "jam_jemput", NULL);



    function make_query()
    {


        $userLoginIn = $this->session->userdata('user');
        $userGroupIn = $this->session->userdata('group');
        $nik = $this->getNik($userLoginIn);

        $this->db->select($this->select_column);
        if ($this->input->post('carSearch')) {
            $this->db->where('cd_car', $this->input->post('carSearch'));
        }
        if ($this->input->post('kriteriaSearch')) {
            $this->db->where('nama_kriteria', $this->input->post('kriteriaSearch'));
        }
        $this->db->from($this->table);





        if (isset($_POST["search"]["value"])) {
            $this->db->like("nama_kriteria", $_POST["search"]["value"]);
        }


        if (isset($_POST["order"])) {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("nama_kriteria");
        }
    }

    function make_datatable()
    {
        $this->make_query();
        if ($_POST["length"] != -1) {
            $this->db->limit($_POST["length"], $_POST["start"]);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function insert_crud($data)
    {
        $this->db->insert($this->table, $data);
    }

    function fetch_single_user($user_id)
    {
        $this->db->where("id_kriteria", $user_id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    function update_crud($id, $data)
    {
        $this->db->where("id_kriteria", $id);
        $this->db->update($this->table, $data);
    }


    function delete_single_user($id)
    {
        $this->db->where("id_kriteria", $id);
        $this->db->delete($this->table);
    }

    function cekOvertime($nik, $tglOvertime)
    {

        $this->db->where('nik_user', $nik);
        $this->db->where('tanggal_order', $tglOvertime);
        $query = $this->db->get('t_request_order');
        if ($query->num_rows() > 0) {

            return true;
        } else {
            return false;
        }
    }

    function cekDataBeforeSubmitOvertime($user)
    {

        $sql = "select nik_user from t_request_order 
			where FLAG_SUBMIT='N' and nik_user = (select CD_EMPLOYEE from m_user where CD_USER = ? ) ";

        $query = $this->db->query($sql, $user);

        if ($query->num_rows() > 0) {

            return true;
        } else {
            return false;
        }
    }


    function cekExistAbsenBeforeClaim($cdUser, $tglAbsen)
    {

        $this->db->where('CD_USER', $cdUser);
        $this->db->where('tgl_absensi', $tglAbsen);
        $query = $this->db->get('t_absensi_detail');
        if ($query->num_rows() > 0) {

            return true;
        } else {
            return false;
        }
    }

    function getNik($vpuser)
    {
        $this->db->select('CD_EMPLOYEE');
        $this->db->where('CD_USER', $vpuser);
        $query = $this->db->get('m_user');
        return $query->result();
    }

    function submit($user, $pilih)
    {
        $sql = "call submit_all(?,?)";
        $param = array(
            'user' => $user,
            'pilih' => $pilih

        );
        $this->db->query($sql, $param);
    }
    function getKriteriaName($param)
    {
        $sql = "select DESCRIPTION from m_gcm where PARAMETER = 'CD_KRITERIA' AND CODE = ? ";

        $res = $this->db->query($sql, $param);
        return $res->result_array();
    }
    function getCaraName($param)
    {
        $sql = "select DESCRIPTION from m_gcm where PARAMETER = 'CD_CAR' AND CODE = ? ";

        $res = $this->db->query($sql, $param);
        return $res->result_array();
    }
    function getSPKaName($param)
    {
        $sql = "select DESCRIPTION from m_gcm where PARAMETER = 'CD_SPK' AND CODE = ? ";

        $res = $this->db->query($sql, $param);
        return $res->result_array();
    }
}
//69ffe55a99db23b320584d31242cab8f //