  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>Cash Flow</strong></h3>
                <div align='right'> <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info ">Add</button></div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
            <tr>
              <th>No</th>
                <th>Stock</th>
                <th>Quarter</th>
                <th>Year</th>
                <th>Net Cash</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
                <th>Stock</th>
                <th>Quarter</th>
                <th>Year</th>
                <th>Net Cash</th>
                <th>Action</th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Cash Flow</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Stock Name:</label>
                <select name="stock" id="stock" class="form-control jurusan">
                 <option value="">-Pilih Saham-</option>
                 <?php foreach ($cboSaham as $isigroup) {?>
                  <option value="<?php echo $isigroup['CODE']; ?>">
                    <?php echo $isigroup['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Quarter:</label>
                <input type="text" class="form-control" name="quarter" id='quarter'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Year</label>
                <input type="text" class="form-control" name="year" id='year'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Operating Activities</label>
                <input type="text" class="form-control" name="operation" id='operation'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Investing Activities</label>
                <input type="text" class="form-control" name="investing" id='investing'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Financing Activities</label>
                <input type="text" class="form-control" name="financing" id='financing'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Net Cash Flow Activities</label>
                <input type="text" class="form-control" name="netCash" id='netCash'>
              </div>

               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Cash & equiv.Ending</label>
                <input type="text" class="form-control" name="cashEquityEnd" id='cashEquityEnd'>
              </div>

             
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->