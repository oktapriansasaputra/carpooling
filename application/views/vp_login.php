<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style_old.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.loadingModal.css');?>">
    <link rel="stylesheet" href=""<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.min.css"');?>">
    <title>Sign in & Sign up Form</title>
  </head>
  <body>
    <div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          
          <form id="signupForm" role="form" action="#" method="post" class="sign-in-form">
            <h2 class="title">Sign in</h2>
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" placeholder="Username" name="username" id="username" />
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" placeholder="Password"  name="password" id="password" />
            </div>
            <input type="submit" value="Login" class="btn solid" />
            
          </form>
          <form action="#" class="sign-up-form">
            <h2 class="title">About Us</h2>
            <p class="social-text"> UOB Indonesia didirikan pada tanggal 31 Agustus 1956 dengan nama PT Bank Buana Indonesia. Pada bulan Mei 2011, berganti nama menjadi PT Bank UOB Indonesia.
            </p>

            <p>
              Jaringan layanan UOB Indonesia mencakup 41 kantor cabang, 168 kantor cabang pembantu dan 191 ATM yang tersebar di 54 kota di 18 provinsi. Layanan UOB Indonesia juga dapat dinikmati melalui jaringan ATM regional UOB, ATM Prima, ATM bersama serta jaringan VISA.
            </p>

            <p>
              UOB Indonesia dikenal sebagai Bank dengan fokus pada layanan Usaha Kecil Menengah (UKM), layanan kepada nasabah retail, serta mengembangkan bisnis consumer dan corporate banking melalui layanan tresuri dan cash management.
            </p></p>
           
          <p class="social-text">follow our social medias</p>
            <div class="social-media">
              <a href="#" class="social-icon">
                <i class="fab fa-facebook-f"></i>
              </a>
              <a href="#" class="social-icon">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="#" class="social-icon">
                <i class="fab fa-google"></i>
              </a>
              <a href="#" class="social-icon">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </div>
          </form>
        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
            <h3>PT Bank UOB Indonesia</h3>
            <p>
            UOB Indonesia dikenal sebagai Bank dengan fokus pada layanan Usaha Kecil Menengah (UKM)...
            </p>
            <button class="btn transparent" id="sign-up-btn">
              more ...
            </button>
          </div>
          <img src="<?php echo base_url('assets/dist/img/toyota-alphard_169.png');?>" class="image" alt="" />
        </div>
        <div class="panel right-panel">
          <div class="content">
            <h3>RIGHT BY YOU ?</h3>
            <p>
              UOB Indonesia melalui program Corporate Social Responsibility turut berpartisipasi aktif membangun komunitas. Kegiatan CSR UOBI fokus kepada dunia seni, pendidikan dan anak-anak. UOBI mengadakan kompetisi seni secara regular di Indonesia melalui UOB Painting of The Year. UOBI juga mendorong karyawannya untuk ikut serta di kegiatan sukarela, antara lain melalui UOB Heartbeat, Kegiatan Donor Darah dan Donasi Buku.
            </p>
            <button class="btn transparent" id="sign-in-btn">
              Back to Login
            </button>
          </div>
          <img src="<?php echo base_url('assets/dist/img/CS.png');?>" class="image" alt="" />
        </div>
      </div>
    </div>
    <!-- <script src="<?php echo base_url('assets/plugins/jquery/jquery-3.4.1.min.js');?>"></script> -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- <script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.loadingModal.js');?>"></script>

    <script src="<?php echo base_url('assets/js/app.js');?>"></script>
     <script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
    <script type="text/javascript">

    
$(document).ready( function () {
  $('#signupForm').submit(function(){
    
    event.preventDefault();
    
    var user = document.getElementById('username').value;
    var passwd =document.getElementById('password').value;
    if (user == '' || passwd=='' ){
       Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Please enter your username & password !',
          showConfirmButton: false,
          timer: 1500
        })
      return false;
    }else{

      $.ajax({
        url:'login/ceklogin',
        method:'POST',
        data: {
          userjq :user,
          passjq :passwd 
        },
        beforeSend: function() { 
           $('body').loadingModal({
              position:'auto',
              text:'',
              color:'#fff',
              opacity:'0.7',
              backgroundColor:'rgb(0,0,0)',
              animation:'doubleBounce'
                });
        },
        complete: function(){

          $('body').loadingModal('destroy');

            },
        success:function(response){
          console.log(response);
          if (response =='success'){
            $(location).attr('href', 'welcome');
          }else{
            
            $('#username').focus();
            document.getElementById('username').value='';
            document.getElementById('password').value='';
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Please check your username / password !'
            })
          }
        },
        dataType:'text'
      });
        
          
    }

  });
});       
  </script>
  </body>
</html>
