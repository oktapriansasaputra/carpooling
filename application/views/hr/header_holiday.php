<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ;?></title>
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/dist/img/favicon-32x32.png');?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  

 <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css');?>">
  <!-- DataTables -->
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed ">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <ul class="navbar-nav " style="border-right: 1px solid ">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto" align='right' >
      
     <li class="nav-item dropdown">
				<a href="#" class="nav-link" data-toggle="dropdown" data-hover="dropdown">
                    
          <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" style="width: 10%;"  class="img-circle">
          <em><strong>Hi</strong>, <span class="namaUser"><?php foreach ($get_current_user as $get_current_user) { echo $get_current_user['NAME'] ; }?></span> </em> <i class="dropdown-icon fa fa-angle-down"></i>
        </a>
										<!-- <ul class="dropdown-menu pull-right icon-right arrow"> -->
          <div class="dropdown-menu dropdown-menu-right p-0">		
           <a href="#" class="dropdown-item">
                        <i class="fa fa-user"></i> Profile</a>
												<a href="#" class="dropdown-item"  data-toggle="modal" data-target="#modal-default">
                        <i class="fa fa-key"></i> Change Password</a>
                        
                        <a href="<?php echo base_url('login/logout');?>" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>
                        Sign Out
                        </a> 
                        
                        </div>
                      <!-- </ul> -->
      </li> 
                              <!-- /.card-tools -->
     </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('welcome');?>" class="brand-link">
      <img src="<?php echo base_url('assets/dist/img/absensiX1.png');?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AbsensiX</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
      <?php foreach ($get_current_group as $get_current_group) { 

          echo $this->dynamic_menu->build_menu($get_current_group['GROUP_USER'],'P','Y',$pilih_bahasa); 
        }?>
        </ul>
      </nav>

        
  <!-- /.sidebar-menu -->
   
    <!-- /.sidebar -->
  </aside>

  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Password</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="formPassword" action="#" method="post">
              <div class="form-group">
                        <label>Old Password</label>
                        <input type="text" class="form-control" placeholder="Old Password..." id="oldpasswd" onchange="">
              </div>
              <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control pwd" placeholder="New Password..." id="newpasswd">
                         <span class="input-group-btn">
            <button class="btn btn-default reveal" type="button"><i class="class="fa fa-fw fa-eye field-icon"></i></button>
          </span>    
                        <!-- <span  id='pass-status' toggle="#newpasswd" class="fa fa-fw fa-eye field-icon toggle-password"></spans> -->
              </div>
              <div class="form-group">
                        <label>Re-Type Password</label>
                        <input type="password" class="form-control" placeholder="Re-Type Password..." id="retypepasswd">
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>