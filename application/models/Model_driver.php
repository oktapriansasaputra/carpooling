<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_driver extends CI_Model {

	
	var $table = "m_driver";
	var $select_column = array ("ID_DRIVER","CD_EMPLOYEE","(select EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE = m_driver.CD_EMPLOYEE) as EMPLOYEE_NAME","(select DESCRIPTION from m_gcm where PARAMETER = 'CD_CAR' AND FLAG_ACTIVE = 'Y' and CODE = CD_CAR) as CD_CAR","(select DESCRIPTION from m_gcm where PARAMETER = 'CD_COLOUR' AND FLAG_ACTIVE = 'Y' AND CODE = CD_COLOUR) as CD_COLOUR","POLICE_NO");
	var $order_column = array ("ID_DRIVER","CD_EMPLOYEE","CD_CAR","CD_COLOUR","POLICE_NO");
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CD_EMPLOYEE", $_POST["search"]["value"]);
			

		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CD_EMPLOYEE");
		}	
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("ID_DRIVER",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("ID_DRIVER",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("ID_DRIVER",$id);
		$this->db->delete($this->table);
		
	}

	function select_cbo_parameter($param,$aktif)
	{
		$this->db->where ("PARAMETER",$param);
		$this->db->where ("FLAG_ACTIVE",$aktif);
		$query = $this->db->get('m_gcm');
		return $query->result();
	}

	public function qetCabang()
	{
		$this->db->where('FLAG_ACTIVE','Y');
		$query =$this->db->from('m_cabang');
		return $query->result();
	}
}
