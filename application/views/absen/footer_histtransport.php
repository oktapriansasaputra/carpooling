<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="<?php echo base_url('assets/plugins/jquery/jquery-3.5.1.js');?>"></script>

<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables/1.10.22/jquery.dataTables.min.js');?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables-bs4/js/1.10.22/dataTables.bootstrap4.min.js');?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-rowreorder/js/1.2.7/dataTables.rowReorder.min.js');?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-responsive/js/2.2.6/dataTables.responsive.min.js');?>"></script>

<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>



<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js');?>"></script>
<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
<script>

   
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "responsive": true,
      "order" :[],
      
      "ajax":{
        url:"<?php echo base_url().'histtransport/fetch_transport';?>",
        type:"POST",

      },
     
      "columnDefs":[{
          "target" :[0],
          "orderable" :false
      }]
      
  });

  
    $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('#nim').removeAttr('disabled');
     $('.modal-title').reset();
     
  });


  $(document).on('submit','#formPassword',function(event){
      event.preventDefault();
     
        $.ajax({
          url:"<?php echo base_url().'login/NewPassword';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(response)
          {
            if (response=='ubah') {
              Swal.fire({
                  icon: 'success',
                  title: 'Password',
                  text: 'Password telah berhasil di ubah !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
              $('#modal-default').modal('hide');
            }else if(response=='beda'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password tidak sama !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } else if(response=='null'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password kosong !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } //end if
             
          }
        });

      
  });

 

});
    
    
  
</script>
</body>
</html>