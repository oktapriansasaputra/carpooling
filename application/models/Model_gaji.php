<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_gaji extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "m_salary";
	var $select_column = array ("ID_SALARY","(select EMPLOYEE_NAME from m_employee 
		where m_employee.CD_EMPLOYEE =m_salary.CD_EMPLOYEE ) as CD_EMPLOYEE",
		"(select DESCRIPTION from m_gcm where PARAMETER='CD_SALARY' and CODE= CD_SALARY) as  CD_SALARY",
		"SALARY_VALUE","FLAG_ACTIVE");
	var $order_column = array(null,"CD_EMPLOYEE","CD_SALARY","SALARY_VALUE",null);

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
			
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("CD_EMPLOYEE", $_POST["search"]["value"]);
		$this->db->like("CD_SALARY", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CD_SALARY","CD_EMPLOYEE");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("ID_SALARY",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("ID_SALARY",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("ID_SALARY",$id);
		$this->db->delete($this->table);
		
	}

	function cek_absen(){
		$userLogCek=$this->session->userdata('user');
		$this->db->where('cd_user',$userLogCek);
		$this->db->where('tgl_absensi',date("Y-m-d"));
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}
}
