  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>Approval Overtime</strong></h3>
                <div align='right'>
                  
                 </div>
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">
                
            
              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Tgl Overtime</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
              <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Tgl Overtime</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
