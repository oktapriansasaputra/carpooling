<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	// ************************************************//
	// Contoller user                                  //
	// fungsi :                                        //
	// 1. Menambah User baru                           //
	// 2. Mengubah password,grup user yang ada         //
	// 3. Menghapus User                               //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//


	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('model_crud','mcrud');
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'user');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}

		
	}

	public function index()
	{	
				
    	$user=$this->session->userdata('user');
    	$group = $this->session->userdata('group');

		$data = array(
   		'get_current_user'=> $this->madmin->ambiluserbyid('m_user', $user),
		'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
		'cboGroup'=>$this->madmin->select_cbo_parameter('GRP_USR'),
		'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
		'cboKaryawan'=>$this->madmin->getEmploye(),
		'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'user'),
		'cboCabang'=>$this->madmin->getCabang(),
		'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
		'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
		'getColorSidebar'=> $this->madmin->getSettingById('OP05')

    	);
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_user',$data);
		$this->load->view('absen/footer_user');
	}

	

	function fetch_user(){

		$fetch_data= $this->mcrud->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->CD_USER;
			$sub_array[] = $row ->CD_EMPLOYEE;
			$sub_array[] = $row ->EMPLOYEE_NAME;
			$sub_array[] = $row ->CABANG_ID;
			$sub_array[] = $row ->GROUP_USER;
						
			switch ($row ->FLAG_ACTIVE) {
			       case "Y":
					$sub_array[] = '<span class="badge badge-success">Aktif</span>';
					break;
					case "N":
					$sub_array[] =  '<span class="badge badge-danger">Tidak Aktif</span>';
					break;
			}
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->CD_USER.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit </button> <button type="button" name="delete" id="'. $row ->CD_USER.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mcrud->get_all_data(),
			"recordsFiltered"=>$this->mcrud->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		$hasilCek =$this->madmin->cekExist('CD_USER',$this->input->post('nim'),'m_user');
		$hasilcekNik = $this->madmin->cekExist('CD_EMPLOYEE',$this->input->post('karyawan'),'m_user');

		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'CD_USER'=>$this->input->post('nim'),
				'CD_EMPLOYEE'=>$this->input->post('karyawan'),
				'GROUP_USER	'=>$this->input->post('group'),
				'PASSWORD'=>$this->input->post('mypassword'),
				'FLAG_ACTIVE'=>$this->input->post('aktif'),
				'CABANG_ID'=>$this->input->post('cabang')
			);
			
			if($hasilCek <> true){
				if($hasilcekNik<>true){
					$this->mcrud->insert_crud($insert_data);
					echo 'masuk';
				}else{
					echo 'nik';
				}
			
			}else{
				echo 'exist';
			}

		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'CD_EMPLOYEE'=>$this->input->post('karyawan'),
				'GROUP_USER	'=>$this->input->post('group'),
				'PASSWORD'=>$this->input->post('mypassword'),
				'FLAG_ACTIVE'=>$this->input->post('aktif'),
				'CABANG_ID'=>$this->input->post('cabang')
			);
			
				$this->mcrud->update_crud($this->input->post('nim'),$update_data);
				echo 'ubah';
			}
	}

	function fetch_single_user(){
		$output = array();
		$data =$this->mcrud->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['CD_USER'] = $row->CD_USER;
			$output['CD_EMPLOYEE'] = $row->CD_EMPLOYEE;
			$output['GROUP_USER'] = $row->GROUP_USER;
			$output['PASSWORD'] = $row->PASSWORD;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
			$output['CABANG_ID'] = $row->CABANG_ID;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		
		$this->mcrud->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	

	
}
// ウェンディバユ作成 //
