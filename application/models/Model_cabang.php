<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cabang extends CI_Model {

	
	var $table = "m_cabang";
	var $select_column = array ("CABANG_ID","NAMA_CABANG","ALAMAT_CABANG","NO_TLP","TGL_TODAY","TGL_EOD","FLAG_ACTIVE","(select EMPLOYEE_NAME from m_employee where CD_EMPLOYEE = NIK_PIC )as NIK_PIC");
	var $order_column = array (null,"ALAMAT_CABANG","TGL_TODAY","TGL_EOD",null);
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CABANG_ID", $_POST["search"]["value"]);
			$this->db->or_like("TGL_TODAY", $_POST["search"]["value"]);
			

		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CABANG_ID");
		}	
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("CABANG_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("CABANG_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("CABANG_ID",$id);
		$this->db->delete($this->table);
		
	}

	function select_cbo_parameter($param,$aktif)
	{
		$this->db->where ("PARAMETER",$param);
		$this->db->where ("FLAG_ACTIVE",$aktif);
		$query = $this->db->get('m_gcm');
		return $query->result();
	}
}
//69ffe55a99db23b320584d31242cab8f //