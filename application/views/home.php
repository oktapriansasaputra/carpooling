  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
		<div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="far fa-file-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Order</span>
                <span class="info-box-number">
                  <?php echo $cTotEmployee ;?>
                  <small></small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

		 <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="far fa-file-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Finish</span>
                <span class="info-box-number"><?php echo $cTotEmployeePermanent ;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

			<div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="far fa-file-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Cancel</span>
                <span class="info-box-number"><?php echo $cTotEmployeeContract ;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
		  <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="far fa-file-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Pending</span>
                <span class="info-box-number"><?php echo $cTotEmployeeMagang ;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
		
        <div class="row">
         <!--  <?php foreach ($dashboard as $dashboard) { ?> -->
          <div class="col-md-4 col-sm-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p></i> </p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer"> <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        <!--    <?php }?> -->
          <!-- ./col -->
          
          </div>
          <!-- ./col -->
        </div>

        
      
	  
       <!-- Main row -->
        <div class="row">
          <!-- Left col -->
           <div class="col-md-12">
                <!-- USERS LIST -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Report Monthly Order</h3>

                    <div class="card-tools">
                      <!-- <span class="badge badge-danger">8 New Members</span> -->
                      <select name="year" id="year" class="form-control">
                            <option value="">--Pilih Tahun--</option>
                        <?php
                        foreach($year_list->result_array() as $row)
                        {
                            echo '<option value="'.$row["year"].'">'.$row["year"].'</option>';
                        }
                        ?>

                        </select>
                      
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <div id="chart_area" style="width: 800px; height: 400px;"></div>
                    <div id="columnchart_material" style="width: 800px; height: 400px;"></div>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  
                  <!-- /.card-footer -->
                </div>
                <!--/.card -->
              </div>
          <!-- /.col -->

         

            
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->