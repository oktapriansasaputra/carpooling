<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AppClaimOther extends CI_Controller
{


	function __construct()
	{
		parent::__construct();

		$this->load->model('model_admin', 'madmin');
		$this->load->model('claim/model_app_claim_other', 'mClaimOther');


		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'claim/appclaimother');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title' => '',
			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboAktif' => $this->madmin->select_cbo_parameter('FLAG_AKTIF'),
			'cboEmployee' => $this->madmin->getEmployeForClaim($user, $group),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'claim/appclaimother'),
			'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
			'getColorTopbar' => $this->madmin->getSettingById('OP04'),
			'getColorSidebar' => $this->madmin->getSettingById('OP05'),
			'cboClaimOther' => $this->madmin->select_cbo_parameter('EXPENSE')
		);

		$this->load->view('absen/header_user', $data);
		$this->load->view('claim/view_app_claim_other');
		$this->load->view('claim/footer_app_claim_other');
	}



	function fetch_transport()
	{


		$fetch_data = $this->mClaimOther->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no;
			$sub_array[] = $row->EMPLOYEE_CODE;
			$sub_array[] = $row->EMPLOYEE_NAME;
			$sub_array[] = date_format(new DateTime($row->DATE_REQUEST), "d F Y");
			$sub_array[] = $row->CLAIM_OTHER_CODE;
			$sub_array[] = number_format($row->CLAIM_VALUE, 2, ".", ",");
			// switch ($row ->STATUS) {
			//        case "AP":
			// 		$sub_array[] = '<span class="badge badge-success">Approve</span>';
			// 		break;
			// 		case "RJ":
			// 		$sub_array[] =  '<span class="badge badge-danger">Reject</span>';
			// 		break;
			// 		case "ON":
			// 		$sub_array[] =  '<span class="badge badge-warning">On Progress</span>';
			// 		break;
			// }

			$sub_array[] = '<button type="button" name="delete" id="' . $row->CLAIM_OTHER_ID . '" class="btn btn-danger btn-xs delete" ><i class="far fa-question-circle"></i>
			</button>
			 ';;
			$no = $no + 1;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=> $this->mClaimOther->get_all_data(),
			"recordsFiltered" => $this->mClaimOther->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action()
	{
		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$CabangUser = $this->session->userdata('vp_cabang');
		if ($_POST["action"] == "Add") {
			$insert_data = array(
				'CLAIM_OTHER_CODE' => $this->input->post('jenisKlaim'),
				'EMPLOYEE_CODE' => $this->input->post('nik'),
				'CABANG_ID' => $CabangUser,
				'CLAIM_VALUE' => $this->input->post('nilaiKlaim'),
				'DATE_REQUEST' => $this->input->post('tglklaim'),
				'USER_CREATED' => $user,
				'DATE_CREATED' => date('Y-m-d'),
				'STATUS' => 'ON'

			);

			$hasilCek = $this->mClaimOther->cekAbsen($user, $this->input->post('tglklaim'));
			if ($hasilCek <> true) {
				echo 'null';
			} else {
				$hasilExistingClaim = $this->mClaimOther->cekExistClaimOther($this->input->post('nik'), $this->input->post('jenisKlaim'));

				if ($hasilExistingClaim <> true) {
					$this->mClaimOther->insert_crud($insert_data);
					echo 'masuk';
				} else {
					echo 'exist';
				}
			}
		}
	}



	function aproval()
	{
		$user = $this->session->userdata('user');
		$update_data = array(

			'STATUS' => 'AP',
			'APPROVED_BY' => $user,
			'APPROVED_DATE' => date('Y-m-d'),
			'FLAG_APPROVED' => 'Y'
		);
		$this->mClaimOther->update_crud($_POST['user_id'], $update_data);
	}

	function reject()
	{
		$update_data = array(

			'STATUS' => 'RJ',
			'APPROVED_BY' => $user,
			'APPROVED_DATE' => date('Y-m-d'),
			'FLAG_APPROVED' => 'Y'
		);
		$this->mClaimOther->update_crud($_POST['user_id'], $update_data);
	}
}

// ウェンディバユ作成 //	