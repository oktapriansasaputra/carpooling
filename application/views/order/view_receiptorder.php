  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong><?php foreach ($cboMenu as $isiMenu) {
                          echo $isiMenu['DESCRIPTION'];
                    }?></strong></h3>
                <div align='right'>

             
                </div>
                  
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">
                
            
              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
            <tr>
              
               <th>No Order</th>
              <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Tgl Order</th>
                <th>Tujuan</th>
                <th>Penjemputan</th>
                <th>Jam Jemput</th>               
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              
               <th>No Order</th>
              <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Tgl Order</th>
                <th>Tujuan</th>
                <th>Penjemputan</th>
                <th>Jam Jemput</th>
                <th>Action</th>
            </tr>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Form Assign</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nama Karyawan</label>
 <input type="hidden" class="form-control" name="action" id="action" value="Add">
 <input type="hidden" class="form-control" name="unik" id="unik" >
                <select name="nik" id="nik" class="form-control">
                 
                 <?php foreach ($cboEmployee as $isiTitle) {?>
                  <option value="<?php echo $isiTitle['CD_EMPLOYEE']; ?>">
                    <?php echo $isiTitle['EMPLOYEE_NAME']; ?></option>
                  <?php  }?>
               </select>

              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tanggal Order</label>
                 <input type="date" name="tgl_overtime"  class="form-control" id="tgl_overtime">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Penjemputan</label>
                <input type="text" class="form-control" id="jemput" name="jemput">
                 </div>
                 <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tujuan</label>
                <input type="text" class="form-control" id="tujuan" name="tujuan">
                 </div>
              
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nama Driver</label>

                <select name="driver" id="driver" class="form-control">
                <option > --  Pilih Driver-- </option>
                 <?php foreach ($cboDriver as $cboDriver) {?>
                  <option value="<?php echo $cboDriver['CD_EMPLOYEE']; ?>">
                    <?php echo $cboDriver['EMPLOYEE_NAME']; ?></option>
                  <?php  }?>
               </select>
              </div>
              
              
            </div>
            <div class="modal-footer">
  <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
   <input type="submit" name="action" class="btn btn-success"  value="Submit" />
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->