<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_gain extends CI_Model {

	
	var $table = "t_stock_income";
	var $select_column = array ("ID_STOCK_INCOME","STOCK_CODE","BUY_DATE","SELL_DATE"	,"LOT","BUY_PRICE_PER_LOT","SELL_PRICE_PER_LOT","PROFIT_LOSS");
	var $order_column = array(null,"STOCK_CODE","BUY_DATE","SELL_DATE"	,"LOT","BUY_PRICE_PER_LOT","SELL_PRICE_PER_LOT","PROFIT_LOSS");
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("STOCK_CODE", $_POST["search"]["value"]);
			$this->db->like("BUY_DATE", $_POST["search"]["value"]);
			$this->db->like("SELL_DATE", $_POST["search"]["value"]);
		}       
		    
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("STOCK_CODE","DESC");
		}	
		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("ID_STOCK_INCOME",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("ID_STOCK_INCOME",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("ID_STOCK_INCOME",$id);
		$this->db->delete($this->table);
		
	}
}
