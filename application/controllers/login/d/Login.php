<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{




	function __construct()
	{
		parent::__construct();
		$this->load->model('login/model_login', 'mlogin');
	}

	public function index()
	{
		$this->load->view('vlogin');
	}

	public function ceklogin()
	{
		$username = $this->input->post('userjq');
		$password = $this->input->post('passjq');

		$result = $this->mlogin->ambillogin($username, $password);

		if ($result == true) {
			// redirect('welcome');
			echo ('success');
		} else {
			// redirect('login');
			echo ('failed');
		}
	}


	public function logout()
	{
		$this->session->set_userdata('user', FALSE);
		$this->session->sess_destroy();
		redirect('login');
	}

	public function CekOldPasswd($id)
	{
		$oldPasswd = $this->input->post('oldpaswdjqr');


		$result = $this->mlogin->ambillogin($username, $oldPasswd);
		if ($result == true) {
			// redirect('welcome');
			echo ('success');
		} else {
			// redirect('login');
			echo ('failed');
		}
	}

	public function NewPassword()
	{

		$newPassword = trim($this->input->post('newPassword'));
		$retypePass = $this->input->post('retypePass');
		if ($newPassword <> '' && $retypePass <> '') {
			if ($newPassword <> $retypePass) {
				echo 'beda';
			} else {
				$update_data = array(
					'PASSWORD' => $newPassword
				);
				$this->mlogin->updatePassword($update_data);
				echo 'ubah';
			}
		} else {
			echo 'null';
		}
	}
}
