  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>
                 <?php foreach ($cboMenu as $isiMenu) {
                          echo $isiMenu['DESCRIPTION'];
                    }?></strong></h3>
                <div align='right'>
                  
                 <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info "><i class="fas fa-plus-circle"></i> Baru</button></div>
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">
                
            
              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
            <tr>
              <th>No</th>
                <th>Parameter</th>
                <th>Code</th>
                <th>Description</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Parameter</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Paramater:</label>
                <input type="text" name="paramater" class="form-control" id="paramater">
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Code:</label>
                <input type="text" class="form-control" name="code" id='code'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Description</label>
                <input type="text" class="form-control" name="description" id='description'>
              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Aktif</label>
                <select name="aktif" id="aktif" class="form-control jurusan">
                 <option value="">-Pilih Status Menu-</option>
                 <?php foreach ($cboAktif as $isiAktif) {?>
                  <option value="<?php echo $isiAktif['CODE']; ?>">
                    <?php echo $isiAktif['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
              
              
            </div>
            <div class="modal-footer">
  <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
   <input type="submit" name="action" class="btn btn-success"  value="Submit" />
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->