<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href="<?php echo base_url('assets/css/simple-line-icons.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/styles_ok.css');?> "/>
</head>
<body>
    <div class="registration-form">
        <form id="signupForm" role="form" action="#" method="post" class="sign-in-form">
            <div class="form-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" id="username" placeholder="Username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control item" id="password" placeholder="Password">
            </div>
           
            <div class="form-group">
                <!-- <button type="button" class="btn btn-block create-account">Sign in</button> -->
                <input type="submit" value="Login" class="btn btn-block create-account solid" />
            </div>
        </form>
        
    </div>
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery-3.4.1.min.js');?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
     <script src="<?php echo base_url('assets/js/jquery.loadingModal.js');?>"></script>

    <script src="<?php echo base_url('assets/js/app.js');?>"></script>
     <script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/script_ok.js');?>"></script>
     <script type="text/javascript">

    
$(document).ready( function () {
  $('#signupForm').submit(function(){
    
    event.preventDefault();
    
    var user = document.getElementById('username').value;
    var passwd =document.getElementById('password').value;
    if (user == '' || passwd=='' ){
       Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Please enter your username & password !',
          showConfirmButton: false,
          timer: 1500
        })
      return false;
    }else{

      $.ajax({
        url:'login/ceklogin',
        method:'POST',
        data: {
          userjq :user,
          passjq :passwd 
        },
        beforeSend: function() { 
           $('body').loadingModal({
              position:'auto',
              text:'',
              color:'#fff',
              opacity:'0.7',
              backgroundColor:'rgb(0,0,0)',
              animation:'doubleBounce'
                });
        },
        complete: function(){

          $('body').loadingModal('destroy');

            },
        success:function(response){
          console.log(response);
          if (response =='success'){
            $(location).attr('href', 'welcome');
          }else{
            
            $('#username').focus();
            document.getElementById('username').value='';
            document.getElementById('password').value='';
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Please check your username / password !'
            })
          }
        },
        dataType:'text'
      });
        
          
    }

  });
});       
  </script>
</body>
</html>
