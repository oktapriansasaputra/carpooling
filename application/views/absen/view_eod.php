  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

         <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title"> <?php foreach ($cboMenu as $isiMenu) {
                          echo $isiMenu['DESCRIPTION'];
                    }?></h3>
            <form method="POST" id="user_form"> 
            <div class="card-tools">
              <div align="right"><button type="button" id="submitall" name="submitall" class="btn btn-info " data-toggle="tooltip" data-placement="top" title="Submit All"><i class="fas fa-circle-notch"></i> Proses</button>
              </div>
              
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal Tutup Hari</label>
                  <input type="date" class="form-control select2bs4" name="eodDate" id="eodDate">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Cabang</label>
                  <select name="cabang" id="cabang" class="form-control select2bs4">
                 <option value="">-Pilih Cabang User-</option>
                 <?php foreach ($cboCabang as $isiCabang) {?>
                  <option value="<?php echo $isiCabang['CABANG_ID']; ?>">
                    <?php echo $isiCabang['NAMA_CABANG']; ?></option>
                  <?php  }?>
               </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            
          </div>
              </form>
        </div>

        <!-- /.card -->
        <!-- /.col -->
          <!-- ./col -->
        </div>
  
       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
      <!-- /.modal -->