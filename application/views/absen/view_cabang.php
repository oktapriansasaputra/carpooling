  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->


        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>
                    <?php foreach ($cboMenu as $isiMenu) {
                      echo $isiMenu['DESCRIPTION'];
                    } ?>
                  </strong></h3>
                <div align="right">
                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info"><i class="fas fa-plus-circle"></i> Baru</button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">

                <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode</th>
                      <th>Nama Cabang</th>
                      <th>Alamat</th>
                      <th>No Telp</th>
                      <th>Nama PIC</th>
                      <th>Tanggal Today</th>
                      <th>Aktif</th>
                      <th>Action</th>
                    </tr>
                  </thead>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.col -->
        <!-- ./col -->
      </div>





      <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form method="POST" id="user_form">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Cabang Baru</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Kode Cabang:</label>
              <input type="text" class="form-control" name="kodeCabang" id='kodeCabang' maxlength="3">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Nama Cabang:</label>
              <input type="text" class="form-control" name="NameCabang" id='NameCabang' maxlength="50">
            </div>
            <div class="form-group">
              <label for="alamat">Alamat Cabang</label>
              <textarea class="form-control" id="alamat" name="alamat" rows="3" maxlength="100"></textarea>
            </div>
            <div class="form-group">
              <label for="alamat">No Tlp Cabang</label>
              <input type="text" class="form-control" name="noTlp" id='noTlp'>
            </div>

            <div class="form-group">
              <label for="alamat">Nama PIC</label>
              <select name="nik" id="nik" class="form-control">

                <?php foreach ($cboEmployee as $isiTitle) { ?>
                  <option value="<?php echo $isiTitle['CD_EMPLOYEE']; ?>">
                    <?php echo $isiTitle['EMPLOYEE_NAME']; ?></option>
                <?php  } ?>
              </select>
            </div>

            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Tanggal Hari ini:</label>
              <input type="date" class="form-control" name="tglToday" id='tglToday'>
              <input type="hidden" class="form-control" name="action" id="action" value="Add">
            </div>

            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Status Cabang</label>
              <select name="aktif" id="aktif" class="form-control jurusan">
                <option value="">-Pilih Status Cabang-</option>
                <?php foreach ($cboAktif as $isiAktif) { ?>
                  <option value="<?php echo $isiAktif['CODE']; ?>">
                    <?php echo $isiAktif['DESCRIPTION']; ?></option>
                <?php  } ?>
              </select>
            </div>

          </div>
          <div class="modal-footer">
            <input type="submit" name="action" class="btn btn-success" value="Submit" />


          </div>

        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->