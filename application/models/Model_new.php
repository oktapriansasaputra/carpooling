<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_new extends CI_Model {


	public function ambilalluser($table)
	{
		$this->db->from($table);
		$this->db->where('FLAG_ACTIVE','Y');
		$res=$this->db->get();
        return $res->result_array(); 
	}

	public function ambilweekberita($table1)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('hot_issued',0);
		$this->db->order_by("tgl", "DESC");
		$this->db->limit(10);
		$res=$this->db->get();
	    return $res->result_array(); 
	}

	public function ambilsubmenu($var1,$user)
	{
		$sql = "select * from m_treeacc where group_user =? and parent=?";
		$var = array($var1,$user);
		$res=$this->db->query($sql,$var);
		// $res=$this->db->get();
	    return $res->result_array(); 
	}

	public function ambilmenu($var1,$user)
	{
		$sql = "select * from m_treeacc where group_user =? and parent=?";
		$var = array($var1,$user);
		$res=$this->db->query($sql,$var);
		// $res=$this->db->get();
	    return $res->result_array(); 
	}

	public function ambilkategoribyid($table,$id)
	{
		$this->db->from($table);
		$this->db->where('active','Y');
		$this->db->where('id_cat',$id);
		$res=$this->db->get();
	    return $res; 
	}

	public function ambilRecordbyid($table,$column,$id)
	{
		$this->db->from($table);
		$this->db->where($column,$id);
		$res=$this->db->get();
	    return $res; 
	}

	public function ambilonevideo($table)
	{
		$this->db->from($table);
		$this->db->where('flag_view',1);
		$this->db->order_by("id_video", "DESC");
		$this->db->limit(1);
		$res=$this->db->get();
	    return $res->result_array(); 
	}

	function data($number,$offset){
		return $query = $this->db->get('t_kampus',$number,$offset)->result();		
	}

	public function ambildetailberita($table,$id)
	{
		$this->db->from($table);
		$this->db->where('id_berita_campus',$id);
		$res=$this->db->get();
		return $res->result_array(); 
	}

	public function get_count($id){
				
		$sql ="call get_count(?)";
		$this->db->query($sql,$id);
		
	}

	public function get_count_comment($id){
				
		$sql ="call get_count_comment(?)";
		$this->db->query($sql,$id);
		
	}

	public function ambilpopularberita($table)
	{
		$sql = "select * from t_kampus where total_visit >1  order by total_visit desc limit 5";
		$res=$this->db->query($sql,$table);
        return $res->result_array(); 
	}

	public function ambilmostcomment($table)
	{
		$sql = "select * from t_kampus where id_berita_campus in (select t_comment_header.id_berita from t_comment_header where t_comment_header.total_comment>1 order by t_comment_header.total_comment desc)limit 5";
		$res=$this->db->query($sql);
        return $res->result_array(); 
	}
	public function form_insert($table,$data){
	
		$this->db->insert($table, $data);

	}

	public function ambilcommentberita($table,$id)
	{
		$this->db->from($table);
		$this->db->where('id_berita',$id);
		$res=$this->db->get();
		return $res->result_array(); 
	}

	public function cari($table,$keyword){
		$this->db->like('judul_berita_campus', $keyword);
		$res=$this->db->get($table);
		return $res->result_array();
	}
	
	
 
	function getAll($config){  
        $hasilquery=$this->db->get('t_kampus', $config['per_page'], $this->uri->segment(3));
        if ($hasilquery->num_rows() > 0) {
            foreach ($hasilquery->result() as $value) {
                $data[]=$value;
            }
            return $data;
        }      
	}
	
	function getoneberita($table){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('t_kampus.id_cat','001');
		$this->db->order_by("tgl", "DESC");
		$this->db->limit(1);
		$res=$this->db->get();
		return $res->result_array(); 
	}

	function getoneapps($table){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('t_kampus.id_cat','002');
		$this->db->order_by("tgl", "DESC");
		$this->db->limit(1);
		$res=$this->db->get();
		return $res->result_array(); 
	}
	function getonegadget($table){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('t_kampus.id_cat','003');
		$this->db->order_by("tgl", "DESC");
		$this->db->limit(1);
		$res=$this->db->get();
		return $res->result_array(); 
	}

	function getonegames($table){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('t_kampus.id_cat','004');
		$this->db->order_by("tgl", "DESC");
		$this->db->limit(1);
		$res=$this->db->get();
		return $res->result_array(); 
	}

	function getallbycategory($table,$id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('m_kategori', 'm_kategori.id_cat = t_kampus.id_cat','left');
		$this->db->where('t_kampus.id_cat',$id);
		$this->db->order_by("tgl", "DESC");
		$res=$this->db->get();
		return $res->result_array(); 
	}
	public function find($table,$keyword){
		$this->db->like('judul_find', $keyword);
		$this->db->limit(20);
		$res=$this->db->get($table);
		return $res->result_array();
	}

	public function ambildetailfind($table,$id)
	{
		$this->db->from($table);
		$this->db->where('id_find',$id);
		$res=$this->db->get();
		return $res->result_array(); 
	}
	
	public function view(){
		$this->load->library('pagination'); // Load librari paginationnya
    
		$query = "SELECT * FROM t_kampus where hot_issued=0";
		$config['base_url'] = base_url('home/index');
		$config['total_rows'] = $this->db->query($query)->num_rows();
		$config['per_page'] = 3;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['full_tag_open']   = '<ul class="pagination pagination-sm m-t-xs m-b-xs">';
        $config['full_tag_close']  = '</ul>';
        
        $config['first_link']      = 'First'; 
        $config['first_tag_open']  = '<li>';
        $config['first_tag_close'] = '</li>';
        
        $config['last_link']       = 'Last'; 
        $config['last_tag_open']   = '<li>';
        $config['last_tag_close']  = '</li>';
        
        $config['next_link']       = ' <i class="glyphicon glyphicon-menu-right"></i> '; 
        $config['next_tag_open']   = '<li>';
        $config['next_tag_close']  = '</li>';
        
        $config['prev_link']       = ' <i class="glyphicon glyphicon-menu-left"></i> '; 
        $config['prev_tag_open']   = '<li>';
        $config['prev_tag_close']  = '</li>';
        
        $config['cur_tag_open']    = '<li class="active"><a href="#">';
        $config['cur_tag_close']   = '</a></li>';
         
        $config['num_tag_open']    = '<li>';
		$config['num_tag_close']   = '</li>';
		
		$this->pagination->initialize($config); // Set konfigurasi paginationnya
    
		$page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
		$query .= " LIMIT ".$page.", ".$config['per_page'];

		$data['limit'] = $config['per_page'];
		$data['total_rows'] = $config['total_rows'];
		$data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
		$data['siswa'] = $this->db->query($query)->result();
    
    return $data;
	}

	
}
