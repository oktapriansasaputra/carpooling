  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>

<!-- ChartJS -->
<script src="<?php echo base_url('assets/plugins/chart.js/Chart.min.js');?>"></script>
<!-- AdminLTE App -->

<script src="<?php echo base_url('assets/dist/js/demo.js');?>"></script>

<script src="<?php echo base_url('assets/dist/js/pages/dashboard2.js');?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<script type="text/javascript">

google.charts.load('current', {packages:['corechart', 'bar']});
google.charts.setOnLoadCallback();
$(document).ready(function(){
$(document).on('submit','#formPassword',function(event){
      event.preventDefault();
     
        $.ajax({
          url:"<?php echo base_url().'login/NewPassword';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(response)
          {
            if (response=='ubah') {
              Swal.fire({
                  icon: 'success',
                  title: 'Password',
                  text: 'Password telah berhasil di ubah !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
              $('#modal-default').modal('hide');
            }else if(response=='beda'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password tidak sama !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } else if(response=='null'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password kosong !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } //end if
             
          }
        });
  })
});

function load_monthwise_data(year, title)
{
    //var temp_title = title + ' ' + year;
    var temp_title ='';
    $.ajax({
        url:"<?php echo base_url(); ?>welcome/fetch_data",
        method:"POST",
        data:{year:year},
        dataType:"JSON",
        success:function(data)
        {
            drawMonthwiseChart(data, temp_title);
        }
    })
}

function drawMonthwiseChart(chart_data, chart_main_title)
{
    var jsonData = chart_data;
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'total');

    $.each(jsonData, function(i, jsonData){
        var month = jsonData.month;
        var total = parseFloat($.trim(jsonData.total));
        data.addRows([[month, total]]);
    });

    var options = {
        title:chart_main_title,
        hAxis: {
            title: "Bulan"
        },
        vAxis: {
            title: 'Total Order'
        },
        chartArea:{width:'70%',height:'60%'}
    }

    var chart = new google.visualization.ColumnChart(document.getElementById('chart_area'));

    chart.draw(data, options);
}

</script>

<script>
    
$(document).ready(function(){
    $('#year').change(function(){
        var year = $(this).val();
        if(year != '')
        {
            load_monthwise_data(year, '');
           
        }
    });
});

</script>




</body>
</html>