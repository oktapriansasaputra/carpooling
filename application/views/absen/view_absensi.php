  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">

                <h3 class="card-title"><strong>Absensi</strong></h3>
                <div align="right">
                  <?php if ($cekAbsen <> true) { ?>
                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-success"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                  <?php   }else{ 
                    if ($cekAbsenPulang <> true) {?>
                    <button type="button" data-toggle="modal" data-target="#modal-keluar" data-whatever="@mdo" class="btn btn-success"><i class="fas fa-sign-out-alt"></i> Keluar</button>
                   <?php }} ?>
                   
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
            <tr>
              <th>No</th>
                <th>NIK</th>
                 <th>Tanggal</th>
                <th>Jam Masuk</th>
                <th>Jam Pulang</th>
               
            </tr>
        </thead>
      
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">

          <form method="post" action="" enctype="multipart/form-data"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Absen Masuk</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tanggal</label>
                 <input type="text" name="tglmasuk" class="form-control" id="tglmasuk" value="<?php echo date("Y-m-d");?>">

                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Jam</label>
                <input type="text" class="form-control" name="Jammasuk" id="Jammasuk" value="<?php echo "<script> waktu.getHours(); </script>";?>">
                 <span id="jam"></span>&nbsp;<span id="menit"></span>&nbsp;<span id="detik"></span>
              </div>
               <div class="form-group" id="my_camera" name='my_camera'> </div>
             
              <div  class="form-group" id="post_take_buttons" style="display:none">
                <a class="btn btn-success" href="#" onClick="cancel_preview()"><i class="fas fa-undo"></i></a>
                <a class="btn btn-danger" href="#" onClick="save_photo()" id="but_upload"><i class="fas fa-save"></i></a>
                 </div>
            <div id ="pre_take_buttons">
               <a class="btn btn-primary" href="#" onClick="preview_snapshot()"><i class="fas fa-camera"></i></a>
                                     
            </div>
          
            </div>
          </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      

      <!-- Modal Jam Keluar -->
      <div class="modal fade" id="modal-keluar" tabindex="-1" role="dialog">
        <div class="modal-dialog">

          <form method="post" action="" enctype="multipart/form-data"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Absen Keluar</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tanggal</label>
                 <input type="text" name="tglkeluar" class="form-control" id="tglkeluar" value="<?php echo date("Y-m-d");?>">

                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Jam</label>
                <input type="text" class="form-control" name="Jammasuk" id="Jammasuk" value="<?php echo "<script> waktu.getHours(); </script>";?>">
                 <span id="jam1"></span>&nbsp;<span id="menit1"></span>&nbsp;<span id="detik1"></span>
              </div>
               <div class="form-group" id="my_camera1" name='my_camera1'> </div>
             
              <div  class="form-group" id="post_take_buttons1" style="display:none">
                <a class="btn btn-success" href="#" onClick="cancel()"><i class="fas fa-undo"></i></a>
                <a class="btn btn-danger" href="#" onClick="save()" id="but_upload"><i class="fas fa-save"></i></a>
                 </div>
            <div id ="pre_take_buttons1">
               <a class="btn btn-primary" href="#" onClick="preview()"><i class="fas fa-camera"></i></a>
                                     
            </div>
          
            </div>
          </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>