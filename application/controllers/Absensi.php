<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('model_admin','madmin');	
		$this->load->model('model_absensi','mabsensi');
		

		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'absensi');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
			

		
    	$user=$this->session->userdata('user');

		$data = array(
               'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboAktif'=>$this->madmin->select_cbo_parameter('CD_TITLE'),
				'cboCity'=>$this->madmin->select_cbo_parameter('CITY'),
				'cekAbsen'=>$this->mabsensi->cek_absen(),
				'cekAbsenPulang'=>$this->mabsensi->cek_absen_pulang(),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')

            );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_absensi');
		$this->load->view('absen/footer_absensi');
	}

	

	function fetch_absensi(){

			
		$fetch_data= $this->mabsensi->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->cd_user;
				$sub_array[] = date_format(new DateTime($row ->tgl_absensi),"d F Y")	;
			$sub_array[] = $row ->waktu_masuk;
			$sub_array[] = $row ->waktu_keluar;
			
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mabsensi->get_all_data(),
			"recordsFiltered"=>$this->mabsensi->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	

	function upload(){
		$userLoginPhoto=$this->session->userdata('user');

		date_default_timezone_set('Asia/Jakarta'); 
		$time = date('H:i:s');
		$date = date('Y-m-d');

		$filename =  time() . '.jpg';
		$filepath = 'assets/upload/absen/'.$userLoginPhoto.'/';
		if(!is_dir($filepath))
    		mkdir($filepath);
		move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath.$filename);

				$insert_data = array(
				'tgl_absensi'=>$date,
				'cd_user'=>$userLoginPhoto,
				'waktu_masuk'=>$time,
				'IMAGE_MASUK'=>$filename 

				);
				$this->mabsensi->insert_crud($insert_data);
						echo 'data inserted successfully';

		

		echo $filepath.$filename;
			}

	function upload1(){
		$userLoginPhoto=$this->session->userdata('user');

		date_default_timezone_set('Asia/Jakarta'); 
		$jamKeluar = date('H:i:s');
		$tanggal = date('Y-m-d');

		$filename =  time() . '.jpg';
		$filepath = 'assets/upload/absen/'.$userLoginPhoto.'/';
		if(!is_dir($filepath))
    		mkdir($filepath);
		move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath.$filename);

			$this->mabsensi->upload_jamKeluar($userLoginPhoto,$tanggal,$jamKeluar,$filename );

		

		echo $filepath.$filename;
			}

		
}
	