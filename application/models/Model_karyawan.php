<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_karyawan extends CI_Model
{


	var $table = "m_employee";
	var $select_column = array("CD_EMPLOYEE", "EMPLOYEE_NAME", "ALAMAT", "(select DESCRIPTION FROM m_gcm where PARAMETER ='CITY'
	AND CODE = CITY )as CITY", "TELP", "(select DESCRIPTION FROM m_gcm where PARAMETER ='CD_TITLE'
	AND CODE = CD_TITLE ) as CD_TITLE", "(select DESCRIPTION FROM m_gcm where PARAMETER ='CD_DIVISI'
	AND CODE = CD_DESIGN ) as CD_DESIGN", "STATUS", "DATE_BIRTH", "PLACE_BIRTH", "DATE_JOIN");
	var $order_column = array("CD_EMPLOYEE", "EMPLOYEE_NAME", "CD_TITLE", null);


	function make_query()
	{
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if (isset($_POST["search"]["value"])) {
			$this->db->like("CD_EMPLOYEE", $_POST["search"]["value"]);
			$this->db->or_like("EMPLOYEE_NAME", $_POST["search"]["value"]);
			$this->db->or_like("CD_TITLE", $_POST["search"]["value"]);
		}

		if (isset($_POST["order"])) {
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else {
			$this->db->order_by("CD_EMPLOYEE");
		}
	}

	function make_datatable()
	{
		$this->make_query();
		if ($_POST["length"] != -1) {
			$this->db->limit($_POST["length"], $_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data()
	{
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_all_data()
	{

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	function fetch_single_user($user_id)
	{
		$this->db->where("CD_EMPLOYEE", $user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id, $data)
	{
		$this->db->where("CD_EMPLOYEE", $id);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}

	function delete_single_user($id)
	{
		$this->db->where("CD_EMPLOYEE", $id);
		$this->db->delete($this->table);
	}
}
