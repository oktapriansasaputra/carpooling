<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct(){
		parent::__construct();

		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}
	}

	public function index()
	{	
		$this->load->model('model_admin');	

		if(get_cookie('lang_is')==='EN'){
            // $this->db->where('NO_SR','EN');
            $id='EN';
        }elseif (get_cookie('lang_is')==='MY') {
            // $this->db->where('NO_SR','MY');
            $id='MY';
        
        }else{
            // $this->db->where('NO_SR','IN');
            $id='IN';
    	}

    	$user=$this->session->userdata('user');

		$data = array(
                'title' => 'TERAS  |  Laboratary Information System',
				'bahasa' =>$this->model_admin->ambilbahasa('LANG'),
				'def_bahasa' =>$this->model_admin->ambilbahasa('DEF_LANG'),
				'dashboard' =>$this->model_admin->ambilDeskiprsiAll('DSB_LANG',$id),
				'pilih_bahasa'=>$id,
				'get_current_user'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'cboJurusan'=>$this->model_admin->select_cbo_parameter('PRODI'),
				'cboGroup'=>$this->model_admin->select_cbo_parameter('GRP_USR')
            );

					
		$this->load->view('header',$data);
		$this->load->view('absen/view_profile',$data);
		$this->load->view('footer');
	}

	

	function fetch_user(){

		$this->load->model('model_crud');	
		$fetch_data= $this->model_crud->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->CD_USER;
			$sub_array[] = $row ->NAME;
			$sub_array[] = $row ->GROUP_USER;
			$sub_array[] = $row ->CD_PRODI;
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->CD_USER.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="'. $row ->CD_USER.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->model_crud->get_all_data(),
			"recordsFiltered"=>$this->model_crud->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'CD_USER'=>$this->input->post('nim'),
				'NAME'=>$this->input->post('fullname'),
				'GROUP_USER	'=>$this->input->post('group'),
				'CD_PRODI'=>$this->input->post('jurusan'),
				'PASSWORD'=>$this->input->post('password')
			);
			$this->load->model('model_crud');
			$this->model_crud->insert_crud($insert_data);
			echo 'data inserted successfully';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'NAME'=>$this->input->post('fullname'),
				'GROUP_USER	'=>$this->input->post('group'),
				'CD_PRODI'=>$this->input->post('jurusan'),
				'PASSWORD'=>$this->input->post('password')
			);
			$this->load->model('model_crud');
			$this->model_crud->update_crud($this->input->post('nim'),$update_data);
			echo 'data updates successfully';
		}
	}

	function fetch_single_user(){
		$output = array();
		$this->load->model('model_crud');	
		$data =$this->model_crud->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['CD_USER'] = $row->CD_USER;
			$output['NAME'] = $row->NAME;
			$output['GROUP_USER'] = $row->GROUP_USER;
			$output['CD_PRODI'] = $row->CD_PRODI;
			$output['PASSWORD'] = $row->PASSWORD;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->load->model('model_crud');	
		$this->model_crud->delete_single_user($_POST['user_id']);
		echo "data deleted";
	}

	function select_cbo_parameter()
	{
		$output = array();
		$this->load->model('model_crud');	
		$data =$this->model_crud->select_cbo_parameter('PRODI','Y');
		foreach ($data as $row) {
			$output .= '<option value="'.$row->NO_SR.'">'.$row->DESCRIPTION.'</option>';
			
		}
		echo json_encode($output);
	}

	
}
