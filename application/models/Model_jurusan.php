<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jurusan extends CI_Model {

	
	var $table = "m_gcm";
	var $select_column = array ("NO_SR","PARAMETER","CODE","DESCRIPTION","FLAG_ACTIVE","CHARVALUE");
	var $order_column = array ("PARAMETER","CODE","DESCRIPTION",null,null);

		
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("NO_SR", $_POST["search"]["value"]);
			$this->db->or_like("PARAMETER", $_POST["search"]["value"]);
			$this->db->or_like("CODE", $_POST["search"]["value"]);

		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CODE","PARAMETER");
		}
			
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
		 return $this->db->insert_id();
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("NO_SR",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("NO_SR",$id);
		$this->db->update($this->table,$data);
		return $this->db->affected_rows();	
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("NO_SR",$id);
		$this->db->delete($this->table);
		
	}
}

//69ffe55a99db23b320584d31242cab8f //