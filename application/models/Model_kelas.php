<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kelas extends CI_Model {

	
	var $table = "m_kelas";
	var $select_column = array ("KELAS_ID","CD_MATKUL","(select m_gcm.DESCRIPTION from m_gcm where m_gcm.PARAMETER ='MATKUL' and m_gcm.CODE = m_kelas.CD_MATKUL ) as MATA_KULIAH","CD_DOSEN",
		"(select m_gcm.DESCRIPTION from m_gcm where m_gcm.PARAMETER ='PRODI' and m_gcm.CODE = m_kelas.JURUSAN ) as JURUSAN",
		"(select m_gcm.DESCRIPTION from m_gcm where m_gcm.PARAMETER ='KELAS' and m_gcm.CODE = m_kelas.KELAS )as KELAS"	,"SEMESTER");
	var $order_column = array("CD_MATKUL","JURUSAN","KELAS","SEMESTER","TAHUN");
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CD_MATKUL", $_POST["search"]["value"]);
			

		}       
		    
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CD_MATKUL","DESC");
		}	
		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("KELAS_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("KELAS_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("KELAS_ID",$id);
		$this->db->delete($this->table);
		
	}
}
