  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
        
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Income Statement</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Stock Name:</label>
                <select name="stock" id="stock" class="form-control jurusan">
                 <option value="">-Pilih Saham-</option>
                 <?php foreach ($cboSaham as $isigroup) {?>
                  <option value="<?php echo $isigroup['CODE']; ?>">
                    <?php echo $isigroup['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Quarter:</label>
                <input type="text" class="form-control" name="quarter" id='quarter'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Year</label>
                <input type="text" class="form-control" name="year" id='year'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Total Sales</label>
                <input type="text" class="form-control" name="totalSales" id='totalSales'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">COGS</label>
                <input type="text" class="form-control" name="cogs" id='cogs'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Gross Profit</label>
                <input type="text" class="form-control" name="grossProfit" id='grossProfit'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Opex</label>
                <input type="text" class="form-control" name="opex" id='opex'>
              </div>

               <div class="form-group">
                <label for="recipient-name" class="col-form-label">EBIT</label>
                <input type="text" class="form-control" name="ebit" id='ebit'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Other Income</label>
                <input type="text" class="form-control" name="otherIncome" id='otherIncome'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Earning Before Tax</label>
                <input type="text" class="form-control" name="earnBeforeTax" id='earnBeforeTax'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Net Income After Tax</label>
                <input type="text" class="form-control" name="netIncomeAfterTax" id='netIncomeAfterTax'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Minority Interest</label>
                <input type="text" class="form-control" name="minorityInterest" id='minorityInterest'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Net Income</label>
                <input type="text" class="form-control" name="netIncome" id='netIncome'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Earning Per Share(EPS) </label>
                <input type="text" class="form-control" name="eps" id='eps'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Book Value Per Share(BV) </label>
                <input type="text" class="form-control" name="bv" id='bv'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Close Price </label>
                <input type="text" class="form-control" name="closePrice" id='closePrice'>
              </div>

               <div class="form-group">
                <label for="recipient-name" class="col-form-label">PER(Colse Price/EPS*)</label>
                <input type="text" class="form-control" name="per" id='per'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">PBV(Close Price/BV) +</label>
                <input type="text" class="form-control" name="pbv" id='pbv'>
              </div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->