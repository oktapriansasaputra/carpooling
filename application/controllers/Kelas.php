<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	function __construct(){
		parent::__construct();

		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}
	}

	public function index()
	{	
		$this->load->model('model_admin');	

		if(get_cookie('lang_is')==='EN'){
            // $this->db->where('NO_SR','EN');
            $id='EN';
        }elseif (get_cookie('lang_is')==='MY') {
            // $this->db->where('NO_SR','MY');
            $id='MY';
        
        }else{
            // $this->db->where('NO_SR','IN');
            $id='IN';
    	}

    	$user=$this->session->userdata('user');

		$data = array(
                'title' => 'TERAS  |  Laboratary Information System',
				'bahasa' =>$this->model_admin->ambilbahasa('LANG'),
				'def_bahasa' =>$this->model_admin->ambilbahasa('DEF_LANG'),
				'dashboard' =>$this->model_admin->ambilDeskiprsiAll('DSB_LANG',$id),
				'pilih_bahasa'=>$id,
				'get_current_user'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'cboKul'=>$this->model_admin->select_cbo_parameter('MATKUL'),
				'cboDosen'=>$this->model_admin->select_cbo_parameter('DOSEN'),
				'cboKelas'=>$this->model_admin->select_cbo_parameter('KELAS'),
				'cboJurusan'=>$this->model_admin->select_cbo_parameter('PRODI'),
				'cboAktif'=>$this->model_admin->select_cbo_parameter('FLAG_AKTIF')

            );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_kelas');
		$this->load->view('absen/footer_kelas');
	}

	

	function fetch_kelas(){

		$this->load->model('model_kelas');	
		$fetch_data= $this->model_kelas->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->MATA_KULIAH;
			$sub_array[] = $row ->JURUSAN;
			$sub_array[] = $row ->SEMESTER;
			$sub_array[] = $row ->KELAS;
					$sub_array[] = '<button type="button" name="edit" id="'. $row ->KELAS_ID.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="'. $row ->KELAS_ID.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->model_kelas->get_all_data(),
			"recordsFiltered"=>$this->model_kelas->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'CD_MATKUL'=>$this->input->post('kuliah'),
				'CD_DOSEN'=>$this->input->post('dosen'),
				'KELAS'=>$this->input->post('kelas'),
				'JURUSAN'=>$this->input->post('jurusan'),
				'SEMESTER'=>$this->input->post('semester'),
				'TAHUN'=>$this->input->post('tahun'),
				'SKS'=>$this->input->post('sks'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			$this->load->model('model_kelas');
			$this->model_kelas->insert_crud($insert_data);
			echo 'data inserted successfully';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'CD_MATKUL'=>$this->input->post('kuliah'),
				'CD_DOSEN'=>$this->input->post('dosen'),
				'KELAS'=>$this->input->post('kelas'),
				'JURUSAN'=>$this->input->post('jurusan'),
				'SEMESTER'=>$this->input->post('semester'),
				'TAHUN'=>$this->input->post('tahun'),
				'SKS'=>$this->input->post('sks'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			$this->load->model('model_kelas');
			$this->model_kelas->update_crud($this->input->post('unik'),$update_data);
			echo 'data updates successfully';
		}
	}

	function fetch_single_user(){
		$output = array();
		$this->load->model('model_kelas');	
		$data =$this->model_kelas->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['KELAS_ID'] = $row->KELAS_ID;
			$output['CD_MATKUL'] = $row->CD_MATKUL;
			$output['CD_DOSEN'] = $row->CD_DOSEN;
			$output['JURUSAN'] = $row->JURUSAN;
			$output['KELAS'] = $row->KELAS;
			$output['SEMESTER'] = $row->SEMESTER;
			$output['TAHUN'] = $row->TAHUN;
			$output['SKS'] = $row->SKS;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->load->model('model_jurusan');	
		$this->model_jurusan->delete_single_user($_POST['user_id']);
		echo "data deleted";
	}

	
}
