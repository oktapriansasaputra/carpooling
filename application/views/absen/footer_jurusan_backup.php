x<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>

  <!-- jquery-validation -->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js');?>"></script>
<script type="text/javascript"> 
  var save_method; //for save method string
var table;
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'jurusan/fetch_jurusan';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[-1],
          "orderable" :false
      }]
  });

  $('input').change(function(){
    $(this).parent().parent().removeClass('is-invalid');
    $(this).next().empty();
  });

  $(document).on('submit','#user_form',function(event){
      event.preventDefault();
      var paramater = $("#paramater").val();
      var kode = $("#kode").val();
      $('#user_form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('is-invalid'); // clear error class
      

        $.ajax({
          url:"<?php echo base_url().'jurusan/user_action';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(data)
          {
            alert(data);
            $('#user_form')[0].reset();
            $('#modal-lg').modal('hide');
            $('#action').val("Add");
            $('.modal-title').text("Add Parameter");
            dataTable.ajax.reload();
          }
        });

      
  });

  $(document).on('click','.edit',function(event){
    var user_id = $(this).attr('id');
    $.ajax({
       url:"<?php echo base_url().'jurusan/fetch_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          dataType:"json",
          success:function(data)
          {
            
            $('#modal-lg').modal('show');
            $('#paramater').val(data.PARAMETER);
            $('#paramater').attr('disabled','disabled');
            $('#code').val(data.CODE);
            $('#description').val(data.DESCRIPTION);
             $('#aktif').val(data.FLAG_ACTIVE);
             $('#unik').val(data.NO_SR);
            $('.modal-title').text("Edit Parameter");
            $('#action').val("Edit");

          }  
        })
  });

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('is-invalid'); // clear error class

     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('#paramater').removeAttr('disabled');
     $('.modal-title').text("Add Parameter");
  });

  $(document).on('click','.delete',function(event){
    var user_id = $(this).attr('id');
    if(confirm("Are you sure you want to delete this ?"))
    {
      $.ajax({
       url:"<?php echo base_url().'jurusan/delete_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          success:function(data)
          {
            
            alert(data);
            dataTable.ajax.reload();

          }  
        })
    }else{
      return false;
    }
    
  });
  $('#user_form').validate({
    rules: {
      kondisi: {
        required: true,
      },
      code: {
        required: true        
      },
      description: {
        required: true        
      },
       
       aktif: {
        required: true
      },
    },
    messages: {
      kondisi: {
        required: "Please enter a parameter"
      },
      code: {
        required: "Please enter your kode"
      },
      
      description: {
        required: "Please choose a group user"
      },
      aktif: {
        required: "Please choose status user"
      }
    },
    errorElement: 'span',
    errorClass: "help-block",
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });




});
    
   
 function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('jurusan/user_add')?>";
    } else {
        url = "<?php echo site_url('jurusan/user_update')?>";
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
  
</script>
<div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Parameter</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Paramater:</label>
                <input type="text" name="kondisi" class="form-control" id="kondisi">
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Code:</label>
                <input type="text" class="form-control" name="code" id='code'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Description</label>
                <input type="text" class="form-control" name="description" id='description'>
              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Aktif</label>
                <select name="aktif" id="aktif" class="form-control jurusan">
                 <option value="">-Pilih Status Menu-</option>
                 <?php foreach ($cboAktif as $isiAktif) {?>
                  <option value="<?php echo $isiAktif['CODE']; ?>">
                    <?php echo $isiAktif['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
              
              
            </div>
            <div class="modal-footer">
  <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
</body>
</html>