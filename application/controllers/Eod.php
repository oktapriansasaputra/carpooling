<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eod extends CI_Controller {

	// ************************************************//
	// Contoller Eod                                   //
	// fungsi :                                        //
	// 1. Mengelola group user                         //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('model_eod','meod');
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'eod');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
		
    	$user=$this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
                
				'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboGroup'=>$this->madmin->select_cbo_parameter('GRP_USR'),
				'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'eod'),
				'cboCabang'=>$this->madmin->getCabang(),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')
            );

					
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_eod',$data);
		$this->load->view('absen/footer_eod');
	}

	

	function save_menu(){

		
		$date=$this->input->post('eodDate');
		$cabang=$this->input->post('cabang');

		if ($date <>'' and $cabang<>''){
			
			$this->meod->copy_menu($from,$to);
			echo "masuk";
			
		}else{
			echo 'null' ;
		}
				
		
		
		// echo "<script>
		// 			alert('Data berhasil di copy ..');
		// 			window.location='index';</script>";
	}	
        	
	
}
// ウェンディバユ作成 //
