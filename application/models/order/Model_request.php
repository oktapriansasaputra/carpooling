<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_request extends CI_Model {

	
	var $table = "t_request_order";
	var $select_column = array ("id_request","nik_user","tanggal_order","tujuan","status");
	var $order_column = array ("id_request","nik_user","tanggal_order","tujuan","status");
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where ('nik_user',$this->session->userdata('user'));
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("id_request", $_POST["search"]["value"]);
			$this->db->or_like("tanggal_order", $_POST["search"]["value"]);
			$this->db->or_like("nik_user", $_POST["search"]["value"]);


		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("id_request");
		}
		
		


	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("id_request",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("id_request",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("id_request",$id);
		$this->db->delete($this->table);
		
	}
}
