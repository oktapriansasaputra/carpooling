<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_listsurvey extends CI_Model {


	var $table = "t_survey_header";
	var $select_column = array ("T_SURVEY_ID","CUSTOMER_ID","CD_USER","STATUS");
	var $order_column = array(null,"CUSTOMER_ID","CD_USER","STATUS");

	
	
	function make_query(){
		$CabangUser=$this->session->userdata('vp_cabang');
		$user=$this->session->userdata('user');
		$where  = array('OP','RJ' );

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('STATUS','O');
		$this->db->where('CD_USER',$user);
		
		
			
		
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("T_SURVEY_ID", $_POST["search"]["value"]);
				
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("T_SURVEY_ID");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("TASK_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("TASK_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("TASK_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cekOvertime($nik,$tglOvertime){
		
		$this->db->where('NIK',$nik);
		$this->db->where('TGL_OVERTIME',$tglOvertime);
		$query = $this->db->get('t_overtime');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekDataBeforeSubmitOvertime($user){
		
		$sql = "select	COUNT(0) from t_task
			where FLAG_SUBMIT='Y' AND FLAG_ASSIGN='N' AND STATUS='OP' and ASSIGN_BY = ? ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekDataCompleted($user){
		
		$sql = "select	COUNT(0) from t_task
			where FLAG_SUBMIT='Y' AND FLAG_ASSIGN='N' AND STATUS='OP' and trim(ASSIGN_TO) <>'' and ASSIGN_BY = ? ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekExistAbsenBeforeClaim($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}
	}

	function submit($user,$pilih){
		$sql ="call submit_all(?,?)";
		$param = array(
			'user'=>$user,
			'pilih'=>$pilih
			
		);
		$this->db->query($sql,$param);
	}

	function getCustomer($cabang){
		$this->db->where('CABANG_ID',$cabang);
		$query = $this->db->get('m_customer');
		return $query->result_array(); 

	}

	function getsurvey($user){
		$sql = "select T_SURVEY_ID,(select CUSTOMER_NAME from m_customer where m_customer.CUSTOMER_ID = t_survey_header.CUSTOMER_ID)CUSTOMER_ID,(select CUSTOMER_ADDRESS from m_customer where m_customer.CUSTOMER_ID = t_survey_header.CUSTOMER_ID) CUSTOMER_ADDRESS, CABANG_ID,CD_USER,STATUS from t_survey_header where STATUS ='O' and CD_USER = ? ";
		
			$res=$this->db->query($sql,$user);
			return $res->result_array(); 
	}
}
//69ffe55a99db23b320584d31242cab8f //