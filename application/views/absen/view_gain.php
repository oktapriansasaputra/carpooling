  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">

                <h3 class="card-title"><strong>Gain/Loss</strong></h3>
                <div align="right">
                <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-success "><i class="fas fa-plus-circle"></i> Beli</button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
            <tr>
              <th>No</th>
                <th>Saham</th>
                <th>Lot</th>
                <th>Tgl Beli</th>
                <th>Tgl Jual</th>
                <th>Buy</th>
                <th>Sell</th>
                <th>Profit</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
               <th>Saham</th>
                <th>Lot</th>
                <th>Tgl Beli</th>
                <th>Tgl Jual</th>
                <th>Buy</th>
                <th>Sell</th>
                <th>Profit</th>
                <th>Action</th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Beli Saham</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Saham:</label>
                 <select name="saham" id="saham" class="form-control jurusan">
                 <option value="">-Pilih Saham-</option>
                 <?php foreach ($cboSaham as $isisaham) {?>
                  <option value="<?php echo $isisaham['CODE']; ?>">
                    <?php echo $isisaham['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tanggal Beli</label>
                 <input type="date" class="form-control" name="buyDate" id='buyDate'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Harga Beli</label>
               <input type="text" class="form-control" name="buyPrice" id='buyPrice'>
              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Lot</label>
                 <input type="text" class="form-control" name="lot" id='lot'> 
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tanggal Jual</label>
                <input type="date" class="form-control" name="sellDate" id='sellDate'> 
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Harga Jual</label>
                <input type="text" class="form-control" name="sellPrice" id='sellPrice'>
              </div>
              
              
              
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->