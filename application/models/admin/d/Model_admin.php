<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_admin extends CI_Model
{

	public function cekMenu($group, $url)
	{


		$this->db->where('GROUP_USER', $group);
		$this->db->where('URL', $url);
		$this->db->where('FLAG_ACTIVE', 'Y');
		$query = $this->db->get('m_treeacc');
		if ($query->num_rows() > 0) {

			return true;
		} else {
			// redirect('login');
			return false;
		}
	}

	public function ambiluser($table)
	{
		$this->db->from($table);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function ambiladmingroup($table)
	{
		$this->db->from($table);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function ambiluserbyid($table, $id)
	{
		$this->db->select('(Select EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE = m_user.CD_EMPLOYEE)as NAME,GROUP_USER');
		$this->db->from($table);
		$this->db->where('CD_USER', $id);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function ambilbahasa($cat)
	{

		$sql = "select CODE,DESCRIPTION,CHARVALUE from m_gcm 
			where flag_active='Y' and parameter= ? ";

		$res = $this->db->query($sql, $cat);
		return $res->result_array();
	}



	public function form_update($table, $coloumn, $data, $id)
	{

		$this->db->where($coloumn, $id);
		$this->db->update($table, $data);
	}

	public function ambilmenubahasa($cat)
	{

		$sql = "select NO_SR,DESCRIPTION,CHARVALUE from m_gcm 
			where flag_active='Y' and parameter= ? ";

		$res = $this->db->query($sql, $cat);
		return $res->result_array();
	}

	public function ambilDeskiprsiAll()
	{

		// $sql = "select NO_SR,DESCRIPTION,CHARVALUE from m_gcm 
		// 	where FLAG_ACTIVE ='Y' and parameter= ? and CHARVALUE = ?  ";
		$this->db->select('CODE,DESCRIPTION,CHARVALUE');
		$this->db->from('m_gcm ');
		//$this->db->where('parameter', $cat);
		$res = $this->db->get();
		return $res->result_array();
	}


	public function find($table, $keyword)
	{
		$this->db->like('judul_find', $keyword);
		$this->db->or_like('isi_find', $keyword);
		$this->db->limit(20);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	function select_cbo_parameter($param)
	{
		$sql = "select CODE,DESCRIPTION from m_gcm 
		where flag_active='Y' and parameter= ? ";


		$res = $this->db->query($sql, $param);
		return $res->result_array();
	}

	function getSubKriteria()
	{
		$sql = "select CODE,DESCRIPTION from m_gcm 
		where flag_active='Y' and parameter in ('CD_CAR','CD_TITLE','TOT_ORDER','CD_JOIN') ";


		$res = $this->db->query($sql);
		return $res->result_array();
	}

	function getEmploye()
	{
		$sql = "select CD_EMPLOYEE,EMPLOYEE_NAME from m_employee ";

		$res = $this->db->query($sql);
		return $res->result_array();
	}

	public function cekNikEmployee($CD_EMPLOYEE)
	{
		$this->db->where('CD_EMPLOYEE', $CD_EMPLOYEE);
		$query = $this->db->get('m_employee');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function cekExist($kolom, $isikolom, $table)
	{
		$this->db->where($kolom, $isikolom);
		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function getEmployeName($param)
	{
		$sql = "select EMPLOYEE_NAME from m_employee 
		where CD_EMPLOYEE = ? ";

		$res = $this->db->query($sql, $param);
		return $res->result_array();
	}

	public function cekKomponenGajiEmployee($CD_EMPLOYEE, $CD_SALARY)
	{
		$this->db->where('CD_EMPLOYEE', $CD_EMPLOYEE);
		$this->db->where('CD_SALARY', $CD_SALARY);
		$query = $this->db->get('m_salary');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}


	public function ambilDeskiprsi($cat, $code)
	{

		// $sql = "select NO_SR,DESCRIPTION,CHARVALUE from m_gcm 
		// 	where FLAG_ACTIVE ='Y' and parameter= ? and CHARVALUE = ?  ";
		$this->db->select('DESCRIPTION');
		$this->db->from('m_gcm ');
		$this->db->where('PARAMETER', $cat);
		$this->db->where('CODE', $code);
		$this->db->where('FLAG_ACTIVE', 'Y');
		$res = $this->db->get();
		return $res->row();
	}

	public function getDeskripsiMenu($grup, $url)
	{
		$this->db->select('DESCRIPTION');
		$this->db->from('m_treeacc ');
		$this->db->where('GROUP_USER', $grup);
		$this->db->where('URL', $url);
		$this->db->where('FLAG_ACTIVE', 'Y');
		$res = $this->db->get();
		return $res->result_array();
	}

	function getCabang()
	{
		$sql = "select CABANG_ID,NAMA_CABANG from m_cabang ";

		$res = $this->db->query($sql);
		return $res->result_array();
	}

	function getEmployeForClaim($user, $group)
	{
		if ($group == 'SUPERADMIN') {
			$sql = "select CD_EMPLOYEE, EMPLOYEE_NAME from m_employee ";


			$res = $this->db->query($sql);
		} else {
			$sql = "select CD_EMPLOYEE, EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE = (select m_user.CD_EMPLOYEE from m_user where m_user.CD_USER= ?) ";


			$res = $this->db->query($sql, $user);
		}

		return $res->result_array();
	}

	function getEmployeForKurir()
	{
		$sql = "select CD_EMPLOYEE, EMPLOYEE_NAME from m_employee
			where CD_TITLE ='KUR' ";

		$res = $this->db->query($sql);
		return $res->result_array();
	}

	public function getSettingById($id)
	{

		$sql = "select SETTING_VALUE from m_setting 
			where 	ID_SETTING = ? ";

		$res = $this->db->query($sql, $id);
		return $res->result_array();
	}

	function getCount($param)
	{
		$sql = "select count(0) from m_employee ";

		$res = $this->db->query($sql, $param);
		return $res->result_array();
	}

	function count_article($table)
	{
		$this->db->select('COUNT(*) as count');
		$this->db->from($table);
		//$this->db->where(array('image !=' => ''));
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row->count;
		}
		return 0;
	}

	function cTotEmployeeStatus($table, $where)
	{
		$this->db->select('COUNT(*) as count');
		$this->db->from($table);
		$this->db->where('STATUS', $where);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row->count;
		}
		return 0;
	}
}


//69ffe55a99db23b320584d31242cab8f //