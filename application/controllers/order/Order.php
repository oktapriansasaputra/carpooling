<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('model_admin', 'madmin');
		$this->load->model('order/model_order', 'morder');


		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'order/order');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title' => '',
			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboAktif' => $this->madmin->select_cbo_parameter('FLAG_AKTIF'),
			'cboEmployee' => $this->madmin->getEmployeForClaim($user, $group),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'order/order'),
			'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
			'getColorTopbar' => $this->madmin->getSettingById('OP04'),
			'getColorSidebar' => $this->madmin->getSettingById('OP05')
		);

		$this->load->view('absen/header_user', $data);
		$this->load->view('order/view_order');
		$this->load->view('order/footer_order');
	}



	function fetch_order()
	{


		$fetch_data = $this->morder->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no;
			$sub_array[] = $row->nik_user;
			$sub_array[] = $row->EMPLOYEE_NAME;
			$sub_array[] = date_format(new DateTime($row->tanggal_order), "d F Y");
			$sub_array[] = $row->tujuan;
			$sub_array[] = $row->penjemputan;
			$sub_array[] = $row->jam_jemput;
			$sub_array[] = $row->finish;
			switch ($row->STATUS) {
				case "AP":
					$sub_array[] = '<span class="badge badge-success">Approve</span>';
					break;
				case "RJ":
					$sub_array[] =  '<span class="badge badge-danger">Reject</span>';
					break;
				case "ON":
					$sub_array[] =  '<span class="badge badge-warning">On Progress</span>';
					break;
			}

			$sub_array[] = '<button type="button" name="edit" id="' . $row->id_request . '" class="btn btn-warning btn-xs edit" data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></button>
			<button type="button" name="delete" id="' . $row->id_request . '" class="btn btn-danger btn-xs delete" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></button> ';
			$no = $no + 1;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=> $this->morder->get_all_data(),
			"recordsFiltered" => $this->morder->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action()
	{

		if ($_POST["action"] == "Add") {
			$insert_data = array(
				'nik_user' => $this->input->post('nik'),
				'tanggal_order' => $this->input->post('tgl_overtime'),
				'tujuan' => $this->input->post('tujuan'),
				'penjemputan' => $this->input->post('jemput'),
				'jam_jemput' => $this->input->post('jamJemput'),
				'user_created' => $this->session->userdata('user'),
				'STATUS' => 'ON',
				'date_created' => date('Y-m-d'),
				'tot_day' => $this->input->post('totHari'),
				'tot_people' => $this->input->post('totOrang'),

			);

			$hasilCek = $this->morder->cekOvertime($this->input->post('nik'), $this->input->post('tgl_overtime'));
			if ($hasilCek <> true) {

				$this->morder->insert_crud($insert_data);
				echo 'masuk';
			} else {
				echo 'exist';
			}
		}

		if ($_POST["action"] == "Edit") {
			$update_data = array(
				'nik_user' => $this->input->post('nik'),
				'tanggal_order' => $this->input->post('tgl_overtime'),
				'tujuan' => $this->input->post('tujuan'),
				'penjemputan' => $this->input->post('jemput'),
				'jam_jemput' => $this->input->post('jamJemput'),
				'user_created' => $this->session->userdata('user'),
				'STATUS' => 'ON',
				'date_created' => date('Y-m-d'),
				'tot_day' => $this->input->post('totHari'),
				'tot_people' => $this->input->post('totOrang'),
			);


			$this->morder->update_crud($this->input->post('unik'), $update_data);
			echo 'ubah';
		}
	}



	function fetch_single_user()
	{
		$output = array();

		$data = $this->morder->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['nik_user'] = $row->nik_user;
			$output['EMPLOYEE_NAME'] = $row->nik_user;
			$output['tanggal_order'] = $row->tanggal_order;
			$output['tujuan'] = $row->tujuan;
			$output['jam_jemput'] = $row->jam_jemput;
			$output['penjemputan'] = $row->penjemputan;
			$output['id_request'] = $row->id_request;
			$output['tot_day'] = $row->tot_day;
			$output['tot_people'] = $row->tot_people;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->morder->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	function submit_all()
	{
		$user = $this->session->userdata('user');
		$pilih = 'R';


		$this->morder->submit($user, $pilih);
		echo 'masuk';
	}
}
