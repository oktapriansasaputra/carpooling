<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_artikel extends CI_Model {

	function __construct(){
		parent::__construct();

	}

	function get_artikel(){
		$this->db->select('*');
		$this->db->from('m_gcm');
		$this->db->where('PARAMETER','LANG');
		$this->db->where('FLAG_ACTIVE','Y');
		if(get_cookie('LANG_IS')=='EN'){
			$this->db->where('NO_SR','EN');
		}elseif (get_cookie('LANG_IS')=='MY') {
			$this->db->where('NO_SR','MY');
		
		}else{
			$this->db->where('NO_SR','IN');
		}
		$QUERY = $this->db->get();
		RETURN $QUERY->result();

	}
	
	
}
