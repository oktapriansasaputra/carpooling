x<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>
<script>
   
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'kelas/fetch_kelas';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[0,3,4],
          "orderable" :false
      }]
  });

  $(document).on('submit','#user_form',function(event){
      event.preventDefault();
      var kuliah = $("#kuliah").val();
      var dosen = $("#dosen").val();
      var kelas = $("#kelas").val();
      var jurusan = $("#jurusan").val();
      var semester = $("#semester").val();
      var tahun = $("#tahun").val();
      var sks = $("#sks").val();
      var active = $("#active").val();

      if(kuliah !='' && dosen !='')
      {
        $.ajax({
          url:"<?php echo base_url().'kelas/user_action';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(data)
          {
            alert(data);
            $('#user_form')[0].reset();
            $('#modal-lg').modal('hide');
            $('#action').val("Add");
            $('.modal-title').text("Add Kelas");
            dataTable.ajax.reload();
          }
        });

      }else{
        alert("Both field are required !");
      }
  });

  $(document).on('click','.edit',function(event){
    var user_id = $(this).attr('id');
    $.ajax({
       url:"<?php echo base_url().'kelas/fetch_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          dataType:"json",
          success:function(data)
          {
            
            $('#modal-lg').modal('show');
            $('#kuliah').val(data.CD_MATKUL);
            $('#dosen').val(data.CD_DOSEN);
            $('#jurusan').val(data.JURUSAN);
            $('#kelas').val(data.KELAS);
            $('#semester').val(data.SEMESTER);
            $('#tahun').val(data.TAHUN);
            $('#sks').val(data.SKS);
            $('#aktif').val(data.FLAG_ACTIVE);
            $('#unik').val(data.KELAS_ID);
            $('.modal-title').text("Edit Kelas");
            $('#action').val("Edit");

          }  
        })
  });

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('.modal-title').text("Add Kelas");
  });

  $(document).on('click','.delete',function(event){
    var user_id = $(this).attr('id');
    if(confirm("Are you sure you want to delete this ?"))
    {
      $.ajax({
       url:"<?php echo base_url().'menu/delete_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          success:function(data)
          {
            
            alert(data);
            dataTable.ajax.reload();

          }  
        })
    }else{
      return false;
    }
    
  });

});
    
    
  
</script>
</body>
</html>