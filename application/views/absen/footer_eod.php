<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="<?php echo base_url('assets/plugins/jquery/jquery-3.5.1.js');?>"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>
<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
<script>
   
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'menu/fetch_menu';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[0,3,4],
          "orderable" :false
      }]
  });

  $(document).on('submit','#user_form',function(event){
      event.preventDefault();
      
        $.ajax({
          url:"<?php echo base_url().'eod/save_menu';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(data)
          {
           if(data=='null'){
            Swal.fire({
                  icon: 'error',
                  title: 'Oops..',
                  text: 'Masukkan Tanggal dan Cabang yang akan di proses !',
                  showConfirmButton: false,
                  timer: 1500
                }) // end sweet alert
           }else if(data=='sama'){
            Swal.fire({
                  icon: 'error',
                  title: 'error',
                  text: 'Pilihan grup tidak boleh sama!',
                  showConfirmButton: false,
                  timer: 1500
                }) // end sweet alert
           }else{
            Swal.fire({
                  icon: 'success',
                  title: 'Data Grup',
                  text: 'Data grup user berhasil di copy !',
                  showConfirmButton: false,
                  timer: 1500
                }) // end sweet alert
           }
           
            $('.modal-title').reset();
            
          }
        });

      
  });

  

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
     $('#action').val("Add");
     $('.modal-title').reset();
  });

$(document).on('submit','#formPassword',function(event){
      event.preventDefault();
     
        $.ajax({
          url:"<?php echo base_url().'login/NewPassword';?>",
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(response)
          {
            if (response=='ubah') {
              Swal.fire({
                  icon: 'success',
                  title: 'Password',
                  text: 'Password telah berhasil di ubah !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
              $('#modal-default').modal('hide');
            }else if(response=='beda'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password tidak sama !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } else if(response=='null'){
              Swal.fire({
                  icon: 'error',
                  title: 'Password',
                  text: 'Password kosong !',
                  showConfirmButton: false,
                  timer: 1500
                }) 
            } //end if
             
          }
        });

      
  });
  

  
});
    
    
  
</script>
</body>
</html>