<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_monitoring_claim_other extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "t_claim_other";
	var $select_column = array ("CLAIM_OTHER_ID","(select DESCRIPTION from m_gcm where PARAMETER ='EXPENSE' and CODE = CLAIM_OTHER_CODE and FLAG_ACTIVE='Y')as CLAIM_OTHER_CODE","EMPLOYEE_CODE","(select EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE = t_claim_other.EMPLOYEE_CODE)as EMPLOYEE_NAME","(select NAMA_CABANG from m_cabang where m_cabang.CABANG_ID = t_claim_other.CABANG_ID )as CABANG_ID","CLAIM_VALUE","DATE_REQUEST","STATUS");
	var $order_column = array(null,"CLAIM_OTHER_CODE","EMPLOYEE_CODE","CABANG_ID","CLAIM_VALUE","DATE_REQUEST");

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('FLAG_SUBMIT','Y');
		if (($userGroupIn<>'SUPERADMIN')&& ($userGroupIn<>'BM')){
			$this->db->where('USER_CREATED',$userLoginIn);
		}elseif ($userGroupIn=='BM') {
			$this->db->where('APPROVED_BY',$userLoginIn);
		}
		
			
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("DATE_REQUEST", $_POST["search"]["value"]);
		$this->db->like("EMPLOYEE_CODE", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("EMPLOYEE_CODE","DATE_REQUEST");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("CLAIM_OTHER_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("CLAIM_OTHER_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("CLAIM_OTHER_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cekAbsen($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekExistClaimOther($cdUser,$JenisKlaim){
		
		$this->db->where('EMPLOYEE_CODE',$cdUser);
		$this->db->where('CLAIM_OTHER_CODE',$JenisKlaim);
		$query = $this->db->get($this->table);
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function submit($user,$pilih){
		$sql ="call submit_all(?,?)";
		$param = array(
			'user'=>$user,
			'pilih'=>$pilih
			
		);
		$this->db->query($sql,$param);
	}

	function cekDataBeforeSubmitTransport($user){
		
		$sql = "select NIK from t_transport 
			where FLAG_SUBMIT='N' and NIK = (select CD_EMPLOYEE from m_user where CD_USER = ? ) ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}
}
