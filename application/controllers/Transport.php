<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends CI_Controller {

	// ************************************************//
	// Contoller transport                             //
	// fungsi :                                        //
	// 1. Mengelola klaim transport karyawan           //
	// 2. Menghapus klaim transport karyawan           //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();

		$this->load->model('model_admin','madmin');	
		$this->load->model('model_transport','mtransport');
		

		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'transport');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
			
    	$user=$this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title'=>'',
               	'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
				'cboEmployee'=>$this->madmin->getEmployeForClaim($user,$group),
				'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'transport'),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')
		            );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_transport');
		$this->load->view('absen/footer_transport');
	}

	

	function fetch_transport(){

			
		$fetch_data= $this->mtransport->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->NIK;
			$sub_array[] = $row ->EMPLOYEE_NAME;
			$sub_array[] = date_format(new DateTime($row ->TGL_REIMBURSE),"d F Y")	;
			switch ($row ->STATUS) {
			       case "AP":
					$sub_array[] = '<span class="badge badge-success">Approve</span>';
					break;
					case "RJ":
					$sub_array[] =  '<span class="badge badge-danger">Reject</span>';
					break;
					case "ON":
					$sub_array[] =  '<span class="badge badge-warning">On Progress</span>';
					break;
			}
			
			$sub_array[] = number_format($row ->VALUE,2,".",",");
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mtransport->get_all_data(),
			"recordsFiltered"=>$this->mtransport->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action(){
		$user=$this->session->userdata('user');
		if($_POST["action"]=="Add")
		{
				$insert_data = array(
				'NIK'=>$this->input->post('nik'),
				'TGL_REIMBURSE'=>$this->input->post('tglklaim'),
				'STATUS'=>'ON'

				);

			$hasilCek =$this->mtransport->cekAbsen($user,$this->input->post('tglklaim'));
			if($hasilCek<>true){
				echo 'null';
			}else{
			$hasilExistingClaim =$this->mtransport->cekExistClaimTransport($this->input->post('nik'),$this->input->post('tglklaim'));
				
				if ($hasilExistingClaim<>true){
					$this->mtransport->insert_crud($insert_data);
					echo 'masuk';
				}else{
					echo 'exist';
				}
				
			}
			
						
			
		}

		// if($_POST["action"]=="Edit")
		// {
		// 	$update_data = array(
		// 		'NIK'=>$this->input->post('nik'),
		// 		'TGL_OVERTIME'=>$this->input->post('tgl_overtime'),
		// 		'JAM_MASUK'=>$this->input->post('jamMulai'),
		// 		'JAM_SELESAI'=>$this->input->post('jamSelesai'),
		// 		'STATUS'=>'ON'
		// 	);
			
		// 	$this->mtransport->update_crud($this->input->post('unik'),$update_data);
		// 	echo 'ubah';
		// }
	}
	
	

	function fetch_single_user(){
		$output = array();
		
		$data =$this->mtransport->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['NIK'] = $row->NIK;
			$output['TGL_OVERTIME'] = $row->TGL_OVERTIME;
			$output['JAM_MASUK'] = $row->JAM_MASUK;
			$output['JAM_SELESAI'] = $row->JAM_SELESAI;
			$output['OVERTIME_ID'] = $row->OVERTIME_ID;
			
		}
		echo json_encode($output);
	}

	
	function submit_all(){
		$user=$this->session->userdata('user');
		$pilih='T';

		$hasilCekdata = $this->mtransport->cekDataBeforeSubmitTransport($user);
		if($hasilCekdata<>true){
			echo 'null';

		}else{
			$this->mtransport->submit($user,$pilih);
			echo 'masuk';	
		}

		
	}

		
}

// ウェンディバユ作成 //	