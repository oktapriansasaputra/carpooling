<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	
	// ************************************************//
	// Contoller Menu                                  //
	// fungsi :                                        //
	// 1. Menambah Menu baru pada grup user            //
	// 2. Mengubah menu yang ada                       //
	// 3. Menghapus menu                               //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// email        : wendy.bayu@gmail.com             //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('model_menu','mmenu');	
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'menu');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
		

    	$user=$this->session->userdata('user');
    	$group = $this->session->userdata('group');

		$data = array(
                'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboGroup'=>$this->madmin->select_cbo_parameter('GRP_USR'),
				'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
				'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'menu'),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')
		

            );

					
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_menu',$data);
		$this->load->view('absen/footer_menu');
	}

	

	function fetch_menu(){

			
		$fetch_data= $this->mmenu->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->GROUP_USER;
			$sub_array[] = $row ->GROUP_NAME;
			$sub_array[] = $row ->PUBLIC_CODE;
			$sub_array[] = $row ->DESCRIPTION;
			switch ($row ->FLAG_ACTIVE) {
			       case "Y":
					$sub_array[] = '<span class="badge badge-success">Aktif</span>';
					break;
					case "N":
					$sub_array[] =  '<span class="badge badge-danger">Tidak Aktif</span>';
					break;
			}
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->ID_TREE.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit</button> <button type="button" name="delete" id="'. $row ->ID_TREE.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mmenu->get_all_data(),
			"recordsFiltered"=>$this->mmenu->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function menu_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'PARENT'=>$this->input->post('parentmenu'),
				'CHILD'=>$this->input->post('childmenu'),
				'GROUP_USER	'=>$this->input->post('group'),
				'PUBLIC_CODE'=>$this->input->post('publiccode'),
				'DESCRIPTION'=>$this->input->post('deskripsi'),
				'FLAG_ACTIVE'=>$this->input->post('aktif'),
				'URL'=>$this->input->post('url'),
				'ICON'=>$this->input->post('icon')
			);
			$this->load->model('model_menu');
			$this->model_menu->insert_crud($insert_data);
			echo 'masuk';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'PARENT'=>$this->input->post('parentmenu'),
				'CHILD'=>$this->input->post('childmenu'),
				'GROUP_USER	'=>$this->input->post('group'),
				'PUBLIC_CODE'=>$this->input->post('publiccode'),
				'DESCRIPTION'=>$this->input->post('deskripsi'),
				'FLAG_ACTIVE'=>$this->input->post('aktif'),
				'URL'=>$this->input->post('url'),
				'ICON'=>$this->input->post('icon')
			);
			$this->load->model('model_menu');
			$this->model_menu->update_crud($this->input->post('unik'),$update_data);
			echo 'ubah';
		}
	}

	function fetch_single_user(){
		$output = array();
		$this->load->model('model_menu');	
		$data =$this->model_menu->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['GROUP_USER'] = $row->GROUP_USER;
			$output['CHILD'] = $row->CHILD;
			$output['PARENT'] = $row->PARENT;
			$output['PUBLIC_CODE'] = $row->PUBLIC_CODE;
			$output['DESCRIPTION'] = $row->DESCRIPTION;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
			$output['URL'] = $row->URL;
			$output['ICON'] = $row->ICON;
			$output['ID_TREE'] = $row->ID_TREE;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->load->model('model_menu');	
		$this->mmenu->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

}



// ウェンディバユ作成 //