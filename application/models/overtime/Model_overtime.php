<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_overtime extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = " t_overtime";
	var $select_column = array ("OVERTIME_ID","NIK","(select EMPLOYEE_NAME from m_employee 
		where m_employee.CD_EMPLOYEE =t_overtime.NIK ) as EMPLOYEE_NAME",
		"TGL_OVERTIME","JAM_MASUK","JAM_SELESAI",
		"STATUS");
	var $order_column = array(null,"NIK","CD_SALARY","EMPLOYEE_NAME","TGL_OVERTIME",NULL);

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');
		$vp_cd_employee=$this->session->userdata('S_CD_EMPLOYEE');

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('FLAG_SUBMIT','N');
		$this->db->where('NIK',$vp_cd_employee);
			
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("TGL_OVERTIME", $_POST["search"]["value"]);
		$this->db->like("NIK", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("NIK","TGL_OVERTIME");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("OVERTIME_ID",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("OVERTIME_ID",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("OVERTIME_ID",$id);
		$this->db->delete($this->table);
		
	}

	function cekOvertime($nik,$tglOvertime){
		
		$this->db->where('NIK',$nik);
		$this->db->where('TGL_OVERTIME',$tglOvertime);
		$query = $this->db->get(' t_overtime');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekDataBeforeSubmitOvertime($user){
		
		$sql = "select NIK from t_overtime 
			where FLAG_SUBMIT='N' and NIK = (select CD_EMPLOYEE from m_user where CD_USER = ? ) ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}


	function cekExistAbsenBeforeClaim($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}
	}

	function submit($user,$pilih){
		$sql ="call submit_all(?,?)";
		$param = array(
			'user'=>$user,
			'pilih'=>$pilih
			
		);
		$this->db->query($sql,$param);
	}
}
//69ffe55a99db23b320584d31242cab8f //