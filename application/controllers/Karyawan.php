<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

	// ************************************************//
	// Contoller Karyawan                              //
	// fungsi :                                        //
	// 1. Menambah karyawan baru                       //
	// 2. Mengubah karyawan yang ada                   //
	// 3. Menghapus karyawab                           //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// email        : wendy.bayu@gmail.com             //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct()
	{
		parent::__construct();

		$this->load->model('model_admin', 'madmin');
		$this->load->model('model_karyawan', 'mkaryawan');


		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'karyawan');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboAktif' => $this->madmin->select_cbo_parameter('CD_TITLE'),
			'cboCity' => $this->madmin->select_cbo_parameter('CITY'),
			'cboStatusEmployee' => $this->madmin->select_cbo_parameter('STATUS_KARYAWAN'),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'karyawan'),
			'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
			'getColorTopbar' => $this->madmin->getSettingById('OP04'),
			'getColorSidebar' => $this->madmin->getSettingById('OP05'),
			'cboDivisi' => $this->madmin->select_cbo_parameter('CD_DIVISI'),


		);

		$this->load->view('absen/header_user', $data);
		$this->load->view('absen/view_karyawan');
		$this->load->view('absen/footer_karyawan');
	}



	function fetch_karyawan()
	{


		$fetch_data = $this->mkaryawan->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no;
			$sub_array[] = $row->CD_EMPLOYEE;
			$sub_array[] = $row->EMPLOYEE_NAME;
			$sub_array[] = $row->DATE_BIRTH;
			$sub_array[] = $row->PLACE_BIRTH;
			$sub_array[] = $row->DATE_JOIN;
			$sub_array[] = $row->ALAMAT;
			$sub_array[] = $row->CITY;
			$sub_array[] = $row->TELP;
			$sub_array[] = $row->CD_TITLE;
			$sub_array[] = $row->CD_DESIGN;

			$sub_array[] = '<button type="button" name="edit" id="' . $row->CD_EMPLOYEE . '" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="' . $row->CD_EMPLOYEE . '" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no + 1;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=> $this->mkaryawan->get_all_data(),
			"recordsFiltered" => $this->mkaryawan->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action()
	{
		if ($_POST["action"] == "Add") {
			$insert_data = array(
				'CD_EMPLOYEE' => $this->input->post('nik'),
				'EMPLOYEE_NAME' => $this->input->post('karyawanName'),
				'CD_DESIGN' => $this->input->post('atasan'),
				'CD_TITLE' => $this->input->post('jabatan'),
				'ALAMAT' => $this->input->post('alamat'),
				'CITY' => $this->input->post('city'),
				'STATUS' => $this->input->post('statusEmployee'),
				'DATE_BIRTH' => $this->input->post('dateBirth'),
				'PLACE_BIRTH' => $this->input->post('placeBirth'),
				'DATE_JOIN' => $this->input->post('dateJoin'),

			);

			$hasilCek = $this->madmin->cekNikEmployee($this->input->post('nik'));
			if ($hasilCek <> true) {
				$this->mkaryawan->insert_crud($insert_data);
				echo 'masuk';
			} else {
				echo 'exist';
			}
		}

		if ($_POST["action"] == "Edit") {
			$update_data = array(
				'CD_EMPLOYEE' => $this->input->post('nik'),
				'EMPLOYEE_NAME' => $this->input->post('karyawanName'),
				'CD_DESIGN' => $this->input->post('atasan'),
				'CD_TITLE' => $this->input->post('jabatan'),
				'ALAMAT' => $this->input->post('alamat'),
				'CITY' => $this->input->post('city'),
				'STATUS' => $this->input->post('statusEmployee'),
				'DATE_BIRTH' => $this->input->post('dateBirth'),
				'PLACE_BIRTH' => $this->input->post('placeBirth'),
				'DATE_JOIN' => $this->input->post('dateJoin'),
			);
			$this->mkaryawan->update_crud($this->input->post('nik'), $update_data);
			echo 'ubah';
		}
	}



	function fetch_single_user()
	{
		$output = array();

		$data = $this->mkaryawan->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['CD_EMPLOYEE'] = $row->CD_EMPLOYEE;
			$output['EMPLOYEE_NAME'] = $row->EMPLOYEE_NAME;
			$output['CD_DESIGN'] = $row->CD_DESIGN;
			$output['CD_TITLE'] = $row->CD_TITLE;
			$output['ALAMAT'] = $row->ALAMAT;
			$output['CITY'] = $row->CITY;
			$output['STATUS'] = $row->STATUS;
			$output['DATE_BIRTH'] = $row->DATE_BIRTH;
			$output['PLACE_BIRTH'] = $row->PLACE_BIRTH;
			$output['DATE_JOIN'] = $row->DATE_JOIN;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->mkaryawan->delete_single_user($_POST['user_id']);
		echo "deleted";
	}
}
