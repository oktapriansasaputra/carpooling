  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->


        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>
                    <?php foreach ($cboMenu as $isiMenu) {
                      echo $isiMenu['DESCRIPTION'];
                    } ?></strong></h3>
                <div align='right'>

                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-success "><i class="fas fa-plus-circle"></i> Baru </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">


                <div class="card-body">

                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIK</th>
                        <th>Nama Karyawan</th>
                        <th>Tgl Lahir</th>
                        <th>Tmp Lahir</th>
                        <th>Tgl Join</th>
                        <th>Alamat</th>

                        <th>Kota</th>
                        <th>Telp</th>
                        <th>Jabatan</th>
                        <th>Divisi</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col -->
          <!-- ./col -->
        </div>





        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <form method="POST" id="user_form">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Karyawan Baru</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="recipient-name" class="col-form-label">NIK:</label>
                <input type="text" name="nik" class="form-control" id="nik">
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
              </div>
              <div class="form-group col-md-6">
                <label for="recipient-name" class="col-form-label">Nama Karyawan:</label>
                <input type="text" class="form-control" name="karyawanName" id='karyawanName'>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Tanggal Lahir:</label>
                <input type="date" name="dateBirth" class="form-control" id="dateBirth">

              </div>
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Tempat Lahir:</label>
                <input type="text" class="form-control" name="placeBirth" id='placeBirth'>
              </div>
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Tanggal Join:</label>
                <input type="date" name="dateJoin" class="form-control" id="dateJoin">

              </div>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Alamat</label>
              <input type="text" class="form-control" name="alamat" id='alamat'>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Kota</label>
              <select name="city" id="city" class="form-control jurusan">
                <option value="">-Pilih Kota-</option>
                <?php foreach ($cboCity as $isiKota) { ?>
                  <option value="<?php echo $isiKota['CODE']; ?>">
                    <?php echo $isiKota['DESCRIPTION']; ?></option>
                <?php  } ?>
              </select>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Jabatan</label>
                <select name="jabatan" id="jabatan" class="form-control jurusan">
                  <option value="">-Pilih jabatan-</option>
                  <?php foreach ($cboAktif as $isiAktif) { ?>
                    <option value="<?php echo $isiAktif['CODE']; ?>">
                      <?php echo $isiAktif['DESCRIPTION']; ?></option>
                  <?php  } ?>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Divisi</label>
                <select name="atasan" id="atasan" class="form-control jurusan">
                  <option value="">-Pilih Divisi-</option>
                  <?php foreach ($cboDivisi as $cboDivisi) { ?>
                    <option value="<?php echo $cboDivisi['CODE']; ?>">
                      <?php echo $cboDivisi['DESCRIPTION']; ?></option>
                  <?php  } ?>
                </select>
              </div>

              <div class="form-group col-md-4">
                <label for="recipient-name" class="col-form-label">Status</label>
                <select name="statusEmployee" id="statusEmployee" class="form-control jurusan">
                  <option value="">-Pilih Status-</option>
                  <?php foreach ($cboStatusEmployee as $cboStatusEmployee) { ?>
                    <option value="<?php echo $cboStatusEmployee['CODE']; ?>">
                      <?php echo $cboStatusEmployee['DESCRIPTION']; ?></option>
                  <?php  } ?>
                </select>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
            <input type="submit" name="action" class="btn btn-success" value="Submit" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>


          </div>

        </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->