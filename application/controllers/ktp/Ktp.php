<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ktp extends CI_Controller
{



	function __construct()
	{
		parent::__construct();
		$this->load->model('model_admin', 'madmin');
		$this->load->model('model_eod', 'meod');

		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'ktp/ktp');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(

			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboGroup' => $this->madmin->select_cbo_parameter('GRP_USR'),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'ktp/ktp'),
			'cboCabang' => $this->madmin->getCabang()
		);


		$this->load->view('absen/header_user', $data);
		$this->load->view('ktp/view_ktp', $data);
		$this->load->view('ktp/footer_ktp');
	}



	function save_menu()
	{


		$date = $this->input->post('eodDate');
		$cabang = $this->input->post('cabang');

		if ($date <> '' and $cabang <> '') {

			$this->meod->copy_menu($from, $to);
			echo "masuk";
		} else {
			echo 'null';
		}



		// echo "<script>
		// 			alert('Data berhasil di copy ..');
		// 			window.location='index';</script>";
	}
}
// ウェンディバユ作成 //
