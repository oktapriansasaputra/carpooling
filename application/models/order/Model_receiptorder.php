<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_receiptorder extends CI_Model {

	// var $user=$this->session->userdata('user');
	var $table = "t_request_order";
	var $select_column = array ("id_request","nik_user","(select EMPLOYEE_NAME from m_employee 
		where m_employee.CD_EMPLOYEE =t_request_order.nik_user ) as EMPLOYEE_NAME",
		"tanggal_order","mulai","finish","jam_jemput","tujuan",
		"penjemputan",
		"STATUS");
	var $order_column = array(null,"nik_user",null,"tgl_order","mulai","jam_jemput",NULL);

	
	
	function make_query(){
		

		$userLoginIn=$this->session->userdata('user');
		$userGroupIn=$this->session->userdata('group');
		$nik = $this->getNik ($userLoginIn);

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where('FLAG_SUBMIT','Y');
		$this->db->where('flag_assign','Y');
		$this->db->where('status','ON');

		
			
		
		if(isset($_POST["search"]["value"]))
		{
		$this->db->like("tanggal_order", $_POST["search"]["value"]);
		$this->db->like("nik_user", $_POST["search"]["value"]);
		}       
		

		if(isset($_POST["order"]))
		{
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("nik_user","tanggal_order");
		}	

		
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("id_request",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("id_request",$id);
		$this->db->update($this->table,$data);
		
	}

	
	function delete_single_user($id)
	{
		$this->db->where ("id_request",$id);
		$this->db->delete($this->table);
		
	}

	function cekOvertime($nik,$tglOvertime){
		
		$this->db->where('nik_user',$nik);
		$this->db->where('tanggal_order',$tglOvertime);
		$query = $this->db->get('t_request_order');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}

	function cekDataBeforeSubmitOvertime($user){
		
		$sql = "select nik_user from t_request_order 
			where FLAG_SUBMIT='N' and nik_user = (select CD_EMPLOYEE from m_user where CD_USER = ? ) ";
		  
			$query=$this->db->query($sql,$user);
			
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}

	}


	function cekExistAbsenBeforeClaim($cdUser,$tglAbsen){
		
		$this->db->where('CD_USER',$cdUser);
		$this->db->where('tgl_absensi',$tglAbsen);
		$query = $this->db->get('t_absensi_detail');
		if($query->num_rows()>0){
			
			return true;
		}
		else{
			return false;
		}
	}

	function getNik ($vpuser){
		$this->db->select('CD_EMPLOYEE');
		$this->db->where('CD_USER',$vpuser);
		$query = $this->db->get('m_user');
		return $query->result();
	}

	function submit($user,$pilih){
		$sql ="call submit_all(?,?)";
		$param = array(
			'user'=>$user,
			'pilih'=>$pilih
			
		);
		$this->db->query($sql,$param);
	}
	function getdriver(){
		$this->db->select('ID_DRIVER, CD_EMPLOYEE,(Select EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE = m_driver.CD_EMPLOYEE)as EMPLOYEE_NAME');
		$this->db->from('m_driver');
		$res=$this->db->get();
        return $res->result_array(); 
	}

}
//69ffe55a99db23b320584d31242cab8f //