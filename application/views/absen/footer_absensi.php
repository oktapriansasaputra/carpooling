<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->


<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>
<script src="<?php echo base_url('assets/js/webcam.min.js');?>"></script>
<script>


   
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'absensi/fetch_absensi';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[0,3,4],
          "orderable" :false
      }]
  });





  $(document).on('submit','#user_form',function(event){
      event.preventDefault();
     
        $.ajax({
          url:"<?php echo base_url().'absensi/user_action';?>", 
          method:"POST",
          data: new FormData(this),
          contentType :false,
          processData:false,
          success:function(data)
          {
            alert(data);
            $('#user_form')[0].reset();
            $('#modal-lg').modal('hide');
            $('#action').val("Add");
            $('.modal-title').text("Absen Masuk");
            dataTable.ajax.reload();
            location.reload(true);
          }
        });

    
  });

  $(document).on('click','.edit',function(event){
    var user_id = $(this).attr('id');
    $.ajax({
       url:"<?php echo base_url().'absensi/fetch_single_user';?>",
          method:"POST",  
          data:{user_id:user_id},
          dataType:"json",
          success:function(data)
          {
            
            $('#modal-lg').modal('show');
            $('#kuliah').val(data.CD_MATKUL);
            $('#dosen').val(data.CD_DOSEN);
            $('#jurusan').val(data.JURUSAN);
            $('#kelas').val(data.KELAS);
            $('#semester').val(data.SEMESTER);
            $('#tahun').val(data.TAHUN);
            $('#sks').val(data.SKS);
            $('#active').val(data.FLAG_ACTIVE);
            $('#unik').val(data.KELAS_ID);
            $('.modal-title').text("Absens");
            $('#action').val("Edit");

          }  
        })
  });

  $(document).on('click','.close',function(event){
    var user_id = $(this).attr('id');
     $('#user_form')[0].reset();
     $('#modal-lg').modal('hide');
    
  });

  $(document).on('click','.delete',function(event){
    var user_id = $(this).attr('id');
    var dataTable2 = $('#example2').DataTable({
      "processing" :true,
      "serverSide" :true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'absensi/fetch_resultabsensi/';?>",
        type:"POST",

      },
      "columnDefs":[{

          "target" :[0,3,4],
          "orderable" :false
      }]
    });
  });
});
    
  
    Webcam.set({
      width: 420,
      height: 340,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );

    Webcam.attach( '#my_camera1' );

    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? '<?php echo base_url('assets/js/shutter.ogg');?>' : '<?php echo base_url('assets/js/shutter.mp3');?>';
  
  function preview_snapshot() {
      // freeze camera so user can preview pic
      shutter.play();

      Webcam.freeze();
      
      // swap button sets
      document.getElementById('pre_take_buttons').style.display = 'none';
      document.getElementById('post_take_buttons').style.display = '';
    }

  function preview() {
      // freeze camera so user can preview pic
      shutter.play();

      Webcam.freeze();
      
      // swap button sets
      document.getElementById('pre_take_buttons1').style.display = 'none';
      document.getElementById('post_take_buttons1').style.display = '';
    }

    
    function cancel_preview() {
      // cancel preview freeze and return to live camera feed
      Webcam.unfreeze();
      
      // swap buttons back
      document.getElementById('pre_take_buttons').style.display = '';
      document.getElementById('post_take_buttons').style.display = 'none';
    }

    function cancel() {
      // cancel preview freeze and return to live camera feed
      Webcam.unfreeze();
      
      // swap buttons back
      document.getElementById('pre_take_buttons1').style.display = '';
      document.getElementById('post_take_buttons1').style.display = 'none';
    }
    
    function save_photo() {
      // actually snap photo (from preview freeze) and display it
      Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('my_camera').innerHTML = 
          
          '<img id="imageprev" src="'+data_uri+'"/>';
          
        var base64image = document.getElementById("imageprev").src;
         Webcam.upload( data_uri, '<?php echo base_url('absensi/upload');?>', function(code, text) {
          console.log('Save successfully');
           $('#modal-lg').modal('hide');
           dataTable.ajax.reload();
           location.reload(true);
          //console.log(text);
        });
        // swap buttons back
        document.getElementById('pre_take_buttons').style.display = '';
        document.getElementById('post_take_buttons').style.display = 'none';
        
      
      } );

    }

    function save() {
      // actually snap photo (from preview freeze) and display it
      Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('my_camera1').innerHTML = 
          
          '<img id="imageprev" src="'+data_uri+'"/>';
          
        var base64image = document.getElementById("imageprev").src;
         Webcam.upload( data_uri, '<?php echo base_url('absensi/upload1');?>', function(code, text) {
          console.log('Save successfully');
           $('#modal-keluar').modal('hide');
           dataTable.ajax.reload();
           location.reload(true);
          //console.log(text);
        });
        // swap buttons back
        document.getElementById('pre_take_buttons1').style.display = '';
        document.getElementById('post_take_buttons1').style.display = 'none';
        
      
      } );

    }

    window.setTimeout("waktu()", 1000);

    function waktu() {
        var waktu = new Date();
        setTimeout("waktu()", 1000);
        document.getElementById("jam").innerHTML = waktu.getHours() +
            ` : `;
        document.getElementById("menit").innerHTML = waktu.getMinutes() +
            ` : `;;
        document.getElementById("detik").innerHTML = waktu.getSeconds();
    }
 window.setTimeout("waktu1()", 1000);
     function waktu1() {
        var waktu = new Date();
        setTimeout("waktu1()", 1000);
        document.getElementById("jam1").innerHTML = waktu.getHours() +
            ` : `;
        document.getElementById("menit1").innerHTML = waktu.getMinutes() +
            ` : `;;
        document.getElementById("detik1").innerHTML = waktu.getSeconds();
    }

  
</script>
</body>
</html>