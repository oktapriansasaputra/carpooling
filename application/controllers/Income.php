<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {

	function __construct(){
		parent::__construct();

		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}
	}

	public function index()
	{	
		$this->load->model('model_admin');	

		if(get_cookie('lang_is')==='EN'){
            // $this->db->where('NO_SR','EN');
            $id='EN';
        }elseif (get_cookie('lang_is')==='MY') {
            // $this->db->where('NO_SR','MY');
            $id='MY';
        
        }else{
            // $this->db->where('NO_SR','IN');
            $id='IN';
    	}

    	$user=$this->session->userdata('user');

		$data = array(
                'title' => 'TERAS  |  Laboratary Information System',
				'bahasa' =>$this->model_admin->ambilbahasa('LANG'),
				'def_bahasa' =>$this->model_admin->ambilbahasa('DEF_LANG'),
				'dashboard' =>$this->model_admin->ambilDeskiprsiAll('DSB_LANG',$id),
				'pilih_bahasa'=>$id,
				'get_current_user'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->model_admin->ambiluserbyid('m_user',$user),
				'cboSaham'=>$this->model_admin->select_cbo_parameter('SAHAM')

            );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_income');
		$this->load->view('absen/footer_income');
	}

	

	function fetch_income(){

		$this->load->model('model_income');	
		$fetch_data= $this->model_income->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->STOCK_CODE;
			$sub_array[] = $row ->QUARTER;
			$sub_array[] = $row ->YEAR;
			$sub_array[] = number_format($row ->TOTAL_SALES,2);
			$sub_array[] = number_format($row ->NET_INCOME,2);
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->CODE_EQUITY.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i></button> <button type="button" name="delete" id="'. $row ->CODE_EQUITY.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i></button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->model_income->get_all_data(),
			"recordsFiltered"=>$this->model_income->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'STOCK_CODE'=>$this->input->post('stock'),
				'QUARTER'=>$this->input->post('quarter'),
				'YEAR'=>$this->input->post('year'),
	'TOTAL_SALES'=>str_replace(",","",$this->input->post('totalSales')),
	'COGS'=>str_replace(",","",$this->input->post('cogs')),
'GROSS_PROFIT'=>str_replace(",","",$this->input->post('grossProfit')),
				'OPEX'=>str_replace(",","",$this->input->post('opex')),
			'EBIT' => str_replace(",","",$this->input->post('ebit')),
'OTHER_INCOME' =>str_replace(",","",$this->input->post('otherIncome')),
'EARN_BEFORE_TAX' => str_replace(",","",$this->input->post('earnBeforeTax')),
'NET_INCOME_AFTER_TAX' => str_replace(",","",$this->input->post('netIncomeAfterTax')),
'MINORITY_INTEREST' => str_replace(",","",$this->input->post('minorityInterest')),
'NET_INCOME' => str_replace(",","",$this->input->post('netIncome')),
				'EPS' => str_replace(",","",$this->input->post('eps')),
				'BV' =>str_replace(",","",$this->input->post('bv')) ,
'CLOSE_PRICE' =>str_replace(",","",$this->input->post('closePrice')) ,
				'PER' => str_replace(",","",$this->input->post('per')),
				'PBV' => str_replace(",","",$this->input->post('pbv'))
			);
			$this->load->model('model_income');
			$this->model_income->insert_crud($insert_data);
			echo 'data inserted successfully';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'STOCK_CODE'=>$this->input->post('stock'),
				'QUARTER'=>$this->input->post('quarter'),
				'YEAR'=>$this->input->post('year'),
	'TOTAL_SALES'=>str_replace(",","",$this->input->post('totalSales')),
	'COGS'=>str_replace(",","",$this->input->post('cogs')),
'GROSS_PROFIT'=>str_replace(",","",$this->input->post('grossProfit')),
			'OPEX'=>str_replace(",","",$this->input->post('opex')),
			'EBIT' => str_replace(",","",$this->input->post('ebit')),
'OTHER_INCOME' =>str_replace(",","",$this->input->post('otherIncome')),
'EARN_BEFORE_TAX' => str_replace(",","",$this->input->post('earnBeforeTax')),
'NET_INCOME_AFTER_TAX' => str_replace(",","",$this->input->post('netIncomeAfterTax')),
'MINORITY_INTEREST' => str_replace(",","",$this->input->post('minorityInterest')),
'NET_INCOME' => str_replace(",","",$this->input->post('netIncome')),
				'EPS' => str_replace(",","",$this->input->post('eps')),
				'BV' =>str_replace(",","",$this->input->post('bv')) ,
'CLOSE_PRICE' =>str_replace(",","",$this->input->post('closePrice')) ,
				'PER' => str_replace(",","",$this->input->post('per')),
				'PBV' => str_replace(",","",$this->input->post('pbv'))
			);
			$this->load->model('model_income');
			$this->model_income->update_crud($this->input->post('unik'),$update_data);
			echo 'data updates successfully';
		}
	}

	function fetch_single_user(){
		$output = array();
		$this->load->model('model_income');	
		$data =$this->model_income->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['STOCK_CODE'] = $row->STOCK_CODE;
			$output['QUARTER'] = $row->QUARTER;
			$output['YEAR'] = $row->YEAR;
			$output['TOTAL_SALES'] = $row->TOTAL_SALES;
			$output['COGS'] = $row->COGS;
			$output['CODE_EQUITY'] = $row->CODE_EQUITY;
			$output['GROSS_PROFIT'] = $row->GROSS_PROFIT;
			$output['OPEX'] = $row->OPEX;
			$output['EBIT'] = $row->EBIT;
			$output['OTHER_INCOME'] = $row->OTHER_INCOME;
			$output['EARN_BEFORE_TAX'] = $row->EARN_BEFORE_TAX;
		$output['NET_INCOME_AFTER_TAX'] = $row->NET_INCOME_AFTER_TAX;
			$output['MINORITY_INTEREST'] = $row->MINORITY_INTEREST;
			$output['NET_INCOME'] = $row->NET_INCOME;
			$output['EPS'] = $row->EPS;
			$output['BV'] = $row->BV;
			$output['CLOSE_PRICE'] = $row->CLOSE_PRICE;
			$output['PER'] = $row->PER;
			$output['PBV'] = $row->PBV;

		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->load->model('model_crud');	
		$this->model_crud->delete_single_user($_POST['user_id']);
		echo "data deleted";
	}

	
}
