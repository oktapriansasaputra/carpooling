  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">

                <h3 class="card-title"><strong>Master Kelas</strong></h3>
                <div align="right">
                <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-success "><i class="fas fa-plus-circle"></i> Add</button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
            <tr>
              <th>No</th>
                <th>Mata Kuliah</th>
                <th>Jurusan</th>
                <th>Sms</th>
                <th>Kelas</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
               <th>Nama</th>
               <th>Jurusan</th>
                <th>Sms</th>
                <th>Kelas</th>
                <th>Action</th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Kelas</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Mata Kuliah:</label>
                 <select name="kuliah" id="kuliah" class="form-control jurusan">
                 <option value="">-Pilih Mata Kuliah-</option>
                 <?php foreach ($cboKul as $isikuliah) {?>
                  <option value="<?php echo $isikuliah['CODE']; ?>">
                    <?php echo $isikuliah['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nama Dosen</label>
                 <select name="dosen" id="dosen" class="form-control jurusan">
                 <option value="">-Pilih Dosen-</option>
                 <?php foreach ($cboDosen as $isiDosen) {?>
                  <option value="<?php echo $isiDosen['CODE']; ?>">
                    <?php echo $isiDosen['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Kelas</label>
                <select name="kelas" id="kelas" class="form-control jurusan">
                 <option value="">-Pilih kelas-</option>
                 <?php foreach ($cboKelas as $isiKelas) {?>
                  <option value="<?php echo $isiKelas['CODE']; ?>">
                    <?php echo $isiKelas['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Jurusan</label>
                <select name="jurusan" id="jurusan" class="form-control jurusan">
                 <option value="">-Pilih Jurusan-</option>
                 <?php foreach ($cboJurusan as $isiJurusan) {?>
                  <option value="<?php echo $isiJurusan['CODE']; ?>">
                    <?php echo $isiJurusan['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Semester</label>
                <input type="text" class="form-control" name="semester" id='semester'> 
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tahun</label>
                <input type="text" class="form-control" name="tahun" id='tahun'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">SKS</label>
                <input type="text" class="form-control" name="sks" id='sks'>
              </div>
               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Aktif</label>
                <select name="aktif" id="aktif" class="form-control jurusan">
                 <option value="">-Pilih Status Menu-</option>
                 <?php foreach ($cboAktif as $isiAktif) {?>
                  <option value="<?php echo $isiAktif['CODE']; ?>">
                    <?php echo $isiAktif['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div>
              
              
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->