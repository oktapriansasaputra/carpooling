<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_crud extends CI_Model {

	
	var $table = "m_user";
	var $select_column = array ("CD_USER","CD_EMPLOYEE","(select NAMA_CABANG from m_cabang where m_cabang.CABANG_ID= m_user.CABANG_ID ) as CABANG_ID",
		"(select EMPLOYEE_NAME from m_employee where m_employee.CD_EMPLOYEE =m_user.CD_EMPLOYEE ) as EMPLOYEE_NAME",
		"(SELECT DESCRIPTION FROM m_gcm WHERE PARAMETER ='GRP_USR' AND CODE= GROUP_USER)as GROUP_USER","PASSWORD","FLAG_ACTIVE");
	var $order_column = array ("CD_USER","CD_EMPLOYEE","GROUP_USER",null,null,null);
	
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST["search"]["value"]))
		{
			$this->db->like("CD_USER", $_POST["search"]["value"]);
			$this->db->or_like("CD_EMPLOYEE", $_POST["search"]["value"]);
			$this->db->or_like("GROUP_USER", $_POST["search"]["value"]);

		}

		if(isset($_POST["order"]))
		{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by("CD_USER","DESC");
		}	
	}

	function make_datatable(){
		$this->make_query();
		if($_POST["length"]!=-1)
		{
			$this->db->limit($_POST["length"],$_POST["start"]);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function get_filtered_data(){
		$this->make_query();
		$query =$this->db->get();
		return $query->num_rows();
	}

	function get_all_data(){
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function insert_crud($data)
	{
		$this->db->insert($this->table,$data);
	} 

	function fetch_single_user($user_id)
	{
		$this->db->where ("CD_USER",$user_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function update_crud($id,$data)
	{
		$this->db->where ("CD_USER",$id);
		$this->db->update($this->table,$data);
		
	}

	function delete_single_user($id)
	{
		$this->db->where ("CD_USER",$id);
		$this->db->delete($this->table);
		
	}

	function select_cbo_parameter($param,$aktif)
	{
		$this->db->where ("PARAMETER",$param);
		$this->db->where ("FLAG_ACTIVE",$aktif);
		$query = $this->db->get('m_gcm');
		return $query->result();
	}

	public function qetCabang()
	{
		$this->db->where('FLAG_ACTIVE','Y');
		$query =$this->db->from('m_cabang');
		return $query->result();
	}
}
