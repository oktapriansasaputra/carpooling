<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ClaimOther extends CI_Controller
{



	function __construct()
	{
		parent::__construct();

		$this->load->model('model_admin', 'madmin');
		$this->load->model('claim/model_claim_other', 'mClaimOther');


		if ($this->session->userdata('status') != 'login') {
			redirect(base_url('login'));
		} else {
			$result = $this->madmin->cekMenu($this->session->userdata('group'), 'claim/claimother');

			if ($result == false) {
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title' => '',
			'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
			'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
			'cboAktif' => $this->madmin->select_cbo_parameter('FLAG_AKTIF'),
			'cboEmployee' => $this->madmin->getEmployeForClaim($user, $group),
			'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'claim/claimother'),
			'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
			'getColorTopbar' => $this->madmin->getSettingById('OP04'),
			'getColorSidebar' => $this->madmin->getSettingById('OP05'),
			'cboClaimOther' => $this->madmin->select_cbo_parameter('EXPENSE')
		);

		$this->load->view('absen/header_user', $data);
		$this->load->view('claim/view_claim_other');
		$this->load->view('claim/footer_claim_other');
	}



	public function fetch_claim()
	{


		$fetch_data = $this->mClaimOther->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no;
			$sub_array[] = $row->EMPLOYEE_CODE;
			$sub_array[] = $row->EMPLOYEE_NAME;
			$sub_array[] = date_format(new DateTime($row->DATE_REQUEST), "d F Y");
			$sub_array[] = $row->CLAIM_OTHER_CODE;
			$sub_array[] = number_format($row->CLAIM_VALUE, 2, ".", ",");
			$sub_array[] = "<image src=" . base_url('assets/upload/claim/') . $row->IMAGE . ">";
			// switch ($row ->STATUS) {
			//        case "AP":
			// 		$sub_array[] = '<span class="badge badge-success">Approve</span>';
			// 		break;
			// 		case "RJ":
			// 		$sub_array[] =  '<span class="badge badge-danger">Reject</span>';
			// 		break;
			// 		case "ON":
			// 		$sub_array[] =  '<span class="badge badge-warning">On Progress</span>';
			// 		break;
			// }

			$sub_array[] = '<button type="button" name="edit" id="' . $row->CLAIM_OTHER_ID . '" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit </button> <button type="button" name="delete" id="' . $row->CLAIM_OTHER_ID . '" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no + 1;
			$data[] = $sub_array;
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=> $this->mClaimOther->get_all_data(),
			"recordsFiltered" => $this->mClaimOther->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action()
	{

		$user = $this->session->userdata('user');

		$group = $this->session->userdata('group');
		$CabangUser = $this->session->userdata('vp_cabang');
		$extension  = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$valid_extensions = array("jpg", "jpeg", "png");
		$filename = $filename   = uniqid() . time() . "." . $extension;
		/* Location */
		$location = "assets/upload/claim/" . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($location, PATHINFO_EXTENSION);

		if ($_POST["action"] == "Add") {




			/* Valid Extensions */
			//$valid_extensions = array("jpg","jpeg","png");
			/* Check file extension */
			if (!in_array(strtolower($imageFileType), $valid_extensions)) {
				$uploadOk = 0;
			}


			$insert_data = array(
				'CLAIM_OTHER_CODE' => $this->input->post('jenisKlaim'),
				'EMPLOYEE_CODE' => $this->input->post('nik'),
				'CABANG_ID' => $CabangUser,
				'CLAIM_VALUE' => $this->input->post('nilaiKlaim'),
				'DATE_REQUEST' => $this->input->post('tglklaim'),
				'USER_CREATED' => $user,
				'DATE_CREATED' => date('Y-m-d'),
				'IMAGE' => $filename,
				'STATUS' => 'ON'

			);

			/*$hasilCek =$this->mClaimOther->cekAbsen($user,$this->input->post('tglklaim'));
			if($hasilCek<>true){
				echo 'null';
			}else{*/
			$hasilExistingClaim = $this->mClaimOther->cekExistClaimOther($this->input->post('nik'), $this->input->post('jenisKlaim'), date('Y-m-d'));

			if ($hasilExistingClaim <> true) {
				if ($uploadOk == 0) {
					echo 0;
				} else {
					/* Upload file */
					if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
						$this->mClaimOther->insert_crud($insert_data);
						echo 'masuk';
					} else {
						echo 0;
					}
				}
			} else {
				echo 'exist';
			}

			/*}*/
		}

		if ($_POST["action"] == "Edit") {
			$update_data = array(
				'CLAIM_OTHER_CODE' => $this->input->post('jenisKlaim'),
				'EMPLOYEE_CODE' => $this->input->post('nik'),
				'CABANG_ID' => $CabangUser,
				'CLAIM_VALUE' => $this->input->post('nilaiKlaim'),
				'DATE_REQUEST' => $this->input->post('tglklaim'),
				'USER_CREATED' => $user,
				'DATE_CREATED' => date('Y-m-d'),
				'IMAGE' => $filename,
				'STATUS' => 'ON'
			);

			if ($hasilExistingClaim <> true) {
				if ($uploadOk == 0) {
					echo 0;
				} else {
					/* Upload file */
					if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
						$this->mClaimOther->update_crud($this->input->post('unik'), $update_data);
						echo 'ubah';
					} else {
						echo 0;
					}
				}
			} else {
				echo 'exist';
			}
		}
	}



	function fetch_single_user()
	{
		$output = array();

		$data = $this->mClaimOther->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['EMPLOYEE_CODE'] = $row->EMPLOYEE_CODE;
			$output['CLAIM_VALUE'] = $row->CLAIM_VALUE;
			$output['CLAIM_OTHER_CODE'] = $row->CLAIM_OTHER_CODE;
			$output['DATE_REQUEST'] = $row->DATE_REQUEST;
			$output['CLAIM_OTHER_ID'] = $row->CLAIM_OTHER_ID;
			$output['IMAGE'] = $row->IMAGE;
		}
		echo json_encode($output);
	}
	function delete_single_user()
	{

		$this->mClaimOther->delete_single_user($_POST['user_id']);
		echo "deleted";
	}


	function submit_all()
	{
		$user = $this->session->userdata('user');
		$pilih = 'C';

		$hasilCekdata = $this->mClaimOther->cekDataBeforeSubmitKlaim($user);
		if ($hasilCekdata <> true) {
			echo 'null';
		} else {
			$this->mClaimOther->submit($user, $pilih);
			echo 'masuk';
		}
	}
}

// ウェンディバユ作成 //	