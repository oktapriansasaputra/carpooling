  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>Balance Sheet</strong></h3>
                <div align='right'> <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info ">Add</button></div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
            <tr>
              <th>No</th>
                <th>Stock</th>
                <th>Q</th>
                <th>Year</th>
                 <th>Total Aset</th>
                 <th>Total Liablities</th>
                 <th>Total Equity</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
                <th>Stock</th>
                <th>Q</th>
                <th>Year</th>
                 <th>Total Aset</th>
                 <th>Total Liablities</th>
                  <th>Total Equity</th>
                <th>Action</th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Balance Sheet</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Stock Name:</label>
                <select name="stock" id="stock" class="form-control jurusan">
                 <option value="">-Pilih Saham-</option>
                 <?php foreach ($cboSaham as $isigroup) {?>
                  <option value="<?php echo $isigroup['CODE']; ?>">
                    <?php echo $isigroup['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <input type="hidden" class="form-control" name="unik" id="unik">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Quarter:</label>
                <input type="text" class="form-control" name="quarter" id='quarter'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Year</label>
                <input type="text" class="form-control" name="year" id='year'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Cash & Equivalent</label>
                <input type="text" class="form-control" name="cash" id='cash'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Account Receivable</label>
                <input type="text" class="form-control" name="ar" id='ar'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Inventories</label>
                <input type="text" class="form-control" name="inv" id='inv'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Other Current Assets</label>
                <input type="text" class="form-control" name="othCurAsset" id='othCurAsset'>
              </div>

               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Total Current Assets</label>
                <input type="text" class="form-control" name="totCurrAsset" id='totCurrAsset'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Fixed Assets</label>
                <input type="text" class="form-control" name="fixAsset" id='fixAsset'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Other Non-Curr.Assets</label>
                <input type="text" class="form-control" name="othNonCurrAsset" id='othNonCurrAsset'>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tot.Non-Cuttent Assets</label>
                <input type="text" class="form-control" name="totOthNonCurrAsset" id='totOthNonCurrAsset'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Total Assets</label>
                <input type="text" class="form-control" name="totAsset" id='totAsset'>
              </div>

              
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Current Liabilities </label>
                <input type="text" class="form-control" name="currLiab" id='currLiab'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Long Term Liabilties </label>
                <input type="text" class="form-control" name="longTermLiab" id='longTermLiab'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Total Libilitie </label>
                <input type="text" class="form-control" name="totLiab" id='totLiab'>
              </div>

               <div class="form-group">
                <label for="recipient-name" class="col-form-label">Total Equity</label>
                <input type="text" class="form-control" name="totEquity" id='totEquity'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Minority Interest</label>
                <input type="text" class="form-control" name="minInterest" id='minInterest'>
              </div>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Tot. Liabilities & Equity</label>
                <input type="text" class="form-control" name="totLiabEquity" id='totLiabEquity'>
              </div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->