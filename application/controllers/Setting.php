<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	// ************************************************//
	// Contoller setting                               //
	// fungsi :                                        //
	// 1. Mengelola settingan web                      //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//


	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('setting/model_setting','msetting');
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'setting');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}

		
	}

	public function index()
	{	
				
    	$user=$this->session->userdata('user');
    	$group = $this->session->userdata('group');

		$data = array(
   		'get_current_user'=> $this->madmin->ambiluserbyid('m_user', $user),
		'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
		'cboGroup'=>$this->madmin->select_cbo_parameter('GRP_USR'),
		'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
		'cboSetting'=>$this->madmin->select_cbo_parameter('OPTION'),
		'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'setting'),
		'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
		'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
		'getColorSidebar'=> $this->madmin->getSettingById('OP05'),
		'cboHeader'=>$this->madmin->select_cbo_parameter('HEADER')
    	);
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('setting/view_setting',$data);
		$this->load->view('setting/footer_setting');
	}

	

	function fetch_setting(){

		$fetch_data= $this->msetting->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			
			$sub_array[] = $row ->ID_SETTING;
			$sub_array[] = $row ->DESCRIPTION;
			$sub_array[] = $row ->SETTING_VALUE;
		
			
			$sub_array[] = '<button type="button" name="edit" id="'.$row ->ID_SETTING.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit </button> <button type="button" name="delete" id="'.$row ->ID_SETTING.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->msetting->get_all_data(),
			"recordsFiltered"=>$this->msetting->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		$hasilCek =$this->madmin->cekExist('CD_USER',$this->input->post('nim'),'m_user');
		$hasilcekNik = $this->madmin->cekExist('CD_EMPLOYEE',$this->input->post('karyawan'),'m_user');

		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'ID_SETTING'=>$this->input->post('settingan'),
				'SETTING_VALUE'=>$this->input->post('nilaiSettingan')
				
			);
			
			if($hasilCek <> true){
				if($hasilcekNik<>true){
					$this->msetting->insert_crud($insert_data);
					echo 'masuk';
				}else{
					echo 'nik';
				}
			
			}else{
				echo 'exist';
			}

		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'ID_SETTING'=>$this->input->post('settingan'),
				'SETTING_VALUE'=>$this->input->post('nilaiSettingan')
			);
			
				$this->msetting->update_crud($this->input->post('settingan'),$update_data);
				echo 'ubah';
			}
	}

	function fetch_single_user(){
		$output = array();
		$data =$this->msetting->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['ID_SETTING'] = $row->ID_SETTING;
			$output['SETTING_VALUE'] = $row->SETTING_VALUE;
					}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		
		$this->msetting->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	

	
}
// ウェンディバユ作成 //
