<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/dist/img/favicon-32x32.png');?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->

<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>"> -->


 <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"> -->

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">

 <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css');?>">
  <!-- DataTables -->
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/1.10.22/jquery.dataTables.min.css');?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-rowreorder/css/1.2.7/rowReorder.dataTables.min.css');?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-responsive/css/2.2.6/responsive.dataTables.min.css');?>">

 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/datatables-bs4/css/1.10.22/dataTables.bootstrap4.min.css');?>">
 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

 <link rel="stylesheet" href="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.min.css"');?>">
  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed ">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="<?php foreach ($getColorTopbar as $getColorTopbar) { echo $getColorTopbar['SETTING_VALUE'] ; }?>">
  <ul class="navbar-nav " style="border-right: 1px solid ">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto" align='right' >
      
            
      <!-- Language Dropdown Menu -->
     <!--  <li class="nav-item dropdown" >

      <?php foreach ($def_bahasa as $default) { ?>
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="<?php echo $default['CHARVALUE']; ?>"></i>
        </a>
    
        <div class="dropdown-menu dropdown-menu-right p-0" >
          <?php foreach ($bahasa as $bahasa) { ?>
         
          <?php if(get_cookie('lang_is') == $bahasa['CODE']) { ?>
            <a href="<?php echo site_url().'lang_setter/set_to/'.$bahasa['CODE'] ;?>" class="dropdown-item active">
            <i class="<?php echo $bahasa['CHARVALUE']; ?>"></i> <?php echo $bahasa['DESCRIPTION']; ?>
          </a>
          <?php } else { ?> 
          <a href="<?php echo site_url().'lang_setter/set_to/'.$bahasa['CODE'] ;?>" class="dropdown-item">
            <i class="<?php echo $bahasa['CHARVALUE']; ?>"></i> <?php echo $bahasa['DESCRIPTION']; ?>
          </a>
          <?php } }?>
          
          
        </div>
        <?php }?>
      </li> -->
      <li class="nav-item dropdown">
				<a href="#" class="nav-link" data-toggle="dropdown" data-hover="dropdown">
                    
           <img src="<?php echo base_url('assets/dist/img/profile.png');?>" style="width: 3%;"  class="img-circle">
          <em><strong>Hi</strong>, <span class="namaUser"><?php foreach ($get_current_user as $get_current_user) { echo $get_current_user['NAME'] ; }?></span> </em> <i class="dropdown-icon fa fa-angle-down"></i>
        </a>
										<!-- <ul class="dropdown-menu pull-right icon-right arrow"> -->
          <div class="dropdown-menu dropdown-menu-right p-0">		
           <!-- <a href="<?php echo base_url('profile');?>" class="dropdown-item">
                        <i class="fa fa-user"></i> Profile</a> -->
												<a href=""<?php echo base_url('profile');?>" class="dropdown-item"  data-toggle="modal" data-target="#modal-default">
                        <i class="fa fa-key"></i> Change Password</a>
                        
                        <a href="<?php echo base_url('login/logout');?>" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>
                        Sign Out
                        </a> 
                        
                        </div>
                      <!-- </ul> -->
      </li> 
                              <!-- /.card-tools -->
     </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-indigo elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('welcome');?>" class="<?php foreach ($getColorSidebar as $getColorSidebar) { echo $getColorSidebar['SETTING_VALUE'] ; }?>">
      <img src="https://img.icons8.com/fluent/344/money-transfer.png" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?php foreach ($getTitleSidebar as $getTitleSidebar) { echo $getTitleSidebar['SETTING_VALUE'] ; }?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
      <?php foreach ($get_current_group as $get_current_group) { 

          echo $this->dynamic_menu->build_menu($get_current_group['GROUP_USER'],'P','Y'); 
        }?>
        </ul>
      </nav>

        
  <!-- /.sidebar-menu -->
   
    <!-- /.sidebar -->
  </aside>

   <div class="modal fade" id="modal-default" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="formPassword"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Password</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Password Baru</label>
                <input type="password" name="newPassword" class="form-control" id="password">
                
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Re-type password</label>
                <input type="password" class="form-control" name="retypePass" id='retypePass'>
              </div>
              
                          
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
             </div>
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->