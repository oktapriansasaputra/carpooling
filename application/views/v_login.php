<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_other.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.loadingModal.css');?>">

	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<img class="wave" src="<?php echo base_url('assets/img/undraw_add_tasks_mxew.png');?>">
	<div class="container">
		<div class="img">
			
		</div>
		<div class="login-content" >
			<form id="signupForm" role="form" action="#" method="post">
				<img src="<?php echo base_url('assets/img/avatar.svg');?>">
				<h2 class="title">Welcome</h2>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>Username</h5>
           		   		<input type="text" class="input"  name="username" id="username">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>Password</h5>
           		    	<input type="password" class="input" name="password" id="password">
            	   </div>
            	</div>
            	
            	<input type="submit" class="btn" value="Login">
            </form>
        </div>
    </div>
    <script src="<?php echo base_url('assets/plugins/jquery/jquery-3.4.1.min.js');?>"></script>
  <!-- <script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/main.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.loadingModal.js');?>"></script>

    
<script type="text/javascript">

    
$(document).ready( function () {
  $('#signupForm').submit(function(){
    
    event.preventDefault();
    
    var user = document.getElementById('username').value;
    var passwd =document.getElementById('password').value;
    if (user == '' || passwd=='' ){
      alert ('please check your input')
      return false;
    }else{

      $.ajax({
        url:'login/ceklogin',
        method:'POST',
        data: {
          userjq :user,
          passjq :passwd 
        },
        beforeSend: function() { 
           $('body').loadingModal({
              position:'auto',
              text:'',
              color:'#fff',
              opacity:'0.7',
              backgroundColor:'rgb(0,0,0)',
              animation:'doubleBounce'
                });
        },
        complete: function(){

          $('body').loadingModal('destroy');

            },
        success:function(response){
          console.log(response);
          if (response =='success'){
            $(location).attr('href', 'welcome');
          }else{
            
            $('#username').focus();
            document.getElementById('username').value='';
            document.getElementById('password').value='';
            alert ('Please check Your input');
          }
        },
        dataType:'text'
      });
        
          
    }

  });
});       
  </script>
</body>
</html>
