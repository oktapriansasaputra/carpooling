<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SubKriteria extends CI_Controller
{



    function __construct()
    {
        parent::__construct();

        $this->load->model('admin/d/model_admin', 'madmin');
        $this->load->model('kriteria/model_sub_kriteria', 'mkriteria');


        if ($this->session->userdata('status') != 'login') {
            redirect(base_url('login'));
        } else {
            $result = $this->madmin->cekMenu($this->session->userdata('group'), 'kriteria/SubKriteria');

            if ($result == false) {
                redirect(base_url('login/logout'));
            }
        }
    }

    public function index()
    {

        $user = $this->session->userdata('user');
        $group = $this->session->userdata('group');
        $data = array(
            'title' => '',
            'get_current_user' => $this->madmin->ambiluserbyid('m_user', $user),
            'get_current_group' => $this->madmin->ambiluserbyid('m_user', $user),
            'cboAktif' => $this->madmin->select_cbo_parameter('FLAG_AKTIF'),
            'cboEmployee' => $this->madmin->getEmployeForClaim($user, $group),
            'cboMenu' => $this->madmin->getDeskripsiMenu($group, 'kriteria/SubKriteria'),
            'getTitleSidebar' => $this->madmin->getSettingById('OP03'),
            'getColorTopbar' => $this->madmin->getSettingById('OP04'),
            'getColorSidebar' => $this->madmin->getSettingById('OP05'),
            'getKriteria' => $this->madmin->select_cbo_parameter('CD_KRITERIA'),
            'getSubKriteria' => $this->madmin->getSubKriteria(),
            'getCar' => $this->madmin->select_cbo_parameter('CD_CAR'),
        );

        $this->load->view('absen/header_user', $data);
        $this->load->view('kriteria/view_sub_kriteria');
        $this->load->view('kriteria/footer_sub_kriteria');
    }



    public function fetch_user()
    {


        $fetch_data = $this->mkriteria->make_datatable();
        $data = array();
        $no = 1;
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = $no;
            $carName = $this->mkriteria->getCaraName($row->cd_car);
            foreach ($carName as $carName) {
                $sub_array[] = $carName['DESCRIPTION'];
            }
            $rtName = $this->mkriteria->getKriteriaName($row->id_kriteria);

            foreach ($rtName as $en) {
                $sub_array[] = $en['DESCRIPTION'];
            }

            //$sub_array[] = $row->id_kriteria;
            $SubName = $this->mkriteria->getSubKriteriaName($row->nama_subkriteria);

            foreach ($SubName as $sub) {
                $sub_array[] = $sub['DESCRIPTION'];
            }
            //$sub_array[] = $row->nama_subkriteria;

            $sub_array[] = $row->value;


            $sub_array[] = '<button type="button" name="edit" id="' . $row->id_subkriteria . '" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit </button> <button type="button" name="delete" id="' . $row->id_subkriteria . '" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
            $no = $no + 1;
            $data[] = $sub_array;
        }
        $output = array(
            "draw"             => intval($_POST['draw']),
            "recordsTotal"    => $this->mkriteria->get_all_data(),
            "recordsFiltered" => $this->mkriteria->get_filtered_data(),
            "data"            => $data
        );
        echo json_encode($output);
    }

    function user_action()
    {

        $user = $this->session->userdata('user');

        $group = $this->session->userdata('group');
        $CabangUser = $this->session->userdata('vp_cabang');


        if ($_POST["action"] == "Add") {
            $insert_data = array(
                'id_kriteria' => $this->input->post('id_kriteria'),
                'nama_subkriteria' => $this->input->post('id_subkriteria'),
                'value' => $this->input->post('nilai'),
                'cabang_id' => $CabangUser,
                'cd_car' => $this->input->post('mobil'),

            );

            $this->mkriteria->insert_crud($insert_data);
            echo 'masuk';
        }

        if ($_POST["action"] == "Edit") {
            $update_data = array(
                'id_kriteria' => $this->input->post('id_kriteria'),
                'nama_subkriteria' => $this->input->post('id_subkriteria'),
                'value' => $this->input->post('nilai'),
                'cabang_id' => $CabangUser,
                'cd_car' => $this->input->post('mobil'),
            );

            $this->mkriteria->update_crud($this->input->post('unik'), $update_data);
            echo 'ubah';
        }
    }



    function fetch_single_user()
    {
        $output = array();

        $data = $this->mkriteria->fetch_single_user($_POST['user_id']);
        foreach ($data as $row) {
            $output['nama_subkriteria'] = $row->nama_subkriteria;
            $output['value'] = $row->value;
            $output['cabang_id'] = $row->cabang_id;
            $output['id_kriteria'] = $row->id_kriteria;
            $output['id_subkriteria'] = $row->id_subkriteria;
            $output['cd_car'] = $row->cd_car;
        }
        echo json_encode($output);
    }
    function delete_single_user()
    {

        $this->mkriteria->delete_single_user($_POST['user_id']);
        echo "deleted";
    }
    function getSubCuy()
    {
        $id = $this->input->post('id');
        $data = $this->mkriteria->getSub($id);
        echo json_encode($data);
    }
}

// ウェンディバユ作成 //	