  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                 <h3 class="card-title"><strong>
                 <?php foreach ($cboMenu as $isiMenu) {
                          echo $isiMenu['DESCRIPTION'];
                    }?></strong></h3>
                <div align='right'>
                  <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-info"><i class="fas fa-plus-circle"></i> Baru</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="panel panel-default">
                
            
              <div class="card-body">
                
                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
            <tr>
             
               <th>ID</th>
                <th>Desktripsi</th>
                <th>Nilai</th>
                <th>Action</th>
            </tr>
        </thead>
                 </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">New Setting</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Deskripsi</label>
 <input type="hidden" class="form-control" name="action" id="action" value="Add">
                <select name="settingan" id="settingan" class="form-control" >
                 <option>--pilih salah satu</option>
                 <?php foreach ($cboSetting as $isiSetting) {?>
                  <option value="<?php echo $isiSetting['CODE']; ?>">
                    <?php echo $isiSetting['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>

              </div>
              <div class="form-group">
                  <label for="alamat">Value</label>
                  <textarea class="form-control" id="nilaiSettingan" name="nilaiSettingan" rows="3" maxlength="100" ></textarea>
               </div> 
              </div>

               <div class="modal-footer">
  <!-- <button type="button" id="btnSave" class="btn btn-primary">Save</button> -->
   <input type="submit" name="action" class="btn btn-success"  value="Submit" />
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->