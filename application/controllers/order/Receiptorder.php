<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receiptorder extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('model_admin','madmin');	
		$this->load->model('order/model_receiptorder','mreceiptorder');
		

		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'order/receiptorder');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
		
    	$user=$this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
			'title'=>'',
                'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
				'cboEmployee'=>$this->madmin->getEmployeForClaim($user,$group),
				'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'order/receiptorder'),
				'cboDriver'=>$this->mreceiptorder->getdriver(),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
		'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
		'getColorSidebar'=> $this->madmin->getSettingById('OP05')

		 );
			
		$this->load->view('absen/header_user',$data);
		$this->load->view('order/view_receiptorder');
		$this->load->view('order/footer_receiptorder');
	}

	

	function fetch_order(){

			
		$fetch_data= $this->mreceiptorder->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			
			$sub_array[] = $row ->id_request;
			$sub_array[] = $row ->nik_user;
			$sub_array[] = $row ->EMPLOYEE_NAME;
			$sub_array[] = date_format(new DateTime($row ->tanggal_order),"d F Y")	;
			$sub_array[] = $row ->tujuan;
			$sub_array[] = $row ->penjemputan;
			$sub_array[] = $row ->jam_jemput;
			$sub_array[] = '<button type="button" name="delete" id="'. $row ->id_request.'" class="btn btn-warning btn-xs delete" data-toggle="tooltip" data-placement="top" title="receipt"><i class="far fa-arrow-alt-circle-right"></i></button>
			';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mreceiptorder->get_all_data(),
			"recordsFiltered"=>$this->mreceiptorder->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	function user_action(){
	
		if($_POST["action"]=="Add")
		{
				$insert_data = array(
				'nik_user'=>$this->input->post('nik'),
				'tanggal_order'=>$this->input->post('tgl_overtime'),
				'tujuan'=>$this->input->post('tujuan'),
				'penjemputan'=>$this->input->post('jemput'),
				
				'STATUS'=>'ON'

			);

			$hasilCek =$this->mreceiptorder->cekOvertime($this->input->post('nik'),$this->input->post('tgl_overtime'));
			if($hasilCek<>true){
				
					$this->mreceiptorder->insert_crud($insert_data);
					echo 'masuk';
								
			}else{
				echo 'exist';
			}
			
						
			
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				'nik_user'=>$this->input->post('nik'),
				'tanggal_order'=>$this->input->post('tgl_overtime'),
				'tujuan'=>$this->input->post('tujuan'),
				'penjemputan'=>$this->input->post('jemput'),
			
				'driver'=>$this->input->post('driver'),

				'STATUS'=>'ON'
			);

			
				$this->mreceiptorder->update_crud($this->input->post('unik'),$update_data);
				echo 'ubah';
			
			
		}
	}
	
	

	function fetch_single_user(){
		$output = array();
		
		$data =$this->mreceiptorder->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['nik_user'] = $row->nik_user;
			$output['tanggal_order'] = $row->tanggal_order;
			$output['tujuan'] = $row->tujuan;
			$output['jam_jemput'] = $row->jam_jemput;
			$output['penjemputan'] = $row->penjemputan;
			$output['id_request'] = $row->id_request;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		$this->mreceiptorder->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

	function submit_all(){
		$user=$this->session->userdata('user');
		$pilih='A';

		
			$this->mreceiptorder->submit($user,$pilih);
			echo 'masuk';	
		

		
	}

	function aproval(){
		$update_data = array(
				
				'status'=>'AP',
				'mulai'=>time()
			);
			$this->mreceiptorder->update_crud($_POST['user_id'],$update_data);
	}

	function reject(){
		$update_data = array(
				
				'STATUS'=>'RJ',
				'ACCEPTED_DATE'=>date('Y-m-d'),
				'FLAG_ASSIGN'=>'N'

			);
			$this->mreceiptorder->update_crud($_POST['user_id'],$update_data);
	}


		
}
	