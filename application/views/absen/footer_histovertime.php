x<footer class="main-footer">
    
    <strong>Copyright &copy; 2020 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="<?php echo base_url('assets/plugins/jquery/jquery-3.5.1.js');?>"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables/1.10.22/jquery.dataTables.min.js');?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables-bs4/js/1.10.22/dataTables.bootstrap4.min.js');?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-rowreorder/js/1.2.7/dataTables.rowReorder.min.js');?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-responsive/js/2.2.6/dataTables.responsive.min.js');?>"></script>

<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js');?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js');?>"></script>

<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js');?>"></script>
<script type="text/javascript"> 
  var save_method; //for save method string
var table;
$(document).ready(function(){
  var dataTable = $('#example1').DataTable({
      "processing" :true,
      "serverSide" :true,
       "responsive": true,
      "order" :[],
      "ajax":{
        url:"<?php echo base_url().'histovertime/fetch_overtime';?>",
        type:"POST",

      },"language": {                
            "infoFiltered": ""
        },
      "columnDefs":[{

          "target" :[-1],
          "orderable" :false
      }]
  });


   

});
    
   
 
  
</script>

</body>
</html>