<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends CI_Controller {

	// ************************************************//
	// Contoller Cabang                                //
	// fungsi :                                        //
	// 1. Menambah Cabang baru                         //
	// 2. Mengubah Cabang yang ada                     //
	// 3. Menghapus Cabang                             //
	//												   //	 
	// Created by   : Wendy Bayu                       //
	// email        : wendy.bayu@gmail.com             //
	// created date : 25/10/2020					   //
	// version      : 1.0							   //
	// ************************************************//
	//	for edu only                                   //
	// 	for commercial purpose                         // 
	//	please email wendy.bayu@gmail.com              //
	// ************************************************//

	function __construct(){
		parent::__construct();
		$this->load->model('model_admin','madmin');	
		$this->load->model('Model_cabang','mcabang');
		
		if($this->session->userdata('status')!='login'){
			redirect(base_url('login'));
		}else{
			$result = $this->madmin->cekMenu($this->session->userdata('group'),'cabang');

			if ($result==false){
				redirect(base_url('login/logout'));
			}
		}
	}

	public function index()
	{	
		
		$user=$this->session->userdata('user');
		$group = $this->session->userdata('group');
		$data = array(
                'get_current_user'=> $this->madmin->ambiluserbyid('m_user',$user),
				'get_current_group'=> $this->madmin->ambiluserbyid('m_user',$user),
				'cboAktif'=>$this->madmin->select_cbo_parameter('FLAG_AKTIF'),
				'cboCabang'=>$this->madmin->select_cbo_parameter('CABANG'),
				'cboMenu'=>$this->madmin->getDeskripsiMenu($group,'cabang'),
				'cboEmployee'=>$this->madmin->getEmployeForClaim($user,$group),
				'getTitleSidebar'=> $this->madmin->getSettingById('OP03'),
				'getColorTopbar'=> $this->madmin->getSettingById('OP04'),
				'getColorSidebar'=> $this->madmin->getSettingById('OP05')
            );
		$this->load->view('absen/header_user',$data);
		$this->load->view('absen/view_cabang',$data);
		$this->load->view('absen/footer_cabang');
	}

	

	function fetch_cabang(){
					
		$fetch_data= $this->mcabang->make_datatable();
		$data = array();
		$no = 1;
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$sub_array[] = $no ;
			$sub_array[] = $row ->CABANG_ID;
			$sub_array[] = $row ->NAMA_CABANG;
			$sub_array[] = $row ->ALAMAT_CABANG;
			$sub_array[] = $row ->NO_TLP;
			$sub_array[] = $row ->NIK_PIC;
			$sub_array[] = date_format(new DateTime($row ->TGL_TODAY),"d F Y")	;
			
			switch ($row ->FLAG_ACTIVE) {
			       case "Y":
					$sub_array[] = '<span class="badge badge-success">Aktif</span>';
					break;
					case "N":
					$sub_array[] =  '<span class="badge badge-danger">Tidak Aktif</span>';
					break;
			}
			$sub_array[] = '<button type="button" name="edit" id="'. $row ->CABANG_ID.'" class="btn btn-warning btn-xs edit" ><i class="fas fa-edit"></i> Edit</button> <button type="button" name="delete" id="'. $row ->CABANG_ID.'" class="btn btn-danger btn-xs delete"><i class="fas fa-trash-alt"></i> Hapus</button>';
			$no = $no +1;
			$data[]= $sub_array;
			
		}
		$output = array(
			"draw"			 => intval($_POST['draw']),
			"recordsTotal"	=>$this->mcabang->get_all_data(),
			"recordsFiltered"=>$this->mcabang->get_filtered_data(),
			"data"			=> $data
		);
		echo json_encode($output);
	}

	
	function user_action(){
		if($_POST["action"]=="Add")
		{
			$insert_data = array(
				'CABANG_ID'=>$this->input->post('kodeCabang'),
				'NAMA_CABANG'=>$this->input->post('NameCabang'),
				'NO_TLP'=>$this->input->post('noTlp'),
				'ALAMAT_CABANG'=>$this->input->post('alamat'),
				'NIK_PIC'=>$this->input->post('nik'),
				'TGL_TODAY'=>$this->input->post('tglToday'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			
			$this->mcabang->insert_crud($insert_data);
			echo 'masuk';
		}

		if($_POST["action"]=="Edit")
		{
			$update_data = array(
				
				'NAMA_CABANG'=>$this->input->post('NameCabang'),
				'NO_TLP'=>$this->input->post('noTlp'),
				'ALAMAT_CABANG'=>$this->input->post('alamat'),
				'TGL_TODAY'=>$this->input->post('tglToday'),
				'NIK_PIC'=>$this->input->post('nik'),
				'FLAG_ACTIVE'=>$this->input->post('aktif')
			);
			
			$this->mcabang->update_crud($this->input->post('kodeCabang'),$update_data);
			echo 'ubah';
		}
	}

	function fetch_single_user(){
		$output = array();
			
		$data =$this->mcabang->fetch_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['NAMA_CABANG'] = $row->NAMA_CABANG;
			$output['NO_TLP'] = $row->NO_TLP;
			$output['FLAG_ACTIVE'] = $row->FLAG_ACTIVE;
			$output['CABANG_ID'] = $row->CABANG_ID;
			$output['ALAMAT_CABANG'] = $row->ALAMAT_CABANG;
			$output['NIK_PIC'] = $row->NIK_PIC;
			$output['TGL_TODAY'] = $row->TGL_TODAY;
		}
		echo json_encode($output);
	}

	function delete_single_user()
	{
		
		$this->mcabang->delete_single_user($_POST['user_id']);
		echo "deleted";
	}

		
}

// ウェンディバユ作成 //

