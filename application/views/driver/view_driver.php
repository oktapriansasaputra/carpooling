  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><strong>
                  <?php foreach ($cboMenu as $isiMenu) {
                          echo $isiMenu['DESCRIPTION'];
                    }?>
                </strong></h3>
                <div align="right">
                   <button type="button" data-toggle="modal" data-target="#modal-lg" data-whatever="@mdo" class="btn btn-success"><i class="fas fa-plus-circle"></i></button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
               
                  <table id="example1" class="table table-bordered table-striped" style="width:100%">
                  <thead>
            <tr>
              <th>No</th>
                <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Mobil</th>
                <th>Warna</th>
                <th>Plat No</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>No</th>
              <th>NIK</th>
                <th>Nama Karyawan</th>
                <th>Mobil</th>
                <th>Warna</th>
                <th>Plat No</th>
                <th>Action</th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
          <!-- ./col -->
        </div>

       
      

       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <form method="POST" id="user_form"> 
            <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">New Driver</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nama Karyawan:</label>
                <select name="karyawan" id="karyawan" class="form-control">
                 <option value="">-Pilih Nama Karyawan-</option>
                 <?php foreach ($cboKaryawan as $isiKaryawan) {?>
                  <option value="<?php echo $isiKaryawan['CD_EMPLOYEE']; ?>">
                    <?php echo $isiKaryawan['EMPLOYEE_NAME']; ?></option>
                  <?php  }?>
               </select>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Mobil:</label>
                <select name="mobil" id="mobil" class="form-control">
                 <option value="">-Pilih Mobil-</option>
                 <?php foreach ($cboMobil as $isiMobil) {?>
                  <option value="<?php echo $isiMobil['CODE']; ?>">
                    <?php echo $isiMobil['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
                <input type="hidden" class="form-control" name="action" id="action" value="Add"> <input type="hidden" class="form-control" name="unix" id="unix">
              </div>
              
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Warna Mobil:</label>
              <select name="warna" id="warna" class="form-control">
                 <option value="">-Pilih Warna Mobil-</option>
                 <?php foreach ($cboWarna as $isiWarna) {?>
                  <option value="<?php echo $isiWarna['CODE']; ?>">
                    <?php echo $isiWarna['DESCRIPTION']; ?></option>
                  <?php  }?>
               </select>
              </div> 
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Plat No:</label>
               <input type="text" name="platno" id="platno" class="form-control"> 
              </div>
              
              
              
              
            </div>
            <div class="modal-footer">
              <input type="submit" name="action" class="btn btn-success"  value="Submit" />
              
              
            </div>
          
            </div></form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->