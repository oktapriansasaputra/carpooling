x<footer class="main-footer">

  <strong>Copyright &copy; 2020 </strong> All rights
  reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->


<script src="<?php echo base_url('assets/plugins/jquery/jquery-3.5.1.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->

<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables/1.10.22/jquery.dataTables.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/datatables-bs4/js/1.10.22/dataTables.bootstrap4.min.js'); ?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-rowreorder/js/1.2.7/dataTables.rowReorder.min.js'); ?>"></script>

<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/plugins/datatables-responsive/js/2.2.6/dataTables.responsive.min.js'); ?>"></script>

<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/dist/js/adminlte.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="<?php echo base_url('assets/bower_components/font-awesome/js/all.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/brands.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/solid.js'); ?>"></script>

<script src="<?php echo base_url('assets/bower_components/font-awesome/js/fontawesome.js'); ?>"></script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js'); ?>"></script>
<script type="text/javascript">
  var save_method; //for save method string
  var table;
  $(document).ready(function() {
    var dataTable = $('#example1').DataTable({
      "processing": true,
      "serverSide": true,
      "responsive": true,
      "order": [],
      "ajax": {
        url: "<?php echo base_url() . 'karyawan/fetch_karyawan'; ?>",
        type: "POST"

      },
      "columnDefs": [{

        "target": [-1],
        "orderable": false
      }]
    });


    $(document).on('submit', '#user_form', function(event) {
      event.preventDefault();
      var paramater = $("#nik").val();
      var kode = $("#kode").val();


      $.ajax({
        url: "<?php echo base_url() . 'karyawan/user_action'; ?>",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function(response) {
          if (response == 'masuk') {

            Swal.fire({
              icon: 'success',
              title: 'Data Karyawan',
              text: 'Data telah berhasil di tambahkan !',
              showConfirmButton: false,
              timer: 1500
            }) // end sweet alert
          } else if (response == 'ubah') {
            Swal.fire({
              icon: 'success',
              title: 'Data Karyawan',
              text: 'Data telah berhasil di ubah !',
              showConfirmButton: false,
              timer: 1500
            })
          } else if (response == 'exist') {
            Swal.fire({
              icon: 'error',
              title: 'Data Karyawan',
              text: 'NIK telah ada',
              showConfirmButton: false,
              timer: 1500
            }) //end if
          }
          $('#user_form')[0].reset();
          $('#modal-lg').modal('hide');
          $('#action').val("Add");
          $('.modal-title').text("Add Karyawan");
          dataTable.ajax.reload();
        }
      });


    });

    $(document).on('click', '.edit', function(event) {
      var user_id = $(this).attr('id');
      $.ajax({
        url: "<?php echo base_url() . 'karyawan/fetch_single_user'; ?>",
        method: "POST",
        data: {
          user_id: user_id
        },
        dataType: "json",
        success: function(data) {

          $('#modal-lg').modal('show');
          $('#karyawanName').val(data.EMPLOYEE_NAME);
          $('#nik').val(data.CD_EMPLOYEE);
          $('#atasan').val(data.CD_DESIGN);
          $('#jabatan').val(data.CD_TITLE);
          $('#alamat').val(data.ALAMAT);
          $('#city').val(data.CITY);
          $('#statusEmployee').val(data.STATUS);
          $('#dateBirth').val(data.DATE_BIRTH);
          $('#placeBirth').val(data.PLACE_BIRTH);
          $('#dateJoin').val(data.DATE_JOIN);
          $('.modal-title').text("Edit Karyawan");
          $('#action').val("Edit");

        }
      })
    });

    $(document).on('click', '.close', function(event) {
      var user_id = $(this).attr('id');
      $('#user_form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('is-invalid'); // clear error class

      $('#user_form')[0].reset();
      $('#modal-lg').modal('hide');
      $('#action').val("Add");
      $('#paramater').removeAttr('disabled');
      //$('.modal-title').reset();
      $('.modal-title').text("Tambah Karyawan");
    });

    $(document).on('click', '.delete', function(event) {
      event.preventDefault();
      var user_id = $(this).attr('id');
      Swal.fire({
        title: 'Are you sure?',
        text: "Data Karyawan akan di hapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus Data!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url: "<?php echo base_url() . 'karyawan/delete_single_user'; ?>",
            method: "POST",
            data: {
              user_id: user_id
            },
            success: function(data) {
              dataTable.ajax.reload();
              Swal.fire({
                icon: 'success',
                title: 'Data Karyawan',
                text: 'Data telah berhasil di hapus !',
                showConfirmButton: false,
                timer: 1500
              })
            }
          })


        } // end if
      }) //end sweetalert


    });

    $(document).on('submit', '#formPassword', function(event) {
      event.preventDefault();

      $.ajax({
        url: "<?php echo base_url() . 'login/NewPassword'; ?>",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function(response) {
          if (response == 'ubah') {
            Swal.fire({
              icon: 'success',
              title: 'Password',
              text: 'Password telah berhasil di ubah !',
              showConfirmButton: false,
              timer: 1500
            })
            $('#modal-default').modal('hide');
          } else if (response == 'beda') {
            Swal.fire({
              icon: 'error',
              title: 'Password',
              text: 'Password tidak sama !',
              showConfirmButton: false,
              timer: 1500
            })
          } else if (response == 'null') {
            Swal.fire({
              icon: 'error',
              title: 'Password',
              text: 'Password kosong !',
              showConfirmButton: false,
              timer: 1500
            })
          } //end if

        }
      });
    });

    $('#user_form').validate({
      rules: {
        nik: {
          required: true,
        },
        karyawanName: {
          required: true
        },
        alamat: {
          required: true
        },

        jabatan: {
          required: true
        },
      },
      messages: {
        nik: {
          required: "Please enter nik"
        },
        karyawanName: {
          required: "Please enter your Nama Karyawan"
        },

        alamat: {
          required: "Please enter your alamat"
        },
        jabatan: {
          required: "Please choose jabatan"
        }
      },
      errorElement: 'span',
      errorClass: "help-block",
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });




  });
</script>

</body>

</html>